package com.longnguyen.samplegridview.asymetricgridview;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.longnguyen.samplegridview.R;

import java.util.List;

public class AsymAdapter extends BaseAdapter {

    public static final int TYPE_IMAGE_TEXT = 0;
    public static final int TYPE_TEXT = 1;

    private static final String TAG = AsymAdapter.class.getSimpleName();

    private List<AsymModel> items;

    public AsymAdapter(List<AsymModel> items) {
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {

        View newView;

        if(getItemViewType(position) == TYPE_IMAGE_TEXT) {
            newView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_item_text_image, parent, false);
            populateTextImage(newView, position);
        } else {
            newView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_item_text, parent, false);
            populateText(newView, position);
        }


        return newView;

    }

    private void populateText(View view, int position) {
        TextView text = view.findViewById(R.id.text);
        text.setText(String.valueOf(position));
        text.setTextColor(Color.WHITE);
    }

    private void populateTextImage(View view, int position) {
        ImageView imageView = view.findViewById(R.id.image);
        imageView.setImageResource(items.get(position).getImage());

        TextView textView = view.findViewById(R.id.text);
        textView.setText(String.valueOf(position));
        textView.setTextColor(Color.WHITE);
    }

}
