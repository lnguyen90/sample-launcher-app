package com.longnguyen.samplegridview;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.felipecsl.asymmetricgridview.AsymmetricGridView;
import com.felipecsl.asymmetricgridview.AsymmetricGridViewAdapter;
import com.longnguyen.samplegridview.asymetricgridview.AsymAdapter;
import com.longnguyen.samplegridview.asymetricgridview.AsymModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Play around with the items here with column and row.
        List<AsymModel> list = Arrays.asList(
                new AsymModel(5,2, AsymAdapter.TYPE_IMAGE_TEXT, R.drawable.exp_widget_1_preview),
                new AsymModel(2,1, AsymAdapter.TYPE_TEXT),
                new AsymModel(3,1, AsymAdapter.TYPE_IMAGE_TEXT, R.drawable.exp_widget_1_preview),
                new AsymModel(1,1, AsymAdapter.TYPE_TEXT),
                new AsymModel(1,1, AsymAdapter.TYPE_TEXT),
                new AsymModel(1,1, AsymAdapter.TYPE_TEXT),
                new AsymModel(1,1, AsymAdapter.TYPE_TEXT),
                new AsymModel(1,1, AsymAdapter.TYPE_TEXT)
        );

        AsymmetricGridView asymmetricGridView = findViewById(R.id.listView);

        // Max column is 5
        asymmetricGridView.setRequestedColumnCount(5);

        AsymAdapter asymAdapter = new AsymAdapter(list);
        AsymmetricGridViewAdapter asymmetricGridViewAdapter
                = new AsymmetricGridViewAdapter(this, asymmetricGridView, asymAdapter);
        asymmetricGridView.setAdapter(asymmetricGridViewAdapter);

        asymmetricGridView.setAllowReordering(false);
        asymmetricGridView.setRequestedHorizontalSpacing(0);
        asymmetricGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, "position = " + position, Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!isMyAppLauncherDefault()) resetPreferredLauncherAndOpenChooser(this);
    }

    @Override
    public void onBackPressed() {

    }

    /**
     * Clear the defaults for our launcher. A dialog will appear to let user choose
     * the default launcher.
     */
    private void finishLauncher() {
        // clear our app from default launcher
        this.getPackageManager().clearPackagePreferredActivities(this.getPackageName());
        finish();

        // Open dialog so user can choose a default launcher.
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    /**
     * Checks to see if app is currently set as default launcher
     * @return boolean true means currently set as default, otherwise false
     */
    private boolean isMyAppLauncherDefault() {
        final IntentFilter filter = new IntentFilter(Intent.ACTION_MAIN);
        filter.addCategory(Intent.CATEGORY_HOME);

        List<IntentFilter> filters = new ArrayList<>();
        filters.add(filter);

        final String myPackageName = getPackageName();
        List<ComponentName> activities = new ArrayList<>();
        final PackageManager packageManager = getPackageManager();

        packageManager.getPreferredActivities(filters, activities, null);

        for (ComponentName activity : activities) {
            if (myPackageName.equals(activity.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Reset the preferred launcher and launcher the dialog to let user choose
     * the default launcher. This method will be called in our launcher's activity's onResume method.
     *
     * @param context
     */
    private void resetPreferredLauncherAndOpenChooser(Context context) {
        PackageManager packageManager = context.getPackageManager();
        ComponentName componentName = new ComponentName(context, FakeLauncherActivity.class);
        packageManager.setComponentEnabledSetting(componentName
                , PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

        Intent selector = new Intent(Intent.ACTION_MAIN);
        selector.addCategory(Intent.CATEGORY_HOME);
        selector.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(selector);

        packageManager.setComponentEnabledSetting(componentName
                , PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, PackageManager.DONT_KILL_APP);
    }
}
