package com.longnguyen.samplegridview.asymetricgridview;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.felipecsl.asymmetricgridview.AsymmetricItem;

public class AsymModel implements AsymmetricItem {

    private int column;
    private int row;
    private int type;
    private int image;

    public AsymModel(int column, int row, int type, int image) {
        this.column = column;
        this.row = row;
        this.type = type;
        this.image = image;
    }

    public AsymModel(int column, int row, int type) {
        this.column = column;
        this.row = row;
        this.type = type;
    }

    public AsymModel(Parcel in) {
        readFromParcel(in);
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    @Override
    public int getColumnSpan() {
        return column;
    }

    @Override
    public int getRowSpan() {
        return row;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in) {
        column = in.readInt();
        row = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(column);
        dest.writeInt(row);
    }

    /* Parcelable interface implementation */
    public final Parcelable.Creator<AsymModel> CREATOR = new Parcelable.Creator<AsymModel>() {
        @Override
        public AsymModel createFromParcel(@NonNull Parcel in) {
            return new AsymModel(in);
        }

        @Override
        @NonNull
        public AsymModel[] newArray(int size) {
            return new AsymModel[size];
        }
    };
}
