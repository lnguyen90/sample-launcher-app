package com.longnguyen.samplegridview.asymetricgridview;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

public class GridItemView extends FrameLayout {

    private TextView mTitle;
    private Context mContext;

    public GridItemView(@NonNull Context context) {
        super(context);
        mContext = context;
        init();
    }

    private void init() {
        mTitle = new TextView(mContext);
        mTitle.setGravity(Gravity.CENTER);
        mTitle.setTextColor(Color.CYAN);
        addView(mTitle, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    public void setTitle(String title) {
        this.mTitle.setText(title);
        requestLayout();
    }

}
