package com.me.gridsampleapp.gridlayout;


/**
 * Created by Long on 09/26/2018
 */
public class GridItem implements CustomGridLayoutItem {

    private int mColumnSpan;
    private int mRowSpan;
    private int mColumnPosition;
    private int mRowPosition;
    private int mType;
    private int mImage;
    private String mText;

    public GridItem(int rowPosition, int columnPosition, int mRowSpan, int columnSpan, int type, int image, String text) {
        this.mColumnSpan = columnSpan;
        this.mRowSpan = mRowSpan;
        this.mRowPosition = rowPosition;
        this.mColumnPosition = columnPosition;
        this.mType = type;
        this.mImage = image;
        this.mText = text;
    }

    public GridItem(int rowPosition, int columnPosition, int mRowSpan, int columnSpan, int type, int image) {
        this.mColumnSpan = columnSpan;
        this.mRowSpan = mRowSpan;
        this.mRowPosition = rowPosition;
        this.mColumnPosition = columnPosition;
        this.mType = type;
        this.mImage = image;
    }

    public GridItem(int rowPosition, int columnPosition, int mRowSpan, int columnSpan, int type, String text) {
        this.mColumnSpan = columnSpan;
        this.mRowSpan = mRowSpan;
        this.mRowPosition = rowPosition;
        this.mColumnPosition = columnPosition;
        this.mType = type;
        this.mText = text;
    }

    public GridItem(int rowPosition, int columnPosition, int mRowSpan, int columnSpan, int type) {
        this.mColumnSpan = columnSpan;
        this.mRowSpan = mRowSpan;
        this.mRowPosition = rowPosition;
        this.mColumnPosition = columnPosition;
        this.mType = type;
    }

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        this.mType = type;
    }

    public int getImage() {
        return mImage;
    }

    public void setImage(int image) {
        this.mImage = image;
    }

    public String getText() {
        return mText;
    }

    public void setText(String mText) {
        this.mText = mText;
    }

    @Override
    public int getRowSpan() {
        return mRowSpan;
    }

    @Override
    public int getColumnSpan() {
        return mColumnSpan;
    }

    @Override
    public int getRowPosition() {
        return mRowPosition;
    }

    @Override
    public int getColumnPosition() {
        return mColumnPosition;
    }

}
