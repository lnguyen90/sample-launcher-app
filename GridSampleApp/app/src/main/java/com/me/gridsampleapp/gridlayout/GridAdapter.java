package com.me.gridsampleapp.gridlayout;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.me.gridsampleapp.R;

import java.util.List;

/**
 * Created by Long on 09/26/2018
 */
public class GridAdapter extends CustomGridLayoutAdapter {

    private static final String TAG = GridAdapter.class.getSimpleName();

    public static final int TYPE_IMAGE_TEXT = 0;
    public static final int TYPE_IMAGE = 1;
    public static final int TYPE_TEXT = 2;
    public static final int TYPE_BLANK = 3;

    private List<GridItem> list;

    public GridAdapter(List<GridItem> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public CustomGridLayoutItem getCustomGridLayoutItem(int position) {
        return list.get(position);
    }

    @Override
    protected int getItemViewType(int position) {
        return list.get(position).getType();
    }

    @Override
    protected int getViewTypeCount() {
        return 4;
    }

    @Override
    public View getView(final int position, @NonNull ViewGroup parent) {

        View newView;
        int viewType = getItemViewType(position);
        if(viewType == TYPE_IMAGE_TEXT) {
            newView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_item_image_text, parent, false);
            populateTextImage(newView, position);
        } else if(viewType == TYPE_TEXT){
            newView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_item_text, parent, false);
            populateText(newView, position);
        } else if(viewType == TYPE_IMAGE) {
            newView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_item_image, parent, false);
            populateImage(newView, position);
        } else {
            newView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_item_blank, parent, false);
        }

        newView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "" + position, Toast.LENGTH_SHORT).show();
            }
        });

        return newView;
    }

    private void populateText(View view, int position) {
        TextView text = view.findViewById(R.id.text);
        text.setText(String.valueOf(position));
        text.setTextColor(Color.WHITE);
    }

    private void populateTextImage(View view, final int position) {
        final ImageView imageView = view.findViewById(R.id.image);
        Glide.with(view.getContext()).load(list.get(position).getImage()).into(imageView);

        TextView textView = view.findViewById(R.id.text);
        textView.setText(list.get(position).getText());
        textView.setTextColor(Color.WHITE);

    }

    private void populateImage(View view, int position) {
        ImageView imageView = view.findViewById(R.id.image);
        Glide.with(view.getContext()).load(list.get(position).getImage()).into(imageView);
    }
}
