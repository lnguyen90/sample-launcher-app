package com.me.gridsampleapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ViewPager mPager;
    ViewGroup mFrameLayout;
    CustomPagerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPager = (ViewPager) findViewById(R.id.pager);

        Log.d("*______________", "Running");

        mAdapter = new CustomPagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mAdapter);
        mPager.setOffscreenPageLimit(4);
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    class CustomPagerAdapter extends FragmentPagerAdapter {

        final int MAX = 4;
        public CustomPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            int index = position%MAX;
            switch (index) {
                case 0:
                    return ContentFragment.newInstance("Hello");
                case 1:
                    return ContentFragment.newInstance("GridLayout");
                case 2:
                    return ContentFragment.newInstance("!!!!!");
                case 3:
                    return GridViewLayoutFragment.newInstance();
            }
            return null;
        }

        @Override
        public int getCount() {
            return MAX;
        }
    }

}
