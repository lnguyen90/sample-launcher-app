package com.me.gridsampleapp;

import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.me.gridsampleapp.gridlayout.CustomGridLayout;
import com.me.gridsampleapp.gridlayout.GridAdapter;
import com.me.gridsampleapp.gridlayout.GridItem;


import java.util.Arrays;
import java.util.List;


public class GridViewLayoutFragment extends Fragment implements AdapterView.OnItemClickListener {

    private static final List<GridItem> gridItemList = Arrays.asList(
            new GridItem(0,0, 1, 4, GridAdapter.TYPE_IMAGE_TEXT, R.drawable.watch_whats_new, "WOW"),
            new GridItem(1,0, 2, 2, GridAdapter.TYPE_IMAGE_TEXT, R.drawable.battery, "WOW 1"),
            new GridItem(1,2, 1, 2, GridAdapter.TYPE_IMAGE_TEXT, R.drawable.battery, "WOW 2"),
            new GridItem(2,0, 1, 4, GridAdapter.TYPE_IMAGE, R.drawable.spec),
            new GridItem(3,2, 1, 2, GridAdapter.TYPE_IMAGE_TEXT, R.drawable.battery, "WOW 5"),
            new GridItem(4,2, 1, 2, GridAdapter.TYPE_IMAGE_TEXT, R.drawable.battery, "WOW 6"),
            new GridItem(4,0, 1, 2, GridAdapter.TYPE_IMAGE_TEXT, R.drawable.battery, "WOW 7")
    );

    View mView;

    public GridViewLayoutFragment() {
        // Required empty public constructor
    }

    public static GridViewLayoutFragment newInstance() {
        GridViewLayoutFragment fragment = new GridViewLayoutFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;

            mView = inflater.inflate(R.layout.fragment_grid_view_layout, container, false);

        mView.setSystemUiVisibility(uiOptions);

        useGridLayout(mView);
        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mView != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null) {
                parent.removeAllViews();
            }
        }
    }

    private void useGridLayout(View view) {
        CustomGridLayout customGridLayout = (CustomGridLayout) view.findViewById(R.id.custom_layout);
        customGridLayout.setColumnCount(4);
        GridAdapter adapter = new GridAdapter(gridItemList);
        customGridLayout.setAdapter(adapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(getActivity(), "" + position, Toast.LENGTH_SHORT).show();
    }
}
