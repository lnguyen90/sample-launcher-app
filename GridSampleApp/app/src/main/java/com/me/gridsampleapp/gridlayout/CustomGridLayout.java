package com.me.gridsampleapp.gridlayout;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;


/**
 * Created by Long on 09/26/2018
 */
public class CustomGridLayout extends GridLayout {

    private static final String TAG = CustomGridLayout.class.getSimpleName();

    private static int DEFAULT_COLUMN_SPAN = 4;
    private static int DEFAULT_ROW_SPAN = 4;

    private CustomGridLayoutAdapter mAdapter;

    private int mColumn = DEFAULT_COLUMN_SPAN;
    private int mRow = DEFAULT_ROW_SPAN;

    public CustomGridLayout(Context context) {
        super(context);
        init();
    }

    public CustomGridLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomGridLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * Initialize custom GridLayout
     */
    private void init() {

        this.setColumnCount(mColumn);
        this.setRowCount(mRow);
        this.setAlignmentMode(ALIGN_BOUNDS);
    }

    /**
     * Set the maximum row count.
     *
     * @param row
     */
    public void setRowCount(int row) {
        this.mRow = row;
        updateContainer();
    }

    /**
     * Set the maximum column count.
     *
     * @param column
     */
    public void setColumnCount(int column) {
        this.mColumn = column;
        updateContainer();
    }

    /**
     * Set the adapter so that we can add the view created by the adapter to
     * the parent view
     *
     * @param adapter
     */
    public void setAdapter(CustomGridLayoutAdapter adapter) {
        this.mAdapter = adapter;
        updateContainer();
    }

    /**
     * Remove all previous views, and add all the views created by adapter to
     * the parent view
     */
    private void updateContainer() {
        if(mAdapter == null) return;

        removeChildViews(this);

        for(int i = 0; i < mAdapter.getCount(); i++) {
            View view = mAdapter.getView(i, this);

            int rowPos = mAdapter.getCustomGridLayoutItem(i).getRowPosition();
            int columnPos = mAdapter.getCustomGridLayoutItem(i).getColumnPosition();
            int rowSpan = mAdapter.getCustomGridLayoutItem(i).getRowSpan();
            int columnSpan = mAdapter.getCustomGridLayoutItem(i).getColumnSpan();

            addViewToGridLayout(view, rowPos, columnPos, rowSpan, columnSpan);
        }

    }

    /**
     * Calculate the width of the child view based on the column number.
     *
     * @param viewColumn The child view's column.
     * @return float
     */
    private float getWidth(int viewColumn) {
        if(viewColumn < 0 || viewColumn > mColumn) {
            throw new IllegalArgumentException("column span must be between 0 and " + mColumn);
        }
        return (viewColumn * 100f / mColumn) / 100f;
    }

    /**
     * Set layout params with row/column information to the view. And add the view to the parent
     * view
     *
     * @param view
     * @param row
     * @param column
     * @param rowSpan
     * @param columnSpan
     */
    private void addViewToGridLayout(View view, int row, int column, int rowSpan, int columnSpan) {

//        final int row = 1;  // View starts in second row
//        final int rowSpan = 2; // View spans two rows
//
//        final int column = 1; // View starts in second column
//        final int columnSpan = 2; // View spans two columns

        GridLayout.LayoutParams params = (GridLayout.LayoutParams) view.getLayoutParams();
        params.setGravity(Gravity.FILL_HORIZONTAL);
        params.width = 0;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        params.columnSpec =  GridLayout.spec(column, columnSpan, getWidth(columnSpan));
        params.rowSpec = GridLayout.spec(row,rowSpan);

        this.addView(view, params);
    }

    /**
     * Remove all child views from its parent.
     *
     * @param parent The parent view.
     */
    private void removeChildViews(ViewGroup parent) {
        if (parent == null) return;
        release(parent);
        parent.removeAllViews();
    }

    private void release(ViewGroup parent) {
        for(int i=0; i<parent.getChildCount(); i++) {
            View childView = parent.getChildAt(i);
            if (childView instanceof ViewGroup) {
                release((ViewGroup) childView);
                ((ViewGroup) childView).removeAllViews();
            }
        }
    }

}
