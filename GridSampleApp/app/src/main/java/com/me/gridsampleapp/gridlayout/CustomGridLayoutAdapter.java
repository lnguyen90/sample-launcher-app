package com.me.gridsampleapp.gridlayout;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

import com.me.gridsampleapp.gridlayout.CustomGridLayoutItem;

/**
 * Created by Long on 09/26/2018
 */
public abstract class CustomGridLayoutAdapter {

    public abstract int getCount();

    protected int getItemViewType(int position) { return position; }

    protected int getViewTypeCount() { return 0; }

    public abstract CustomGridLayoutItem getCustomGridLayoutItem(int position);

    public abstract View getView(final int position, @NonNull ViewGroup parent);
}
