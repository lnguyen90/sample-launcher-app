package com.tecace.retail.util;

import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;


/**
 * Created by smheo on 6/22/2016.
 */
public class ScreenRotationHandler {
    private static final String TAG = ScreenRotationHandler.class.getSimpleName();

    private ValueAnimator mValueAnimator = null;

    private static final int ROTATION_CHANGE_MSG = 256;
    private static final long DEFAULT_ROTATION_CHANGE_TIMEOUT = 300; // 550 -> 300;
    private long mChangeTimeOut = DEFAULT_ROTATION_CHANGE_TIMEOUT;

    private OrientationEventListener mOrientationEventListener = null;
    private volatile OnChangeOrientationListener listener;
    public interface OnChangeOrientationListener{
        void OnChangeOrientation(int rotation);
    }

    private Context mContext = null;
    private int mCurrentAngle = 0;
    private boolean mIsSupport180RotationEvent = false;


    private Handler mRotationChangeHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == ROTATION_CHANGE_MSG) {
                //Log.d(TAG, "@#@# Received angle message : " + msg.arg1);
                if (listener != null) listener.OnChangeOrientation(msg.arg1);
            }
        }
    };

    public ScreenRotationHandler(Context context) {
        this.mContext = context;

        // set default timeout
        mChangeTimeOut = DEFAULT_ROTATION_CHANGE_TIMEOUT;

        // for reading only first one time
        initSensorListener();

        /**
         *  when we enabled the OrientationEventListener listener, we received the device orientation angle
         *  And we send angle value to the callback method.
         */
        mOrientationEventListener = new OrientationEventListener(mContext.getApplicationContext(),
                SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged (int orientation) {
                if (orientation == OrientationEventListener.ORIENTATION_UNKNOWN) return;

                int newAngle = 270;
                if (orientation < 45 || orientation > 315) newAngle = 0;
                else if (orientation < 135) newAngle = 90;
                else if (orientation < 225) newAngle = 180;

                if (newAngle != mCurrentAngle) {
                    if (!mIsSupport180RotationEvent && newAngle == 180) {
                        return;
                    }

                    if(mRotationChangeHandler != null) {
                        mRotationChangeHandler.removeMessages(ROTATION_CHANGE_MSG);
                        Message sendMsg = mRotationChangeHandler.obtainMessage(ROTATION_CHANGE_MSG, newAngle, 0);
                        mRotationChangeHandler.sendMessageDelayed(sendMsg, mChangeTimeOut);
                    }
                    Log.d(TAG, "@#@# The device orientation changed from " + mCurrentAngle + " to " + newAngle);
                    mCurrentAngle = newAngle;
                }
            }
        };
    }

    /**
     * Sets the callback for when device rotation changed.
     */
    public void setOnChangeOrientationListener(OnChangeOrientationListener listener){
        this.listener = listener;
    }

    public void setSupport180RotationEvent(boolean enable) {
        this.mIsSupport180RotationEvent = enable;
    }

    public void setTimeout(long ms) {
        mChangeTimeOut = ms;
    }

    public int getCurrentAngle() {
        return this.mCurrentAngle;
    }

    /**
     * enable the OrientationEventListener listener
     */
    public void start(){
        if (mOrientationEventListener != null) {

            /**
             *  init angle
             */
            mCurrentAngle = getCurrentScreenRotationAngle(mContext);
            if (mCurrentAngle == -1) {
                Log.w(TAG, "@#@# cannot get current screen angle. The angle value has been initialized to zero");
                mCurrentAngle = 0;
            }

            mOrientationEventListener.enable();

            /**
             *
             */
            enableSensorListener();
        }
    }

    /**
     * disable the OrientationEventListener listener
     */
    public void stop(){
        mRotationChangeHandler.removeMessages(ROTATION_CHANGE_MSG);
        if (mOrientationEventListener != null) {
            mOrientationEventListener.disable();
        }
    }

    public void release() {
        this.stop();
        mRotationChangeHandler = null;
        mOrientationEventListener = null;

        releaseSensorListener();
    }

    /**
     * Returns current screen angle from the rotation of the screen
     * if the return value is -1, it means that the angle can not be obtained.
     *
     */
    private int getCurrentScreenRotationAngle(Context context) {
        if (context == null) return -1;

        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();
        Log.d(TAG, "@#@# The device orientation rotation : " + rotation);

        int screen_angle = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
            case Surface.ROTATION_180:
                screen_angle = 0;
                break;
            case Surface.ROTATION_90:
                screen_angle = 270;
                break;
            case Surface.ROTATION_270:
                screen_angle = 90;
                break;
            default:
                screen_angle = 0;
        }

        return screen_angle;
    }

    public static void forceRotateScreen(Activity activity, int angle) {
        final int setOrientation;
        if (activity != null) {
            switch(angle) {
                case 0:
                case 360:
                    setOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                case 90:
                case -270:
                    setOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                    break;
                case 180:
                case -180:
                    setOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                    break;
                case 270:
                case -90:
                    setOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                default:
                    return;
            }
            activity.setRequestedOrientation(setOrientation);
        }
    }


    /**
     *
     *  Sensor listener for first time sensing
     *
     */

    private SensorEventListener mSensorEventListener;
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private int mFirstTimeOrientation;

    private void initSensorListener() {
        mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        if (mSensorManager == null) return;

        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (mSensor != null) {
            mSensorEventListener = new CCustomSensorEventListenerImpl();
        }
    }

    private void releaseSensorListener() {
        disableSensorListener();

        if (mSensor != null) {
            mSensor = null;
        }
        if (mSensorManager != null) {
            mSensorManager = null;
        }

        if (mSensorEventListener != null) {
            mSensorEventListener = null;
        }
    }

    private void enableSensorListener() {
        if (mSensor == null) {
            Log.w(TAG, "Cannot detect sensors. Not enabled");
            return;
        }

        if (mSensorManager == null) {
            Log.w(TAG, "Sensor manager is null.");
            return;
        }
        mFirstTimeOrientation = OrientationEventListener.ORIENTATION_UNKNOWN;
        mSensorManager.registerListener(mSensorEventListener, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void disableSensorListener() {
        if (mSensor == null) {
            Log.w(TAG, "Cannot detect sensors. Invalid disable");
            return;
        }

        if (mSensorManager == null) {
            Log.w(TAG, "Sensor manager is null.");
            return;
        }
        mSensorManager.unregisterListener(mSensorEventListener);
    }

    /**
     * Inner class for get orientation angle using sensorEventListener
     */
    class CCustomSensorEventListenerImpl implements SensorEventListener {
        private static final int _DATA_X = 0;
        private static final int _DATA_Y = 1;
        private static final int _DATA_Z = 2;

        public void onSensorChanged(SensorEvent event) {
            float[] values = event.values;
            int orientation = OrientationEventListener.ORIENTATION_UNKNOWN;
            float X = -values[_DATA_X];
            float Y = -values[_DATA_Y];
            float Z = -values[_DATA_Z];
            float magnitude = X*X + Y*Y;
            // Don't trust the angle if the magnitude is small compared to the y value
            if (magnitude * 4 >= Z*Z) {
                float OneEightyOverPi = 57.29577957855f;
                float angle = (float)Math.atan2(-Y, X) * OneEightyOverPi;
                orientation = 90 - (int)Math.round(angle);
                // normalize to 0 - 359 range
                while (orientation >= 360) {
                    orientation -= 360;
                }
                while (orientation < 0) {
                    orientation += 360;
                }
            }

            mFirstTimeOrientation = orientation;

            /**
             *
             */
            disableSensorListener();

            if (mOrientationEventListener != null) {
                mOrientationEventListener.onOrientationChanged(orientation);
            }
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    }

    /**
     * Util methods for the rotation animation
     */
    public void startViewRotateAnimation(final View view, final float setAngle,
                                         final TimeInterpolator interpolator, final int duration) {
        if (view != null) {
            final View ActionView = view;

            if (mValueAnimator == null) {
                mValueAnimator = new ValueAnimator();
            }

            if (mValueAnimator != null) {
                if (mValueAnimator.isRunning()) {
                    mValueAnimator.end();
                    //mValueAnimator.cancel();
                }

                Log.d(TAG, "@#@# Animation angle : " + ActionView.getRotation() + " -> " + setAngle);
                mValueAnimator.setFloatValues(ActionView.getRotation(), setAngle);
                if (mValueAnimator != null) {
                    mValueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(ValueAnimator animation) {
                            if (view != null) {
                                float angle = (float) animation.getAnimatedValue();
                                //Log.d(TAG, "onAnimationUpdate : " + angle);

                                view.setRotation(angle);
                            }
                        }
                    });

                    // set duration time (duration >= 0)
                    int setDuration = duration;
                    if (duration < 0) {
                        setDuration = 0;
                    }
                    mValueAnimator.setDuration(setDuration);

                    // set interpolation (default : AccelerateInterpolator)
                    if (interpolator != null) {
                        mValueAnimator.setInterpolator(interpolator);
                    } else { // default
                        mValueAnimator.setInterpolator(new AccelerateInterpolator());
                    }

                    // start the value animator
                    mValueAnimator.start();
                }
            }
        }
    }

    public void stopViewRotateAnimation() {
        if (mValueAnimator != null) {
            if (mValueAnimator.isRunning()) {
                mValueAnimator.end();
            }
            mValueAnimator.removeAllUpdateListeners();
            //mValueAnimator.removeAllListeners();
            mValueAnimator = null;
        }
    }

}
