package com.tecace.retail.util.analytics;

/**
 * Created by icanmobile on 1/30/17.
 */

public enum FragmentChangeType {
    START_APP("start_app"),
    BACK_PRESSED("back_pressed"),
    TIME_OUT("time_out"),
    VIDEO_COMPLETED("video_completed"),
    GEAR_S2("gear_s2"),
    GEAR_S3("gear_s3"),
    GEAR_SPORT("gear_sport"),
    TAP("tap"), //for bluetooth connection
    HAMBURGER_NENU("hamburger_menu"),
    CANNED_APPS("canned_icon"),
    SPEC_WIDGET("spec_widget"),
    APP_WIDGET("app_widget"),
    WIDGET("widget"),
    FACE_DETECTION("face_detection"),
    MISSING_CONTENT("missing_content"),
    EXIT_APP("exit_app"),
    HOME_KEY("home_key"),
    UNKNOWN("unknown_source"),
    LOST_FOCUS("lost_focus"),
    KEYBOARD("keyboard"),
    SWIPE("swipe"),
    SWIPE_LEFT("swipe_left"),
    SWIPE_RIGHT("swipe_right"),
    SWIPE_UP("swipe_up"),
    SWIPE_DOWN("swipe_down"),
    NFC("NFC"),
    SUBSIDIARY_CHANGED("subsidiary_changed"),
    START_APP_FROM_SHORTCUT("start_app_from_shortcut"),
    SPEN_JOURNEY_CONNECTED("SPEN_JOURNEY_CONNECTED"),
    SPEN_JOURNEY_NOT_CONNECTED("SPEN_JOURNEY_NOT_CONNECTED")
    ;

    String changeType;
    FragmentChangeType(String changeType) {
        this.changeType = changeType;
    }

    public String getChangeType() {
        return this.changeType;
    }
}
