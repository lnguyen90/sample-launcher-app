package com.tecace.retail.util;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

/**
 * Created by smheo on 3/14/2017.
 */

public class BitmapUtil {
    private static volatile BitmapUtil sInstance = null;
    public static BitmapUtil getInstance() {
        if (sInstance == null) {
            synchronized (BitmapUtil.class) {
                if (sInstance == null)
                    sInstance = new BitmapUtil();
            }
        }
        return sInstance;
    }
    private BitmapUtil() {}

    public void recycleImageView (ImageView imageView) {
        if (imageView == null) return;

        Drawable drawable = imageView.getDrawable();
        if (drawable != null && drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
//                Log.d(TAG, "#### bitmap recycled");
            }
        }
        imageView.setImageBitmap(null);
    }
}
