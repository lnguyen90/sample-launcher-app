package com.tecace.retail.util.analytics;

/**
 * Created by icanmobile on 1/30/17.
 */

public enum FragmentActionType {
    TAP("tap"),
    DOUBLE_TAP("double_tap"),
    ZOOM("zoom"),
    SWIPE("swipe"),
    AUTO_SWIPE("auto_swipe"),
    SCROLL("scroll"),
    BACK_PRESSED("back_pressed"),
    FACE_LOOPED("face_looped"),
    TIME_OUT("time_out"),
    DEVICE_ROTATION_ANIMATION("device_rotation_animation"),
    ROTATE_DEVICE("rotate_device"),
    TOUCH("touch"),
    HAMBURGER_MENU_TAP("hamburger_menu_tap"),
    EYE_DETECTED("eye_detected"),
    FACE_DETECTED("face_detected"),
    VIDEO_360("video_360"),
    SHOW_HIDE("show_hide"),
    WRITE_SPEN("write_spen"),
    SPEN_SINGLE_CLICK("spen_single_click"),
    SPEN_DOUBLE_CLICK("spen_double_click"),
    HOVER("hover"),
    DRAG("drag"),
    START_RM_APP("start_rm_app"),
    START_NATIVE_APP("start_native_app"),
    KEYBOARD_ATTACH("keyboard_attach"),
    TYPING("typing");

    String action;
    FragmentActionType(String action) {
        this.action = action;
    }

    public String getActionType() {
        return action;
    }

    public static class Extra {
        public static final String COACH_MARK = "coach_mark";
        public static final String OPEN_MENU = "open_hamburger_menu";
        public static final String CLOSE_MENU = "close_hamburger_menu";
        public static final String SCROLL = "scroll";
        public static final String WATCH = "watch";
        public static final String ROTATE = "rotate";
//        public static final String MANUAL_FOCUS = "manual_focus";
//        public static final String CAPTURE = "take_picture";
//        public static final String IMAGE = "image";
//        public static final String TAP_IMAGE = "tap_image";
//        public static final String ZOOM_IN = "zoom_in";
//        public static final String ZOOM_COUNT = "zoom_count_";
//        public static final String SHORTCUT = "shortcut";
//        public static final String BLUE_TAB = "blue_tab";
//        public static final String MORE_FEEDS = "more_feeds";
//        public static final String CALENDAR = "calendar";
//        public static final String EMAIL = "email";
//        public static final String DOWNLOAD = "download";
//        public static final String PAYMENT = "payment";
//        public static final String TAP_PAYMENT = "tap_payment";
//        public static final String CARDS = "cards";
//        public static final String SWIPE_CARDS_COUNT = "swipe_cards_count_";
//        public static final String SWIPE_CARDS = "swipe_cards";
//        public static final String LAUNCH = "launch";
//        public static final String SCAN_NOW = "scan_now";
//        public static final String HELP_LAYOUT = "help_view";
//        public static final String SPEC = "spec";
//        public static final String APP_ICON = "app_icon";
//        public static final String WATCH = "watch";
//        public static final String BATTERY_ICON = "battery_icon";
    }
}
