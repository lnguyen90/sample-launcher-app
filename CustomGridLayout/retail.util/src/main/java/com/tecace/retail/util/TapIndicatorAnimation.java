package com.tecace.retail.util;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

/**
 * Created by JW on 2017-02-14.
 */

/***************************************************************************************************
        EXAMPLE:
 *             mTapIndicatorAnimation                                                              *
                 .showTapIndicator(x position, y position)
 *               .setOnIndicatorTap(new TapIndicatorAnimation.OnIndicatorTap() {                   *
                         @Override
 *                       public void onIndicatorTap() {                                            *
                             // Implement callback for tap event.
 *                                                                                                 *
                         }
 *               });                                                                               *

 *  AFTER CREATING AND SHOWING ONCE, USE setPosition or setPositionAfter method to show/hide.      *
 ***************************************************************************************************
 *     ATTENTION: Add [ android:clipChildren="false" ] to parent view that contains this view.     *
 ***************************************************************************************************/

public class TapIndicatorAnimation extends RelativeLayout {
    private static final String TAG = TapIndicatorAnimation.class.getSimpleName();

    private static final int INNER_CIRCLE_ANIM_DURATION = 300;
    private static final int OUTER_CIRCLE_ANIM_DURATION = 400;
    private static final int OUTER_CIRCLE_REPEAT_COUNT = 1;

    private OnIndicatorTap mOnIndicatorTap;

    private TimerHandler mShowTimerHandler;

    private ImageButton mInnerButton;
    private ImageButton mOuterButton;

    private ObjectAnimator mInnerCircleAnimXPos;
    private ObjectAnimator mInnerCircleAnimYPos;

    private ObjectAnimator mInnerCircleAnimXReverse;
    private ObjectAnimator mInnerCircleAnimYReverse;

    private ObjectAnimator mOuterCircleAnimXPos;
    private ObjectAnimator mOuterCircleAnimYPos;

    private ObjectAnimator mOuterCircleFadeAnim;

    private AnimatorSet mInnerCircleReverseAnimSet;
    private AnimatorSet mInnerCircleAnimSet;
    private AnimatorSet mOuterCircleAnimSet;

    public TapIndicatorAnimation(Context context) {
        super(context);
        init();
    }

    public TapIndicatorAnimation(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TapIndicatorAnimation(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        init();
    }

    private void init() {
        // Initialize the view.
        this.bringToFront();

        //init position
        this.setX(0);
        this.setY(0);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        killTapIndicator();
    }

    private void initializeInnerButton() {
        if(mInnerButton == null) {
            mInnerButton = new ImageButton(getContext());

            LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            mInnerButton.setLayoutParams(params);

            int resID = getResources().getIdentifier("anim_inner" , "drawable", getContext().getPackageName());

            mInnerButton.setImageResource(resID);
            mInnerButton.setBackgroundColor(Color.TRANSPARENT);
            this.addView(mInnerButton);
        }
    }

    private void initializeOuterButton() {
        if(mOuterButton == null) {
            mOuterButton = new ImageButton(getContext());

            LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            mOuterButton.setLayoutParams(params);

            int resID = getResources().getIdentifier("anim_outer" , "drawable", getContext().getPackageName());

            mOuterButton.setImageResource(resID);
            mOuterButton.setBackgroundColor(Color.TRANSPARENT);
            this.addView(mOuterButton);

            mOuterButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnIndicatorTap != null) {
                        mOnIndicatorTap.onIndicatorTap();
                        hide();
                    }
                }
            });
        }
    }

    private void initializeAnimSets() {
        if (mInnerCircleReverseAnimSet == null)
            mInnerCircleReverseAnimSet = new AnimatorSet();

        if (mInnerCircleAnimSet == null) {
            mInnerCircleAnimSet = new AnimatorSet();
            mInnerCircleReverseAnimSet.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (mInnerCircleAnimSet != null)
                        mInnerCircleAnimSet.start();

                    if (mOuterCircleAnimSet != null)
                        mOuterCircleAnimSet.start();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }
        if (mOuterCircleAnimSet == null) {
            mOuterCircleAnimSet = new AnimatorSet();
            mOuterCircleAnimSet.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    if (mOuterButton != null)
                        mOuterButton.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (mInnerCircleReverseAnimSet != null)
                        mInnerCircleReverseAnimSet.start();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }
    }

    public void hide() {
        this.setVisibility(GONE);
    }

    public void setPosition(int xPos, int yPos) {
        this.measure(0, 0);
        this.setX(Math.abs(xPos - this.getMeasuredWidth()/2));
        this.setY(Math.abs(yPos - this.getMeasuredHeight()/2));
        this.setVisibility(VISIBLE);
        this.bringToFront();
    }

    public void setPositionAfter(final int xPos, final int yPos, int duration) {
        if (mShowTimerHandler == null)
            mShowTimerHandler = new TimerHandler();

        mShowTimerHandler.setOnTimeoutListener(new TimerHandler.OnTimeoutListener() {
            @Override
            public void onTimeout() {
                setPosition(xPos, yPos);
            }
        });
        mShowTimerHandler.start(duration);
    }

    /**
     * Show after some duration.
     * */
    public TapIndicatorAnimation showAnimationAfter(final float xPos, final float yPos, int duration) {
        if (mShowTimerHandler == null)
            mShowTimerHandler = new TimerHandler();

        mShowTimerHandler.setOnTimeoutListener(new TimerHandler.OnTimeoutListener() {
            @Override
            public void onTimeout() {
                showTapIndicator(xPos, yPos);
            }
        });

        mShowTimerHandler.start(duration);
        return this;
    }

    public TapIndicatorAnimation showTapIndicator() {
        return showTapIndicator(this.getX(), this.getY());
    }

    public TapIndicatorAnimation showTapIndicator(float xPos, float yPos) {
        if(xPos < 0 || yPos < 0) {
            throw new IllegalArgumentException("X position and Y position cannot be less than zero.");
        }
        this.setVisibility(VISIBLE);

        initializeInnerButton();
        initializeOuterButton();
        initializeAnimSets();

        this.setX(xPos);
        this.setY(yPos);

        if (mInnerCircleAnimXPos == null) {
            mInnerCircleAnimXPos = ObjectAnimator.ofFloat(mInnerButton, "scaleX", 0.9f, 1.5f);
            mInnerCircleAnimXPos.setDuration(INNER_CIRCLE_ANIM_DURATION);
        }

        if (mInnerCircleAnimYPos == null) {
            mInnerCircleAnimYPos = ObjectAnimator.ofFloat(mInnerButton, "scaleY", 0.9f, 1.5f);
            mInnerCircleAnimYPos.setDuration(INNER_CIRCLE_ANIM_DURATION);
        }

        if (mInnerCircleAnimXReverse == null) {
            mInnerCircleAnimXReverse = ObjectAnimator.ofFloat(mInnerButton, "scaleX", 1.5f, 0.9f);
            mInnerCircleAnimXReverse.setDuration(INNER_CIRCLE_ANIM_DURATION);
        }

        if (mInnerCircleAnimYReverse == null) {
            mInnerCircleAnimYReverse = ObjectAnimator.ofFloat(mInnerButton, "scaleY", 1.5f, 0.9f);
            mInnerCircleAnimYReverse.setDuration(INNER_CIRCLE_ANIM_DURATION);
        }

        if (mOuterCircleAnimXPos == null) {
            mOuterCircleAnimXPos = ObjectAnimator.ofFloat(mOuterButton, "scaleX", 0.7f, 2.0f);
            mOuterCircleAnimXPos.setDuration(OUTER_CIRCLE_ANIM_DURATION);
            mOuterCircleAnimXPos.setRepeatCount(OUTER_CIRCLE_REPEAT_COUNT);
            mOuterCircleAnimXPos.setRepeatMode(ValueAnimator.RESTART);
        }

        if (mOuterCircleAnimYPos == null) {
            mOuterCircleAnimYPos = ObjectAnimator.ofFloat(mOuterButton, "scaleY", 0.7f, 2.0f);
            mOuterCircleAnimYPos.setDuration(OUTER_CIRCLE_ANIM_DURATION);
            mOuterCircleAnimYPos.setRepeatCount(OUTER_CIRCLE_REPEAT_COUNT);
            mOuterCircleAnimYPos.setRepeatMode(ValueAnimator.RESTART);
        }

        if (mOuterCircleFadeAnim == null) {
            mOuterCircleFadeAnim = ObjectAnimator.ofFloat(mOuterButton, "alpha", 1.0f, 0.0f);
            mOuterCircleFadeAnim.setDuration(OUTER_CIRCLE_ANIM_DURATION);
            mOuterCircleFadeAnim.setRepeatCount(OUTER_CIRCLE_REPEAT_COUNT);
            mOuterCircleFadeAnim.setRepeatMode(ValueAnimator.RESTART);
        }

        mInnerCircleAnimSet.playTogether(mInnerCircleAnimXPos, mInnerCircleAnimYPos);
        mOuterCircleAnimSet.playTogether(mOuterCircleAnimXPos, mOuterCircleAnimYPos, mOuterCircleFadeAnim);
        mInnerCircleReverseAnimSet.playTogether(mInnerCircleAnimXReverse, mInnerCircleAnimYReverse);

        mInnerCircleAnimSet.start();
        mOuterCircleAnimSet.start();

        return this;
    }

    public void killTapIndicator() {
        if (mOnIndicatorTap != null)
            mOnIndicatorTap = null;
        if (mInnerButton != null)
            mInnerButton = null;
        if (mOuterButton != null)
            mOuterButton = null;
        if (mInnerCircleAnimXPos != null) {
            mInnerCircleAnimXPos.cancel();
            mInnerCircleAnimXPos = null;
        }
        if (mInnerCircleAnimYPos != null) {
            mInnerCircleAnimYPos.cancel();
            mInnerCircleAnimYPos = null;
        }
        if (mInnerCircleAnimXReverse != null) {
            mInnerCircleAnimXReverse.cancel();
            mInnerCircleAnimXReverse = null;
        }
        if (mInnerCircleAnimYReverse != null) {
            mInnerCircleAnimYReverse.cancel();
            mInnerCircleAnimYReverse = null;
        }
        if (mOuterCircleAnimXPos != null) {
            mOuterCircleAnimXPos.cancel();
            mOuterCircleAnimXPos = null;
        }
        if (mOuterCircleAnimYPos != null) {
            mOuterCircleAnimYPos.cancel();
            mOuterCircleAnimYPos = null;
        }
        if (mOuterCircleFadeAnim != null) {
            mOuterCircleFadeAnim.cancel();
            mOuterCircleFadeAnim = null;
        }
        if (mInnerCircleAnimSet != null) {
            mInnerCircleAnimSet.cancel();
            mInnerCircleAnimSet = null;
        }
        if (mOuterCircleAnimSet != null) {
            mOuterCircleAnimSet.cancel();
            mOuterCircleAnimSet = null;
        }
        if (mInnerCircleReverseAnimSet != null) {
            mInnerCircleReverseAnimSet.cancel();
            mInnerCircleReverseAnimSet = null;
        }
        if (mShowTimerHandler!= null) {
            mShowTimerHandler.stop();
            mShowTimerHandler = null;
        }
    }

    public TapIndicatorAnimation setOnIndicatorTap(TapIndicatorAnimation.OnIndicatorTap callback) {
        this.mOnIndicatorTap = callback;
        return this;
    }

    public interface OnIndicatorTap {
        void onIndicatorTap();
    }
}