package com.tecace.retail.util;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.common.io.Files;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by icanmobile on 3/4/16.
 */
public class FileUtil {
    private static final String TAG = FileUtil.class.getSimpleName();

    private static volatile FileUtil sInstance = null;
    public static FileUtil getInstance() {
        if (sInstance == null) {
            synchronized (FileUtil.class) {
                if (sInstance == null)
                    sInstance = new FileUtil();
            }
        }
        return sInstance;
    }


    private FileUtil() {}

    public String getFolderPath(String filePath) {
        if (filePath == null || filePath.length() == 0) return null;
        return filePath.substring(0, filePath.lastIndexOf("/")+1);
    }

    public static String getFileFullPath(Context context, String fileName){
        if (context == null || fileName == null || fileName.length() == 0) return null;
        final String dir = context.getExternalFilesDir(null).toString();

        if (fileName.contains(dir)) return fileName;

        return String.format("%s/%s", dir, fileName);
    }

    public static boolean exist(String path) {
        File file = new File(path);
        return file.exists();
    }

    public void moveFiles(String fromPath, String toPath) throws IOException {
        final File fromDir = new File(fromPath);
        final File toDir = new File(toPath);

        if (!toDir.exists()) {
            toDir.mkdirs();
        }

        for (File fromFile : fromDir.listFiles()) {
            String toFilePath = toPath + File.separator + fromFile.getName();
            if (fromFile.isDirectory()) {
                moveFiles(fromFile.getAbsolutePath(), toFilePath);
            } else {
                File toFile = new File(toFilePath);
                toFile.delete();
                Files.move(fromFile, toFile);
            }
        }

        fromDir.delete();
    }

    public void makeFolder(String path) {
        File dir = new File(path);
        if (dir != null) {
            dir.mkdirs();
        }
    }

    public static File createSubFolder(Context context, String subFolderName){
        final String dir = context.getExternalFilesDir(null).toString();
        File subFolder = new File(String.format("%s/%s", dir, subFolderName));
        if (!subFolder.exists()) {
            subFolder.mkdir();
        }
        return subFolder;
    }

    public long getLastModifiedTime(Context context, String filename) {
        if (context == null || filename == null || filename.isEmpty()) {
            return 0;
        }

        String fullFilePath = getFileFullPath(context, filename);
        File file = new File(fullFilePath);
        if (!file.exists()) return 0;

        return file.lastModified();
    }

    public boolean setLastModifiedTime(Context context, String filename, long changeTime) {
        if (context == null || filename == null || filename.isEmpty()) {
            return false;
        }

        String fullFilePath = getFileFullPath(context, filename);
        File file = new File(fullFilePath);
        if (!file.exists()) return false;

        return file.setLastModified(changeTime);
    }





    /**
     *  unzip files : for frame, bg, chapter directors
     *  @param context
     *  @param forUnzipList zip file name
     *  @param supportedLanguageList folder names depending on Environment.json
     */
    public void startUnzipFiles(Context context, String[] forUnzipList, ArrayList<String> supportedLanguageList) {
        // In case of default, find one more time at the last.
        int size = supportedLanguageList.size() + 1;

        for(int i = 0; i < size; i++) {
            String folderName = "";
            if(i != supportedLanguageList.size()) {
                folderName = String.format("/%s", supportedLanguageList.get(i));
            }

            for (String ZipFileName : forUnzipList) {
                try {
                    //Log.d(TAG, "@@@ zip filename : " + ZipFileName);

                    final String fullFilePath = String.format("%s%s/%s",
                            context.getExternalFilesDir(null).toString(), folderName, ZipFileName);

                    File zipFile = new File(fullFilePath);
//                    Log.d(TAG, "@@@### zip filename : " + zipFile.toString());
                    if (zipFile.exists()) {
//                        Log.d(TAG, "@@@### start unzip : " + fullFilePath);
                        UnzipTask unzipTask = new UnzipTask();
                        unzipTask.execute(fullFilePath);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class UnzipTask extends AsyncTask<String, Void, Boolean> {
        public UnzipTask() {}

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                Log.d(TAG, "@@@ doInBackground+) " + params[0]);
                if (params[0] == null) return false;

                String zipFilePath = params[0];

                if (FileUtil.getInstance().unzipToFolder(zipFilePath)) {
                    File deletingZip = new File(zipFilePath);
                    Log.d(TAG, "@@@ starting delete zipfile");
                    if (deletingZip != null) {
                        Log.d(TAG, "@@@ I have a zipfile");
                        if (deletingZip.exists()) {
                            Log.d(TAG, "@@@ deleting zip file");
                            deletingZip.delete();
                        }
                    }
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            Log.d(TAG, "@@@@@ Finished unzip");
        }
    }

    static int BUFFER_SIZE = 2048;
    public boolean unzipToFolder(String zipFile) throws IOException {
        if (zipFile == null || zipFile.length() == 0) return false;

        int size;
        byte[] buffer = new byte[BUFFER_SIZE];

        try {
            String location = getFolderPath(zipFile);
            ZipInputStream zin = new ZipInputStream(
                    new BufferedInputStream(new FileInputStream(zipFile), BUFFER_SIZE));
            try {
                ZipEntry ze = null;
                while ((ze = zin.getNextEntry()) != null) {
                    String path = location + ze.getName();
                    File unzipFile = new File(path);

                    if (ze.isDirectory()) {
                        if (!unzipFile.isDirectory()) {
                            unzipFile.mkdirs();
                        }
                    } else {
                        // check for and create parent directories if they don't exist
                        File parentDir = unzipFile.getParentFile();
                        if (null != parentDir) {
                            if (!parentDir.isDirectory()) {
                                parentDir.mkdirs();
                            }
                        }

                        // unzip the file
                        FileOutputStream out = new FileOutputStream(unzipFile, false);
                        BufferedOutputStream fout = new BufferedOutputStream(out, BUFFER_SIZE);
                        try {
                            while ((size = zin.read(buffer, 0, BUFFER_SIZE)) != -1) {
                                fout.write(buffer, 0, size);
                            }
                            zin.closeEntry();
                        } finally {
                            fout.flush();
                            fout.close();
                        }
                    }
                }
            } finally {
                zin.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    //[MOVE_TO_RESMANAGER] - Keep this for config (EnvironmentManager)
    //[TA] 11/10/2017 - Find in language with country -> language -> default folder
//    private final static int SEARCH_FOLDER_LANG_COUNTRY = 0;
//    private final static int SEARCH_FOLDER_LANG = 1;
//    private final static int SEARCH_FOLDER_ROOT = 2;
//
//    public static String getFileFullPathWithLocale(Context context, String fileName) {
//        if (context == null || fileName == null || fileName.length() == 0) return null;
//
//        String language = Locale.getDefault().getLanguage();
//        String country = Locale.getDefault().getCountry();
//        String languageAndCountry = String.format("%s-r%s", language, country);
//
//        ArrayList<String> filePaths = new ArrayList<String>();
//
//        filePaths.add(SEARCH_FOLDER_LANG_COUNTRY, getFileFullPath(context, String.format("%s/%s", languageAndCountry, fileName)));
//        filePaths.add(SEARCH_FOLDER_LANG, getFileFullPath(context, String.format("%s/%s", language, fileName)));
//        filePaths.add(SEARCH_FOLDER_ROOT, getFileFullPath(context, fileName));
//
//        for (int i = 0; i < filePaths.size(); i++) {
//            if (exist(filePaths.get(i))) {
//                return filePaths.get(i);
//            }
//        }
//        return filePaths.get(filePaths.size()-1); //If File does not exist, return default folder path.
//    }

}

