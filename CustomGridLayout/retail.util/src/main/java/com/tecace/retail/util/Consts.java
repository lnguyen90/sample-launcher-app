package com.tecace.retail.util;

/**
 * Created by icanmobile on 1/17/17.
 */

public class Consts {

    public static final String ACTION_CHANGE_FRAGMENT = "com.tecace.retail.scheduler.receiver.action.CHANGE_FRAGMENT";
    public static final String ARG_PREVIOUS_FRAGMENT = "ARG_PREVIOUS_FRAGMENT";
    public static final String ARG_NEXT_FRAGMENT = "ARG_NEXT_FRAGMENT";
    public static final String ARG_CHANGE_CAUSE = "ARG_CHANGE_CAUSE";
    public static final String ARG_INDEX = "ARG_INDEX";
    public static final String SPEC_WIDGET = "SPEC_WIDGET";
    public static final String APP_WIDGET = "APP_WIDGET";
    public static final String NONE = "NONE";


    public static final String PREFERENCE_APP_PACKAGE = "PREFERENCE_APP_PACKAGE";
    public static final String PREFERENCE_APP_CLASS = "PREFERENCE_APP_CLASS";
    public static final String PREFERENCE_APP_TITLE = "PREFERENCE_APP_TITLE";
    public static final String PREFERENCE_APP_SELF_FINISH = "PREFERENCE_APP_SELF_FINISH";
    public static final String PREFERENCE_ANALYTICS_STORE_ID = "PREFERENCE_ANALYTICS_STORE_ID";
    public static final String PREFERENCE_ANALYTICS_DEVICE_ID = "PREFERENCE_ANALYTICS_DEVICE_ID";
    public static final String PREFERENCE_ANALYTICS_CHANNEL_NAME = "PREFERENCE_ANALYTICS_CHANNEL_NAME";

    //[[ Added by SMHEO (03/26/2018) - SCREEN_SAVER
    public static final String PREFERENCE_APP_ENABLE_SCREEN_SAVER = "PREFERENCE_APP_ENABLE_SCREEN_SAVER";
    //]]

    public static final String FRAGMENT_KEY = "FRAGMENT_KEY";
    public static final String VIDEO_TIME = "VIDEO_TIME";
    public static final String FRAGMENT_BUNDLE_KEY = "FRAGMENT_BUNDLE_KEY";

    // ForegroundDetectService
    public static final String ACTION_FOREGROUND_DETECTED = "com.tecace.retail.remoteservice.receiver.action.FOREGROUND_DETECTED";
    public static final String ARG_DETECTED_FOREGROUND = "ARG_DETECTED_FOREGROUND";
}
