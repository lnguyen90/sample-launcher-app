package com.tecace.retail.util;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by icanmobile on 7/10/17.
 */

public class ExecutableQueue {
    private static final String TAG = ExecutableQueue.class.getName();

    private static ThreadPoolExecutor mExecutor;
    private static volatile ExecutableQueue mInstance;
    private final long KIPE_ALIVE_TIME = 0L;
    private final int CORE_POOL_SIZE = Runtime.getRuntime().availableProcessors();
    private final int MAXIMUM_POOL_SIZE = Runtime.getRuntime().availableProcessors();

    public static ExecutableQueue getInstance() {
        if( mInstance == null ) {
            synchronized (ExecutableQueue.class) {
                if (mInstance == null)
                    mInstance = new ExecutableQueue();
            }
        }
        return mInstance;
    }

    private ExecutableQueue() {
        mExecutor = new ThreadPoolExecutor( CORE_POOL_SIZE,
                MAXIMUM_POOL_SIZE,
                KIPE_ALIVE_TIME,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingDeque<Runnable>());
    }

    public void execute(Runnable task) {
        mExecutor.execute( task );
    }

    public void shutdown() {
        mExecutor.shutdownNow();
    }
}