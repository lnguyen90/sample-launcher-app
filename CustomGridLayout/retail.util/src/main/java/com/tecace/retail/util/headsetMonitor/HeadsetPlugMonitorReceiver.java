package com.tecace.retail.util.headsetMonitor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.common.base.Strings;

/**
 * Created by smheo on 6/2/2016.
 */
public class HeadsetPlugMonitorReceiver extends BroadcastReceiver {
    private static final String TAG = HeadsetPlugMonitorReceiver.class.getSimpleName();

    private onUpdateHeadsetPluginListener mListener;

    private static volatile HeadsetPlugMonitorReceiver sInstance;
    public static HeadsetPlugMonitorReceiver getInstance() {
        if (sInstance == null) {
            synchronized (HeadsetPlugMonitorReceiver.class) {
                if (sInstance == null)
                    sInstance = new HeadsetPlugMonitorReceiver();
            }
        }
        return sInstance;
    }
    private HeadsetPlugMonitorReceiver() {
    }

    public interface onUpdateHeadsetPluginListener{
        void onUpdateHeadsetPluginStatus();
    }

    public void setOnUpdateHeadsetPluginListener(onUpdateHeadsetPluginListener listener) {
        mListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        if (Strings.isNullOrEmpty(action)) return;

        if (action.equals(Intent.ACTION_HEADSET_PLUG) ||
                action.equals(BluetoothDevice.ACTION_ACL_CONNECTED) ||
                action.equals(BluetoothDevice.ACTION_ACL_DISCONNECTED) ||
                action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {

            mListener.onUpdateHeadsetPluginStatus();
        }
    }
}
