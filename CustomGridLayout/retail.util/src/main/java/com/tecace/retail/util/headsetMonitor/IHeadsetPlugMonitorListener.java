package com.tecace.retail.util.headsetMonitor;

/**
 * Created by smheo on 11/9/2017.
 */

public interface IHeadsetPlugMonitorListener {
    public void onChangedHeadsetPlugStatus();
}
