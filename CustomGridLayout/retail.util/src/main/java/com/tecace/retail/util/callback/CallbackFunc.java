package com.tecace.retail.util.callback;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by icanmobile on 6/24/16.
 */
public class CallbackFunc {
    private static final String TAG = CallbackFunc.class.getSimpleName();

    public Object obj;
    public String methodName;
    public Class<?>[] paramTypes;
    public Object[] parameters;
    public Object result;
    public boolean selfInvokeNextMethod;
    public int delayTime;


    public static class Builder {
        public Object obj;
        public String methodName;

        public Class<?>[] paramTypes =  new Class[]{};
        public Object[] parameters = new Object[]{};
        public boolean selfInvokeNextMethod = false;
        public int delayTime = 0;
        public CallbackFuncListener listener;

        public Builder(Object obj, String methodName) {
            this.obj = obj;
            this.methodName = methodName;
        }

        public Builder paramTypes(Class<?>[] paramTypes) {
            this.paramTypes = paramTypes;
            return this;
        }

        public Builder parameters(Object[] parameters) {
            this.parameters = parameters;
            return this;
        }

        public Builder selfInvokeNextMethod(boolean selfInvokeNextMethod) {
            this.selfInvokeNextMethod = selfInvokeNextMethod;
            return this;
        }

        public Builder delayTime(int delayTime) {
            this.delayTime = delayTime;
            return this;
        }

        public Builder listener(CallbackFuncListener listener) {
            this.listener = listener;
            return this;
        }

        public CallbackFunc build() {
            return new CallbackFunc(this);
        }
    }

    public CallbackFunc(Builder builder) {
        this.obj = builder.obj;
        this.methodName = builder.methodName;
        this.paramTypes = builder.paramTypes;
        this.parameters = builder.parameters;
        this.selfInvokeNextMethod = builder.selfInvokeNextMethod;
        this.delayTime = builder.delayTime;
        this.mListener = builder.listener;
    }

    public interface CallbackFuncListener {
        void onPrepare(CallbackFunc callbackFunc);
        void onDone(CallbackFunc callbackFunc, Object result);

    }
    public CallbackFuncListener mListener = null;
    public void setListener(CallbackFuncListener listener) {
        mListener = listener;
    }


    public interface LocalListener {
        void selfInvokeNextMethod();
    }
    public LocalListener mLocalListener = null;
    public void setLocalListener(LocalListener listener) {
        mLocalListener = listener;
    }


    public void invokeMethod() {
        if (delayTime > 0) {
            handler.sendEmptyMessageDelayed(MSG_INVOKE_METHOD, delayTime);
            return;
        }

        try {
            if (mListener != null)
                mListener.onPrepare(this);

            Log.d(TAG, "##### methodName = " + methodName + ", selfInvokeNextMethod = " + this.selfInvokeNextMethod);

            if (this.methodName != null && this.methodName.length() > 0) {
                Method method = this.obj.getClass().getMethod(this.methodName, this.paramTypes);
                this.result = (Object) method.invoke(this.obj, this.parameters);
            }

            if (mListener != null)
                mListener.onDone(this, this.result);

            if (this.selfInvokeNextMethod && mLocalListener != null)
                mLocalListener.selfInvokeNextMethod();

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private static final int MSG_INVOKE_METHOD = 1;
    private static final int MSG_INVOKE_METHOD_WITH_PARAMETER = 2;
    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_INVOKE_METHOD: {
                    delayTime = 0;
                    invokeMethod();
                }
                break;
            }
        }
    };

    public void reset() {
        handler.removeCallbacksAndMessages(null);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        if (this.obj != null)
            appendString(builder, "obj = " + this.obj);
        if (this.methodName != null)
            appendString(builder, "methodName = " + this.methodName);
        if (this.paramTypes != null) {
            appendString(builder, "[");
            for (Class<?> paramType : paramTypes)
                appendString(builder, "paramType = " + paramType);
            appendString(builder, "]");
        }
        if (this.parameters != null) {
            appendString(builder, "[");
            for (Object parameter : parameters)
                appendString(builder, "parameter = " + parameter);
            appendString(builder, "]");

        }
        appendString(builder, "selfInvokeNextMethod = " + this.selfInvokeNextMethod);
        appendString(builder, "delayTime = " + this.delayTime);

        return builder.toString();
    }

    public void appendString(StringBuilder builder, String value) {
        builder.append(value + System.getProperty("line.separator"));
    }

    public void print() {
        Log.d(CallbackFunc.class.getSimpleName(), toString());
    }
}
