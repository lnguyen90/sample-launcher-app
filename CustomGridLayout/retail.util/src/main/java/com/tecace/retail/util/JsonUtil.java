package com.tecace.retail.util;

import android.content.Context;
import android.util.Log;

import com.google.common.base.Strings;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Type;

/**
 * Created by icanmobile on 3/1/16.
 */
public class JsonUtil {
    private static final String TAG = JsonUtil.class.getSimpleName();

    private static volatile JsonUtil sInstance = null;
    public static JsonUtil getInstance() {
        if (sInstance == null) {
            synchronized (JsonUtil.class) {
                if (sInstance == null)
                    sInstance = new JsonUtil();
            }
        }
        return sInstance;
    }
    private JsonUtil() {}

    //[MOVE_TO_RESMANAGER] - Keep "getJsonText" for Environment Manager
//   public String getJsonText(Context context, String json) {
//        String text = getText(context, json);
//        if (Strings.isNullOrEmpty(text)) {
//            //[MOVE_TO_RESMANAGER]
//            text = AssetUtil.getInstance().GetTextFromAsset(context, json);
//        }
//        return text;
//    }
//
//    public <T> T loadJsonModel(Context context, String json, Class<T> classOfT) {
//        String data = getJsonText(context, json);
//        if (Strings.isNullOrEmpty(data)) {
//            return null;
//        }
//
//        Gson gson = new Gson();
//        try {
//            return gson.fromJson(data, classOfT);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    public <T> T loadJsonModel(Context context, String json, Type type) {
//        String data = getJsonText(context, json);
//        if (Strings.isNullOrEmpty(data)) {
//            return null;
//        }
//
//        Gson gson = new Gson();
//        try {
//            return gson.fromJson(data, type);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return null;
//    }
//
//    private String getText(Context context, String filename) {
//        StringBuilder ret = new StringBuilder();
//
//        String fullFilePath = FileUtil.getFileFullPathWithLocale(context, filename);
//        boolean fileExist = FileUtil.exist(fullFilePath);
//        if (!fileExist) {
//            Log.d(TAG, "File " + filename + " is not available in files folder");
//            return "";
//        }
//
//        try (BufferedReader br = new BufferedReader(new FileReader(fullFilePath))) {
//            String line = br.readLine();
//
//            while (line != null) {
//                ret.append(line);
//                line = br.readLine();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return ret.toString();
//    }
}
