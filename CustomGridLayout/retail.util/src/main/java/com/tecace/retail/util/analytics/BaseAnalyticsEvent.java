package com.tecace.retail.util.analytics;

/**
 * Created by icanmobile on 3/4/17.
 */

public interface BaseAnalyticsEvent {
    void onScreenChangeEvent(String changeType, String toScreen);
    void onUserEvent(String action, String extra);
}
