package com.tecace.retail.util.analytics;

import android.content.Context;
import android.content.ContextWrapper;
import android.util.Log;

/**
 * Created by icanmobile on 3/4/17.
 */

public class AnalyticsEvent {
    private static final String TAG = AnalyticsEvent.class.getSimpleName();

    private static volatile AnalyticsEvent sInstance = null;
    public static AnalyticsEvent getInstance() {
        if (sInstance == null) {
            synchronized (AnalyticsEvent.class) {
                if (sInstance == null)
                    sInstance = new AnalyticsEvent();
            }
        }
        return sInstance;
    }
    private AnalyticsEvent() {}

    private BaseAnalyticsEvent getInterface(Context context) {
        if (context == null) return null;
        try {
            if (context instanceof BaseAnalyticsEvent)
                return (BaseAnalyticsEvent) context;
            context = ((ContextWrapper) context).getBaseContext();
            return getInterface(context);
        }catch (Exception e) {
            Log.d(TAG, "##### getInterface : Can't find an activity which implements IAnalyticsEvent interface");
            e.printStackTrace();
            return null;
        }
    }

    public void screenChangeEvent(Context context, String changeType, String toScreen) {
        BaseAnalyticsEvent event = getInterface(context);
        if (event != null) event.onScreenChangeEvent(changeType, toScreen);
    }

    public void userEvent(Context context, String action, String extra) {
        BaseAnalyticsEvent event = getInterface(context);
        if (event != null) event.onUserEvent(action, extra);
    }
}
