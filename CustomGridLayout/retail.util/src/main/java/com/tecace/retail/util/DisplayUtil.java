package com.tecace.retail.util;

import android.app.ActionBar;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import static android.provider.Settings.System;

/**
 * Created by smheo on 1/25/2017.
 */

public class DisplayUtil {
    private static final String TAG = DisplayUtil.class.getSimpleName();

    /**
     * Screen Modes : These values are greater than zero.
     */

    // full screen modes
    public static final int FULLSCREEN_NORMAL_MODE = 0x00;
    public static final int FULLSCREEN_OVERLAY_NAVI_HIDE_STATUS_MODE = 0x01;
    public static final int FULLSCREEN_TRANSPARENT_OVERLAY_NAVI_HIDE_STATUS_MODE = 0x02;
    public static final int FULLSCREEN_HIDE_NAVI_OVERLAY_STATUS_MODE = 0x03;
    public static final int FULLSCREEN_HIDE_NAVI_TRANSPARENT_OVERLAY_STATUS_MODE = 0x04;
    public static final int FULLSCREEN_BOTH_OVERLAY_SYSTEM_BAR_MODE = 0x05;
    public static final int FULLSCREEN_BOTH_TRANSPARENT_OVERLAY_SYSTEM_BAR_MODE = 0x06;
    public static final int FULLSCREEN_OVERLAY_NAVI_TRANSPARENT_OVERLAY_STATUS_IMMERSIVE = 0x07;
    public static final int FULLSCREEN_BOTH_TRANSPARENT_HIDE_IMMERSIVE = 0x08;

    // non-full screen mode
    public static final int NON_FULLSCREEN_NORMAL_MODE = 0x10;
    public static final int NON_FULLSCREEN_SHOW_NAVI_HIDE_STATUS_MODE = 0x20;



    // save screen mode
    private static int mWhatScreenMode = -1;
    private static boolean mKeepScreenOnMode;
    private static boolean mHideWindowTitle;
    private static boolean mShowActionBar;
    private static boolean mTransparencyNavigationBar;
    private static boolean mTransparencyStatusBar;



    private DisplayUtil() {}

    private static volatile DisplayUtil sInstance = null;
    public static DisplayUtil getInstance() {
        if (sInstance == null) {
            synchronized (DisplayUtil.class) {
                if (sInstance == null)
                    sInstance = new DisplayUtil();
            }
        }
        return sInstance;
    }

    /**
     * Set the keep screen on mode
     * @param activity
     * @param on true - set the keep screen mode
     *           false - clear the keep screen mode
     * @return true if successful, false if an error occurs
     */
    public boolean setKeepScreenOnMode(Activity activity, boolean on) {
        if (activity == null) return false;

        mKeepScreenOnMode = on;
        if (on) {
            activity.getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
            );
//            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
//                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
//                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
//                    WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
//            );
        } else {
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        return true;
    }

    public boolean isKeepScreenOnMode() {
        return mKeepScreenOnMode;
    }

    //[[ START : Modified and Added by SMHEO (03/26/2018) - SCREEN_SAVER
    /**
     * setAppShowWhenScreenIsLocked
     * For only under API 26
     * @param activity
     * @param on
     */
    public void setAppShowWhenScreenIsLocked(Activity activity, boolean on) {
        if (activity == null) return;

        if (on) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                activity.getWindow().addFlags(
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            }
        } else {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                activity.getWindow().clearFlags(
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            }
        }
    }

    /**
     * Request to dismiss keyguard at Android API level 26+
     * @param activity Activity
     * @return none
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void requestDismissKeyguard(Activity activity) {
        if (activity == null) return;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            Log.w(TAG, "##### This method is not supported in this SDK " + Build.VERSION.SDK_INT);
        } else {
            KeyguardManager keyguardManager = (KeyguardManager) activity.getSystemService(Context.KEYGUARD_SERVICE);
            if (keyguardManager == null) {
                Log.w(TAG, "#### Keyguard manager is null");
                return;
            }
            if (keyguardManager.isKeyguardLocked()) {
                Log.d(TAG, "##@@## Requested dismiss keyguard ");
                keyguardManager.requestDismissKeyguard(activity,null);
//                keyguardManager.requestDismissKeyguard(activity, new KeyguardManager.KeyguardDismissCallback() {
//                    @Override
//                    public void onDismissError() {
//                        super.onDismissError();
//                        Log.d(TAG, "##@@## Dismiss keyguard error.");
//                    }
//
//                    @Override
//                    public void onDismissSucceeded() {
//                        super.onDismissSucceeded();
//                        Log.d(TAG, "##### Succeeded the request dismiss keyguard.");
//                    }
//
//                    @Override
//                    public void onDismissCancelled() {
//                        super.onDismissCancelled();
//                        Log.d(TAG, "##@@## Cancelled the request dismiss keyguard.");
//                    }
//                });
            }
        }
    }
    //]] END - SCREEN_SAVER

    /**
     * Hide the window title (no status bar).
     * This is same 'android:windowNoTitle=true' in android style.
     * @param activity Activity
     * @return true if successful, false if an error occurs
     */
    public boolean hideWindowTitle(Activity activity) {
        if (activity == null) return false;

        mHideWindowTitle = true;
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return true;
    }

    public boolean isHideWindowTitle() {
        return mHideWindowTitle;
    }

    /**
     * Set fullscreen mode on the activity.
     * This is same android:windowFullscreen in android style.
     * @param activity Activity
     * @param hasFullScreen true is set to the window fullscreen
     *                      false is to disable the window fullscreen
     * @return true if successful, false if an error occurs
     */
    public boolean setWindowFullScreenMode(Activity activity, boolean hasFullScreen) {
        if (activity == null) return false;

        if (hasFullScreen) {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        return true;
    }

    /**
     * Sets several screen mode
     * @param activity Activity
     * @param whatScreenMode
     *          --> Non-Fullscreen modes
     *          NON_FULLSCREEN_NORMAL_MODE = 0x10;
     *          NON_FULLSCREEN_SHOW_NAVI_HIDE_STATUS_MODE = 0x20;
     *
     *          --> Fullscreen modes
     *          FULLSCREEN_NORMAL_MODE = 0x00;
     *          FULLSCREEN_OVERLAY_NAVI_HIDE_STATUS_MODE = 0x01;
     *          FULLSCREEN_TRANSPARENT_OVERLAY_NAVI_HIDE_STATUS_MODE = 0x02;
     *          FULLSCREEN_HIDE_NAVI_OVERLAY_STATUS_MODE = 0x03;
     *          FULLSCREEN_HIDE_NAVI_TRANSPARENT_OVERLAY_STATUS_MODE = 0x04;
     *          FULLSCREEN_BOTH_OVERLAY_SYSTEM_BAR_MODE = 0x05;
     *          FULLSCREEN_BOTH_TRANSPARENT_OVERLAY_SYSTEM_BAR_MODE = 0x06;
     *          FULLSCREEN_OVERLAY_NAVI_TRANSPARENT_OVERLAY_STATUS_IMMERSIVE = 0x07;
     *          FULLSCREEN_BOTH_TRANSPARENT_HIDE_IMMERSIVE = 0x08;
     *
     *
     * @return true if successful, false if an error occurs
     */
    public boolean setScreenMode(Activity activity, int whatScreenMode) {
        if (activity == null) return false;

        View decorView = activity.getWindow().getDecorView();
        if (decorView == null) return false;

        //------------------------------------------------------------------------
        // IMPORTANT : DO NOT REMOVE THIS CODE - clear flag of the window fullscreen
        //------------------------------------------------------------------------
        this.setWindowFullScreenMode(activity, false);

        int uiVisibility;
        switch (whatScreenMode) {
            case NON_FULLSCREEN_NORMAL_MODE:
                uiVisibility = View.SYSTEM_UI_FLAG_VISIBLE;

                // set transparency of navigation bar and status bar
                setTransparencyStatusBar(activity, false);
                setTransparencyNavigationBar(activity, false);
                break;

            case NON_FULLSCREEN_SHOW_NAVI_HIDE_STATUS_MODE:
                uiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

                // set transparency of navigation bar and status bar
                setTransparencyStatusBar(activity, false);
                setTransparencyNavigationBar(activity, false);
                break;

            case FULLSCREEN_NORMAL_MODE:
                uiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

                // set transparency of navigation bar and status bar
                setTransparencyStatusBar(activity, false);
                setTransparencyNavigationBar(activity, false);
                break;

            case FULLSCREEN_OVERLAY_NAVI_HIDE_STATUS_MODE:
            case FULLSCREEN_TRANSPARENT_OVERLAY_NAVI_HIDE_STATUS_MODE:
                uiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

                // set transparency of navigation bar and status bar
                if (whatScreenMode == FULLSCREEN_TRANSPARENT_OVERLAY_NAVI_HIDE_STATUS_MODE) {
                    setTransparencyStatusBar(activity, false);
                    setTransparencyNavigationBar(activity, true);
                } else {
                    setTransparencyStatusBar(activity, false);
                    setTransparencyNavigationBar(activity, false);
                }
                break;
            case FULLSCREEN_HIDE_NAVI_OVERLAY_STATUS_MODE:
            case FULLSCREEN_HIDE_NAVI_TRANSPARENT_OVERLAY_STATUS_MODE:
                uiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

                // set transparency of navigation bar and status bar
                if (whatScreenMode == FULLSCREEN_HIDE_NAVI_TRANSPARENT_OVERLAY_STATUS_MODE) {
                    setTransparencyStatusBar(activity, true);
                    setTransparencyNavigationBar(activity, false);
                } else {
                    setTransparencyStatusBar(activity, false);
                    setTransparencyNavigationBar(activity, false);
                }
                break;
            case FULLSCREEN_BOTH_OVERLAY_SYSTEM_BAR_MODE:
            case FULLSCREEN_BOTH_TRANSPARENT_OVERLAY_SYSTEM_BAR_MODE:

                uiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;

                // set transparency of navigation bar and status bar
                if (whatScreenMode == FULLSCREEN_BOTH_TRANSPARENT_OVERLAY_SYSTEM_BAR_MODE) {
                    setTransparencyStatusBar(activity, true);
                    setTransparencyNavigationBar(activity, true);
                } else {
                    setTransparencyStatusBar(activity, false);
                    setTransparencyNavigationBar(activity, false);
                }
                break;

            case FULLSCREEN_OVERLAY_NAVI_TRANSPARENT_OVERLAY_STATUS_IMMERSIVE:
                uiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE;

                // set transparency of navigation bar and status bar
                setTransparencyStatusBar(activity, true);
                setTransparencyNavigationBar(activity, false);
                break;
            case FULLSCREEN_BOTH_TRANSPARENT_HIDE_IMMERSIVE:
                uiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE;

                // set transparency of navigation bar and status bar
                setTransparencyStatusBar(activity, true);
                setTransparencyNavigationBar(activity, false);
                break;
            default:
                Log.w(TAG, "Unknown Screen Mode");
                return false;
        }

        // save current screen mode
        mWhatScreenMode = whatScreenMode;

        // set the system ui visibility
        decorView.setSystemUiVisibility(uiVisibility);

        return true;
    }

    public int getSystemVisivility(Activity activity) {
        if (activity == null) return -1;

        View decorView = activity.getWindow().getDecorView();
        if (decorView == null) return -1;
        return decorView.getSystemUiVisibility();
    }

    public int getScreenMode() {
        return mWhatScreenMode;
    }

    /**
     * Show or hide the ActionBar
     * @param activity
     * @param show
     *          if have the ActionBar
     *          - true : show the ActionBar
     *          - false : hide the ActionBar
     * @return if successful, false if an error occurs
     */
    public boolean showActionBar(Activity activity, boolean show) {
        if (activity == null) return false;

        ActionBar actionBar = activity.getActionBar();
        if (actionBar == null) return false;

        mShowActionBar = show;
        if (show) {
            actionBar.hide();
        } else {
            actionBar.show();
        }
        return true;
    }

    public boolean isShowActionBar() {
        return mShowActionBar;
    }

    /**
     * Set transparency the navigation bar
     * @param activity Activity
     * @param hasTransparent
     *      true : The navigation bar is transparent
     *      false : The navigation bar is not transparent
     */
    public void setTransparencyNavigationBar(Activity activity, boolean hasTransparent) {
        if (activity == null) return;

        mTransparencyNavigationBar = hasTransparent;

        if (hasTransparent) {
            activity.getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        } else {
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
    }

    public boolean isTransparencyNavigationBar() {
        return mTransparencyNavigationBar;
    }

    /**
     * Set transparency the status bar
     * @param activity Activity
     * @param hasTransparent
     *      true : The status bar is transparent
     *      false : The status bar is not transparent
     */
    public void setTransparencyStatusBar(Activity activity, boolean hasTransparent) {
        if (activity == null) return;

        mTransparencyStatusBar = hasTransparent;
        if (hasTransparent) {
            activity.getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    public boolean isTransparencyStatusBar() {
        return mTransparencyStatusBar;
    }

    /**
     * Returned a display that this window manager is managing.
     * @param context Context
     */
    public Display getDisplay(Context context) {
        if (context == null) return null;

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        return wm.getDefaultDisplay();
    }

    /**
     * Gets display metrics that describe the size and density of this display.
     * <p>
     * The size is adjusted based on the current rotation of the display.
     * </p><p>
     * The size returned by this method does not necessarily represent the
     * actual raw size (native resolution) of the display.  The returned size may
     * be adjusted to exclude certain system decor elements that are always visible.
     * It may also be scaled to provide compatibility with older applications that
     * were originally designed for smaller displays.
     * </p>
     * @param context Context
     * @return DisplayMetrics A {@link DisplayMetrics} object to receive the metrics.
     */
    public DisplayMetrics getRawDisplayInfo(Context context) {
        if (context == null) return null;

        Display display = getDisplay(context);
        if (display == null) return null;

        DisplayMetrics metrics = new DisplayMetrics();
        if (metrics == null) return null;

        // Real size of this display
        display.getRealMetrics(metrics);
        return metrics;
    }

    /**
     * Gets display metrics that describe the size and density of this display.
     * <p>
     * The size is adjusted based on the current rotation of the display.
     * </p><p>
     * The size returned by this method does not necessarily represent the
     * actual raw size (native resolution) of the display.  The returned size may
     * be adjusted to exclude certain system decor elements that are always visible.
     * It may also be scaled to provide compatibility with older applications that
     * were originally designed for smaller displays.
     * </p>
     * @param context Context
     * @return DisplayMetrics A {@link DisplayMetrics} object to receive the metrics.
     */
    public DisplayMetrics getDisplayInfo(Context context) {
        if (context == null) return null;

        Display display = getDisplay(context);
        if (display == null) return null;

        DisplayMetrics metrics = new DisplayMetrics();
        if (metrics == null) return null;

        //
        display.getMetrics(metrics);
        return metrics;
    }

    /**
     * Adjust brightness of the application (window)
     * @param activity
     * @param percent The percent of window brightness with a value between 0 and 100 (0 ~ 100)
     * @return void
     */
    public boolean setApplicationBrightness(Activity activity, int percent) {
        if (activity == null) return false;

        int setBrightnessPercent = percent;
        if (setBrightnessPercent < 0) setBrightnessPercent = 0;
        else if (setBrightnessPercent > 100) setBrightnessPercent = 100;

        Log.d(TAG, "##### Brightness is " + setBrightnessPercent + "%");

        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        if (lp == null) {
            Log.w(TAG, "Doesn't set the brightness value");
            return false;
        }

        lp.screenBrightness = (float) setBrightnessPercent / 100.0f;

        // setting value is 0.0f ~ 1.0f
        activity.getWindow().setAttributes(lp);

        return true;
    }

    /**
     * Get brightness percentage of application
     * @param activity
     * @return percentage of brightness between 0 to 100.
     */
    public int getApplicationBrightness(Activity activity) {
        int percent = -1; // -1 is error
        if (activity == null) return percent;

        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        if (lp == null) {
            Log.w(TAG, "Doesn't get the brightness value");
            return percent;
        }

        percent = (int) (lp.screenBrightness * 100.0f);
        return percent;
    }

    /**
     * IsCanSystemSettingWrite
     * @param context
     * @return if this is true, You can change the system settings.
     * if false, you cannot change the system setting. In other word, you need the WRITE_SETTINGS permission.
     */
    private boolean IsCanSystemSettingWrite(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context == null) return false;
            if (!Settings.System.canWrite(context)) {
                Log.w(TAG, "We don't have permission to change system settings");
                return false;
            }
        }
        return true;
    }

    public boolean setSystemBrightness(Context context, int brightness) {
        if (context == null) return false;
        if (!IsCanSystemSettingWrite(context)) return false;

        int setBrightness =  brightness;
        if (brightness < 0) setBrightness = 0;
        else if (brightness > 255) setBrightness = 255;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(context)) {
                return false;
            }
        }
        return System.putInt(context.getContentResolver(), System.SCREEN_BRIGHTNESS, setBrightness);
    }

    /**
     * Get brightness percentage of system
     * @param context Context
     * @return system brightness value between 0 to 255
     */
    public int getSystemBrightness(Context context) {
        int systemBrightness = -1; //-1 is error
        if (context == null) return systemBrightness;
        if (!IsCanSystemSettingWrite(context)) return systemBrightness;

        try {
            systemBrightness = System.getInt(context.getContentResolver(), System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            Log.e(TAG, "Cannot access the system brightness - " + e.toString());
        }
        return systemBrightness;
    }

    public boolean setAutoBrightness(Context context, boolean on) {
        if (context == null) return false;
        if (!IsCanSystemSettingWrite(context)) return false;

        int value;
        if (on)
            value = System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC;
        else
            value = System.SCREEN_BRIGHTNESS_MODE_MANUAL;

        return System.putInt(context.getContentResolver(), System.SCREEN_BRIGHTNESS_MODE, value);
    }

    /**
     * Check if the auto brightness is set on system
     * @param context Context
     * @return true : It is the auto brightness mode
     *          false : It is not the auto brightness mode
     */
    public boolean IsAutoBrightness(Context context) {
        if (context == null) return false;
        if (!IsCanSystemSettingWrite(context)) return false;

        // default value is 0
        // 0 : System.SCREEN_BRIGHTNESS_MODE_MANUAL,
        // 1 : System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC
        int status = System.SCREEN_BRIGHTNESS_MODE_MANUAL;
        try {
            status =  System.getInt(context.getContentResolver(), System.SCREEN_BRIGHTNESS_MODE);
        } catch (Settings.SettingNotFoundException e) {
            Log.e(TAG, "Cannot access the auto system brightness - " + e.toString());
        }

        return status == System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC;
    }

    public void setNavigationBarColor(Activity activity, int color) {
        if (activity == null) return;

        activity.getWindow().setNavigationBarColor(color);
    }


}
