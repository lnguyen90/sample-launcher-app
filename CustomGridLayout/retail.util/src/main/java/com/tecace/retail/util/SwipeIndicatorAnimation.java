package com.tecace.retail.util;

import android.animation.Animator;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

/**
 * Created by dee12452 on 2/27/17.
 */

/* --------------------------
*  HOW TO USE:
*
*  Add this to your layout file, have it match parent for width and height
*
*  In your code, make sure to add an image resource file, or it won't show up:
*  SwipeIndicatorAnimation.setImageResource(R.drawable....);
*
*  to start the animation:
*                                                         * -1 for default
*  SwipeIndicatorAnimation.startAnimation(x, y, direction, displacement);
*
*  To stop call:
*
*  SwipeIndicatorAnimation.stopAnimation()
*
*  Doesn't need any onPause special treatment, will clean itself up, just call the above method when done
*
*  If you need it to perform actions like swiping, clicking, or dragging, call:
*
*  SwipeIndicatorAnimation.setOnSwipeListener(*PUT YOUR LISTENER HERE*);
**---------------------------
* */

public class SwipeIndicatorAnimation extends RelativeLayout implements GestureDetector.OnGestureListener {
    public enum AnimationmDirection { UP, DOWN, LEFT, RIGHT }
    private float mTravelDistance = 400;
    private final static float NO_ALPHA = 0.0f, FULL_ALPHA = 1.0f;
    
    //In ms
    private final static int ALPHA_TIME = 300;
    private final static int TRAVEL_TIME = 600;
    private final static int ANIMATION_DELAY = 1000;
    
    private AnimationmDirection mDirection;
    private float x, y;
    private int mId = -1;
    
    private Handler mHandler;

    private ImageView mAnimationImage;

    private GestureDetector mGestureDetector;
    private OnSwipeListener mListener;

    public SwipeIndicatorAnimation(Context context) {
        super(context);
        init();
    }

    public SwipeIndicatorAnimation(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SwipeIndicatorAnimation(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public SwipeIndicatorAnimation(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        mAnimationImage = new ImageView(getContext());
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        mAnimationImage.setLayoutParams(params);
        addView(mAnimationImage);
        mAnimationImage.setAlpha(NO_ALPHA);
        mGestureDetector = new GestureDetector(getContext(), this);
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
//                switch (motionEvent.getAction()) {
//                    case MotionEvent.ACTION_DOWN:
//                        if(mListener != null)
//                            mListener.onTouchStart(motionEvent);
//                        break;
//                    case MotionEvent.ACTION_UP:
//                        if(mListener != null)
//                            mListener.onTouchEnd(motionEvent);
//                        break;
//                    default:
//                        break;
//                }
                return !mGestureDetector.onTouchEvent(motionEvent);
            }
        });
    }

    public void setImageResource(int id) {
        mAnimationImage.setImageResource(id);
        mId = id;
    }

    public int getImageWidth() { return mAnimationImage.getWidth(); }
    public int getImageHeight() { return mAnimationImage.getHeight(); }
    public float getImageX() { return mAnimationImage.getX(); }
    public float getImageY() { return mAnimationImage.getY(); }



    /* Side being the side (on top, on bottom, on left side, on right side) that the animation plays on
    * location being where it will happen on that side (ex: if side = top, then location is distance down it happens
    * */
    public void startAnimation(float x, float y, AnimationmDirection mDirection, float distance) {
        if(mId == -1)
            Toast.makeText(getContext(), "Programmer failed to set swipe up properly!", Toast.LENGTH_SHORT).show();
        setVisibility(VISIBLE);
        mHandler = new Handler();
        if(distance != -1)
            mTravelDistance = distance;
        this.x = x; this.y = y;
        mAnimationImage.setX(x);
        mAnimationImage.setY(y);
        this.mDirection = mDirection;
        alphaIn();
    }

    private void alphaIn() {
        mAnimationImage.animate().alpha(FULL_ALPHA).setDuration(ALPHA_TIME).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                move();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    private void alphaOut() {
        mAnimationImage.animate().alpha(NO_ALPHA).setDuration(ALPHA_TIME).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if(mHandler == null)
                    return;
                mHandler.postDelayed(mRestartRunnable, ANIMATION_DELAY);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    private void move() {
        float distance = mTravelDistance;
        switch (mDirection) {
            case UP:
                distance *= -1;
                mAnimationImage.animate().y(mAnimationImage.getY() + distance).setDuration(TRAVEL_TIME).setListener(mMoveAnimator);
                break;
            case DOWN:
                mAnimationImage.animate().y(mAnimationImage.getY() + distance).setDuration(TRAVEL_TIME).setListener(mMoveAnimator);
                break;
            case LEFT:
                distance *= -1;
                mAnimationImage.animate().x(mAnimationImage.getX() + distance).setDuration(TRAVEL_TIME).setListener(mMoveAnimator);
                break;
            case RIGHT:
                mAnimationImage.animate().x(mAnimationImage.getX() + distance).setDuration(TRAVEL_TIME).setListener(mMoveAnimator);
                break;
        }
    }

    Animator.AnimatorListener mMoveAnimator = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animator) {

        }

        @Override
        public void onAnimationEnd(Animator animator) {
            alphaOut();
        }

        @Override
        public void onAnimationCancel(Animator animator) {

        }

        @Override
        public void onAnimationRepeat(Animator animator) {

        }
    };
    
    Runnable mRestartRunnable = new Runnable() {
        @Override
        public void run() {
            startAnimation(x, y, mDirection, mTravelDistance);
        }
    };

    public void stopAnimation() {
        setVisibility(INVISIBLE);
        clearAnimation();
        if(mHandler != null) {
            mHandler.removeCallbacks(mRestartRunnable);
            mHandler = null;
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopAnimation();
    }

    public void setOnSwipeListener(OnSwipeListener listener) { mListener = listener; }

    public interface OnSwipeListener {
        void onSwipe(float startX, float startY, float finishX, float finishY, float velocityX, float velocityY);
        void onDrag(float startX, float startY, float currentX, float currentY, float displacementX, float displacementY);
        void onClick(float locationX, float locationY);
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
//        playSoundEffect(SoundEffectConstants.CLICK);
        if(mListener != null)
            mListener.onClick(motionEvent.getX(), motionEvent.getY());
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        if(mListener != null)
            mListener.onDrag(motionEvent.getX(), motionEvent.getY(),
                    motionEvent1.getX(), motionEvent1.getY(),
                    v, v1);
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        if(mListener != null)
            mListener.onSwipe(motionEvent.getX(), motionEvent.getY(),
                    motionEvent1.getX(), motionEvent1.getY(),
                    v, v1);
        return false;
    }
}
