package com.tecace.retail.util.callback;

import java.util.ArrayList;

/**
 * Created by icanmobile on 6/24/16.
 */
public class CallbackFuncs implements CallbackFunc.LocalListener {
    private static final String TAG = CallbackFuncs.class.getSimpleName();

    private static volatile CallbackFuncs sInstance = null;
    public static CallbackFuncs getInstance() {
        if (sInstance == null) {
            synchronized (CallbackFuncs.class) {
                if(sInstance == null)
                    sInstance = new CallbackFuncs();
            }
        }
        return sInstance;
    }
    private CallbackFuncs() {}

    /*
     * CallbackFuncsListener
     */
    private CallbackFuncsListener mListener = null;
    public interface CallbackFuncsListener {
        public void onDone();
    }
    public void setListener(CallbackFuncsListener listener) {
        this.mListener = listener;
    }


    public ArrayList<CallbackFunc> callbackFuncs = new ArrayList<CallbackFunc>();

    public void add(CallbackFunc item) {
        item.setLocalListener(this);
        callbackFuncs.add(item);
    }

    public void remove(CallbackFunc item) {
        callbackFuncs.remove(item);
    }

    public void remove(int index) {
        callbackFuncs.remove(index);
    }

    public void removeAll() {
        for(CallbackFunc func : callbackFuncs)
            func.reset();
        callbackFuncs.clear();
        mListener = null;
    }

    public int getCount() {
        return this.callbackFuncs.size();
    }

    public CallbackFunc getItem(int index) {
        if (index < getCount())
            return callbackFuncs.get(index);
        return null;
    }

    public ArrayList<CallbackFunc> getItems() {
        return this.callbackFuncs;
    }

    public void setItems(ArrayList<CallbackFunc> items) {
        this.callbackFuncs = items;
        for(int i = 0; i< callbackFuncs.size(); i++)
            this.callbackFuncs.get(i).setLocalListener(this);
    }

    public void invokeMethod() {
        if (callbackFuncs.size() == 0 && mListener != null)
            mListener.onDone();

        if (callbackFuncs.size() > 0) {
            callbackFuncs.get(0).invokeMethod();
        }
    }

    public void invokeNextMethod() {
        if (callbackFuncs.size() > 0)  {

            callbackFuncs.get(0).reset();
            callbackFuncs.remove(0);
        }

        if (callbackFuncs.size() == 0 && mListener != null)
            mListener.onDone();

        if (callbackFuncs.size() > 0)
            callbackFuncs.get(0).invokeMethod();
    }

    @Override
    public void selfInvokeNextMethod() {
        invokeNextMethod();
    }
}
