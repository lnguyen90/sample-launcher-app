package com.tecace.retail.util;

import android.content.Context;

import com.tecace.retail.util.exception.MissingContentException;

import java.io.File;

/**
 * Created by icanmobile on 1/30/17.
 */

public class ContentUtil {
    private static String TAG = ContentUtil.class.getSimpleName();

    private static volatile ContentUtil sInstance = null;
    public static ContentUtil getInstance() {
        if (sInstance == null) {
            synchronized (ContentUtil.class) {
                if (sInstance == null)
                    sInstance = new ContentUtil();
            }
        }
        return sInstance;
    }
    private ContentUtil() {}

    @Deprecated
    public String getContentPath(Context context, String fileName){
        final String dir = context.getExternalFilesDir(null).toString();
        return String.format("%s/%s", dir, fileName);
    }

    @Deprecated
    public boolean isContentExist(Context context, String filename) throws MissingContentException {
        if (filename == null || filename.length() == 0) return false;
//        File current = new File(getContentPath(context, filename));
        File current = new File(getContentPath(context, filename));
        if (!current.exists()) {
            MissingContentException exception = new MissingContentException(filename);
            throw exception;
        }
        return true;
    }
}
