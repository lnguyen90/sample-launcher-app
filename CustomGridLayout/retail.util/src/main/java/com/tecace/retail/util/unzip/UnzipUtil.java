package com.tecace.retail.util.unzip;

import android.support.annotation.NonNull;

/**
 * Created by icanmobile on 1/11/18.
 */

public interface UnzipUtil {

    interface UnzipProcess {
        void onNext(String zipFile);
        void onError();
        void onCompleted();
    }

    boolean hasZipFiles(@NonNull String rootDir);

    void unzipFiles(@NonNull String rootDir, UnzipProcess unzipProcess);
}
