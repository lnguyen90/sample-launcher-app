package com.tecace.retail.util;

import android.content.Context;
import android.graphics.Typeface;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by JW on 2016-04-05.
 */
public class FontTypeface {
    private static final String TAG = FontTypeface.class.getSimpleName();

    private static volatile FontTypeface sInstance = null;
    public static FontTypeface getInstance() {
        if (sInstance == null) {
            synchronized (FontTypeface.class) {
                if (sInstance == null)
                    sInstance = new FontTypeface();
            }
        }
        return sInstance;
    }
    private FontTypeface() {}

    HashMap<String, Typeface> fonts = new HashMap<>();
    public void createFonts(Context context, ArrayList<String> fonts) {
        for(String font : fonts)
            createFont(context, font);
    }
    private void createFont(Context context, String font) {
        if (fonts.containsKey(font)) return;
        fonts.put(font, Typeface.createFromAsset(context.getAssets(), font));
    }
    public Typeface getFont(String font) {
        return fonts.get(font);
    }
}
