package com.tecace.retail.util.headsetMonitor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

/**
 * Created by smheo on 11/9/2017.
 */

public class HeadsetPlugMonitor {
    private static final String TAG = HeadsetPlugMonitor.class.getSimpleName();

    private static Set<IHeadsetPlugMonitorListener> mWeakUpdateListeners
            = Collections.newSetFromMap(new WeakHashMap<IHeadsetPlugMonitorListener, Boolean>());

    private static volatile HeadsetPlugMonitor sInstance;
    public static HeadsetPlugMonitor getInstance() {
        if (sInstance == null) {
            synchronized (HeadsetPlugMonitor.class) {
                if (sInstance == null)
                    sInstance = new HeadsetPlugMonitor();
            }
        }
        return sInstance;
    }

    private HeadsetPlugMonitor() {
        HeadsetPlugMonitorReceiver.getInstance().setOnUpdateHeadsetPluginListener(
                new HeadsetPlugMonitorReceiver.onUpdateHeadsetPluginListener() {
                    @Override
                    public void onUpdateHeadsetPluginStatus() {
                        notifyChangeStatus();
                    }
                }
        );
    }

    public boolean startMonitor(Context context) {
        if (context == null) return false;

        HeadsetPlugMonitorReceiver receiver = HeadsetPlugMonitorReceiver.getInstance();
        if (receiver == null) return false;

        IntentFilter headsetIntentFilter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        context.registerReceiver(receiver, headsetIntentFilter);

        IntentFilter BtDevConnectedIntentFilter = new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED);
        context.registerReceiver(receiver, BtDevConnectedIntentFilter);

        IntentFilter BtDevDisconnectedIntentFilter = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        context.registerReceiver(receiver, BtDevDisconnectedIntentFilter);

        IntentFilter BtAdapterStateIntentFilter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        context.registerReceiver(receiver, BtAdapterStateIntentFilter);

        return true;
    }

    public boolean stopMonitor(Context context) {
        if (context == null) return false;

        HeadsetPlugMonitorReceiver receiver = HeadsetPlugMonitorReceiver.getInstance();
        if (receiver == null) return false;

        context.unregisterReceiver(receiver);
        return true;
    }

    public void addListener(IHeadsetPlugMonitorListener listener) {
        if (mWeakUpdateListeners == null || listener == null) return;
        if (mWeakUpdateListeners.contains(listener)) return;

        mWeakUpdateListeners.add(listener);
    }

    public void removeListener(IHeadsetPlugMonitorListener listener) {
        if (mWeakUpdateListeners == null || listener == null) return;
        if (!mWeakUpdateListeners.contains(listener)) return;

        mWeakUpdateListeners.remove(listener);
    }

    private void notifyChangeStatus() {
        for(IHeadsetPlugMonitorListener listener : mWeakUpdateListeners) {
            listener.onChangedHeadsetPlugStatus();
        }
    }
}
