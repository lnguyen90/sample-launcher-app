package com.tecace.retail.util;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * Created by icanmobile on 2/13/18.
 *
 * When you use this Logger class, please turn on the Storage Access Permission of Application
 */

public class Logger {

    private static String FILE_EXT_ROOT_DIR = String.valueOf(Environment.getExternalStorageDirectory());
    private static String FILE_PACKAGE_PATH = "/Android/data/";
    private static String FILE_NAME = "RETAIL_MODE.txt";
    private static String FILE_PATH = FILE_EXT_ROOT_DIR + "/" + FILE_NAME;
    public static void setLoggerFile(Context context, String fileName) {

        FILE_PATH = new StringBuilder().append(FILE_EXT_ROOT_DIR)
                .append(FILE_PACKAGE_PATH).append(context.getApplicationContext().getPackageName()).append("/files/")
                .append(fileName).toString();

        Toast.makeText(context, FILE_PATH, Toast.LENGTH_LONG).show();
    }

    public static void e(String logMessageTag, String logMessage)
    {
        int logResult = Log.e(logMessageTag, logMessage);
        if (logResult > 0)
            logToFile(logMessageTag, logMessage);
    }

    public static void e(String logMessageTag, String logMessage, Throwable throwableException)
    {
        int logResult = Log.e(logMessageTag, logMessage, throwableException);
        if (logResult > 0)
            logToFile(logMessageTag,
                    logMessage + "\r\n" + Log.getStackTraceString(throwableException));
    }


    public static void i(String logMessageTag, String logMessage)
    {
        int logResult = Log.i(logMessageTag, logMessage);
        if (logResult > 0)
            logToFile(logMessageTag, logMessage);
    }
    public static void i(String logMessageTag, String logMessage, Throwable throwableException)
    {
        int logResult = Log.i(logMessageTag, logMessage, throwableException);
        if (logResult > 0)
            logToFile(logMessageTag,
                    logMessage + "\r\n" + Log.getStackTraceString(throwableException));
    }

    public static void w(String logMessageTag, String logMessage)
    {
        int logResult = Log.w(logMessageTag, logMessage);
        if (logResult > 0)
            logToFile(logMessageTag, logMessage);
    }
    public static void w(String logMessageTag, String logMessage, Throwable throwableException)
    {
        int logResult = Log.w(logMessageTag, logMessage, throwableException);
        if (logResult > 0)
            logToFile(logMessageTag,
                    logMessage + "\r\n" + Log.getStackTraceString(throwableException));
    }

    public static void v(String logMessageTag, String logMessage)
    {
        int logResult = Log.v(logMessageTag, logMessage);
        if (logResult > 0)
            logToFile(logMessageTag, logMessage);
    }

    public static void v(String logMessageTag, String logMessage, Throwable throwableException)
    {
        Log.v(logMessageTag, logMessage);

        int logResult = Log.v(logMessageTag, logMessage, throwableException);
        if (logResult > 0)
            logToFile(logMessageTag,
                    logMessage + "\r\n" + Log.getStackTraceString(throwableException));
    }

    public static void d(String logMessageTag, String logMessage)
    {
        int logResult = Log.d(logMessageTag, logMessage);
        if (logResult > 0)
            logToFile(logMessageTag, logMessage);
    }
    public static void d(String logMessageTag, String logMessage, Throwable throwableException)
    {
        int logResult = Log.d(logMessageTag, logMessage, throwableException);
        if (logResult > 0)
            logToFile(logMessageTag,
                    logMessage + "\r\n" + Log.getStackTraceString(throwableException));
    }

    private static String getDateTimeStamp()
    {
        Date dateNow = Calendar.getInstance().getTime();
        // My locale, so all the log files have the same date and time format
        return (DateFormat.getDateTimeInstance
                (DateFormat.SHORT, DateFormat.SHORT, Locale.US).format(dateNow));
    }

    private static void logToFile(String logMessageTag, String logMessage)
    {
        if (!BuildConfig.DEBUG) return;

        try {
            // Gets the log file from the root of the primary storage. If it does
            // not exist, the file is created.
            File logFile = new File(FILE_PATH);

            long Filesize = getFolderSize(logFile)/1024;//call function and convert bytes into Kb
            if(Filesize>=1024*50)  //over 5MB, delete the log file.
                logFile.delete();

            if (!logFile.exists())
                logFile.createNewFile();
            // Write the message to the log with a timestamp
            BufferedWriter writer = new BufferedWriter(new FileWriter(logFile, true));
            writer.write(String.format("%1s [%2s]:%3s\r\n",
                    getDateTimeStamp(), logMessageTag, logMessage));
            writer.close();
        } catch (IOException e) {
            Log.e(Logger.class.getSimpleName(), "Please turn on Storage Access Permission : " + FILE_PATH);
        }
    }

    private static long getFolderSize(File f) {
        long size = 0;
        if (f.isDirectory()) {
            for (File file : f.listFiles()) {
                size += getFolderSize(file);
            }
        } else {
            size=f.length();
        }
        return size;
    }
}
