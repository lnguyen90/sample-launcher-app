package com.tecace.retail.util;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;

import java.util.Collections;
import java.util.Map;

/**
 * Created by icanmobile on 6/3/16.
 */
public class FuncUtil {
    private static final String TAG = FuncUtil.class.getSimpleName();

    private static volatile FuncUtil sInstance = null;
    public static FuncUtil getInstance() {
        if (sInstance == null) {
            synchronized (FuncUtil.class) {
                if (sInstance == null)
                    sInstance = new FuncUtil();
            }
        }
        return sInstance;
    }
    private FuncUtil() {}

    public void wakeUpDevice(Context context) {
        Log.d(TAG, "##### wakeUpDevice)+ ");
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "wakeup");

        if (wakeLock.isHeld()) {
            // release old wake lock
            wakeLock.release();
        }

        // create a new wake lock
        wakeLock.acquire();


        // elease again
        wakeLock.release();
    }

    public void startApp(Context context, String pkg, String cls) {
        Log.d(TAG, "##### startApp)+ pkg = " + pkg + ", cls = " + cls);

        if (pkg == null || pkg.length() == 0 || cls == null || cls.length() == 0 ) {
            Log.d(TAG, "##### startApp : return ERROR !!!");
            return;
        }

        startApp(context, pkg, cls, Collections.<String, String>emptyMap());
    }

    public void startApp(Context context, String pkg, String cls, Bundle args) {
        Log.d(TAG, "##### startApp)+ pkg = " + pkg + ", cls = " + cls);

        if (pkg == null || pkg.length() == 0 || cls == null || cls.length() == 0 ) {
            Log.d(TAG, "##### startApp : return ERROR !!!");
            return;
        }

        try {
            Intent intent = new Intent(context.getApplicationContext(), Class.forName(cls));
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            intent.putExtras(args);

            context.startActivity(intent);
        } catch (Exception e) {

        }
    }

    public void startApp(Context context, String pkg, String cls, Map<String, String> extras) {
        Log.d(TAG, "##### startApp)+ pkg = " + pkg + ", cls = " + cls);
        if (pkg == null || pkg.length() == 0 || cls == null || cls.length() == 0 ) {
            Log.d(TAG, "##### startApp : return ERROR !!!!!");
            return;
        }

        try {
            Intent intent = new Intent(context.getApplicationContext(), Class.forName(cls));
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            for (Map.Entry<String, String> entry : extras.entrySet()) {
                intent.putExtra(entry.getKey(), entry.getValue());
            }

            context.startActivity(intent);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }


    public boolean isScreenOn(Context context) {
        Log.d(TAG, "##### isScreenOn)+ ");
        // checking screen on/off state
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        boolean bIsScreenOn = false;
        if (android.os.Build.VERSION.SDK_INT < 20) {
            bIsScreenOn = pm.isScreenOn();
        } else {
            bIsScreenOn = pm.isInteractive();
        }

        return bIsScreenOn;
    }
}
