package com.tecace.retail.util;

import android.content.Context;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by icanmobile on 3/1/16.
 */
public class AssetUtil {
    private static final String TAG = AssetUtil.class.getSimpleName();

    private static final String DEF_TYPE_DRAWABLE = "drawable";

    private static volatile AssetUtil sInstance = null;
    public static AssetUtil getInstance() {
        if (sInstance == null) {
            synchronized (AssetUtil.class) {
                if (sInstance == null)
                    sInstance = new AssetUtil();
            }
        }
        return sInstance;
    }
    private AssetUtil() {}

    //[MOVE_TO_RESMANAGER] - Keep this for config (EnvironmentManager)
    public static String GetTextFromAsset(Context context, String filename) {
        StringBuilder ret = new StringBuilder();

        try {
            InputStream is = context.getAssets().open(filename);
            InputStreamReader inputStreamReader = new InputStreamReader(is);
            BufferedReader f = new BufferedReader(inputStreamReader);
            String line = f.readLine();
            while (line != null) {
                ret.append(line);
                line = f.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ret.toString();
    }
}
