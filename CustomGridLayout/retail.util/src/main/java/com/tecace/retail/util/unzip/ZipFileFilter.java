package com.tecace.retail.util.unzip;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by icanmobile on 1/11/18.
 */

public class ZipFileFilter implements FileFilter {

    private static final String TAG = ZipFileFilter.class.getSimpleName();

    private final boolean allowDirectories;

    private HashSet<String> zipFiles;


    public ZipFileFilter() {
        this(true);
    }

    public ZipFileFilter(boolean allowDirectories) {
        this.allowDirectories = allowDirectories;
        zipFiles = new HashSet<>();
    }

    @Override
    public boolean accept(File f) {
        if (f.isHidden() || !f.canRead()) return false;

        if (f.isDirectory())
            return checkDirectory(f);

        return checkFileExtension(f);
    }

    private boolean checkFileExtension(File f) {
        String ext = getFileExtension(f);
        if (ext == null) return false;

        try {
            if (SupportedFileFormat.valueOf(ext.toUpperCase()) != null)
                return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
        return false;
    }

    public String getFileExtension( File f ) {
        return getFileExtension( f.getName() );
    }

    public String getFileExtension( String fileName ) {
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            return fileName.substring(i+1);
        } else
            return null;
    }



    private boolean checkDirectory(File dir) {
        if (!allowDirectories) return false;

        final ArrayList<File> subDirs = new ArrayList<>();
        int nums = dir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                if (file.isFile())
                    return checkFileExtension(file);
                else if (file.isDirectory()) {
                    subDirs.add(file);
                    return false;
                }
                else
                    return false;
            }
        }).length;

        if (nums > 0)
            return true;

        for (File subDir : subDirs) {
            if (checkDirectory(subDir)) {
                return true;
            }
        }
        return false;
    }



    /**
     * Files formats currently supported by Library
     */
    public enum SupportedFileFormat
    {
        ZIP("zip");

        private String filesuffix;

        SupportedFileFormat( String filesuffix ) {
            this.filesuffix = filesuffix;
        }

        public String getFilesuffix() {
            return filesuffix;
        }
    }
}
