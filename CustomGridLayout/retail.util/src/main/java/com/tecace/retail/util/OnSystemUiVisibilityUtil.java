package com.tecace.retail.util;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by smheo on 3/20/2017.
 */

public class OnSystemUiVisibilityUtil {
    private static final String TAG = OnSystemUiVisibilityUtil.class.getSimpleName();

    private Activity mActivity;
    private static volatile OnSystemUiVisibilityUtil sInstance = null;

    private List<OnUiVisibilityListener> mListenerList;
    public interface OnUiVisibilityListener {
        void OnChangeUiVisibility(int visibility);
        void OnChangedWindowFocus(boolean hasFocus);
    }

    public static OnSystemUiVisibilityUtil getInstance(Activity activity) {
        if (sInstance == null) {
            synchronized (OnSystemUiVisibilityUtil.class) {
                if (sInstance == null)
                    sInstance = new OnSystemUiVisibilityUtil(activity);
            }
        }
        return sInstance;
    }

    private OnSystemUiVisibilityUtil(Activity activity) {
        this.mActivity = activity;
        if (mListenerList == null) {
            mListenerList = new ArrayList<>();
        }
    }

    public void start() {
        if (mActivity == null) return;
        View decorView = mActivity.getWindow().getDecorView();
        if (decorView == null && mOnSystemUiVisibilityChangeListener == null) return;
        decorView.setOnSystemUiVisibilityChangeListener(mOnSystemUiVisibilityChangeListener);
    }

    public void stop() {
        if (mActivity == null) return;
        View decorView = mActivity.getWindow().getDecorView();
        if (decorView == null) return;
        decorView.setOnSystemUiVisibilityChangeListener(null);
    }

    public void release() {
        this.stop();

        if (mListenerList != null) {
            mListenerList.clear();
            mListenerList = null;
        }

        mOnSystemUiVisibilityChangeListener = null;
        mActivity = null;
        sInstance = null;

    }

    private View.OnSystemUiVisibilityChangeListener
            mOnSystemUiVisibilityChangeListener = new View.OnSystemUiVisibilityChangeListener() {
        @Override
        public void onSystemUiVisibilityChange(int visibility) {
            if (mListenerList == null || mListenerList.isEmpty()) {
                Log.w(TAG, "####SYSTEMUI onSystemUiVisibilityChange - List is null");
                return;
            }

            for (OnUiVisibilityListener listener : mListenerList) {
                if (listener != null) {
                    listener.OnChangeUiVisibility(visibility);
                }
            }
        }
    };


    public void registerListener(OnUiVisibilityListener listener) {
        if (mListenerList == null || listener == null) return;
        mListenerList.add(listener);
    }

    public void unregisterListener(OnUiVisibilityListener listener) {
        if (mListenerList == null || mListenerList.isEmpty() || listener == null) return;

        if (mListenerList.contains(listener)) {
            mListenerList.remove(listener);
        }
    }

    public void setOnWindowFocus(boolean hasFocus) {
        if (mListenerList == null || mListenerList.isEmpty()) {
            Log.w(TAG, "####SYSTEMUI setOnWindowFocus - List is null");
            return;
        }

        for (OnUiVisibilityListener listener : mListenerList) {
            if (listener != null) {
                listener.OnChangedWindowFocus(hasFocus);
            }
        }
    }


}
