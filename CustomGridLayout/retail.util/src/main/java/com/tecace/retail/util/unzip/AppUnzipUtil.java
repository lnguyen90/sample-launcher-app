package com.tecace.retail.util.unzip;

import android.support.annotation.NonNull;

import com.tecace.retail.util.FileUtil;

import java.io.File;
import java.util.HashSet;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by icanmobile on 1/11/18.
 */

public class AppUnzipUtil implements UnzipUtil {

    private static final String TAG = AppUnzipUtil.class.getSimpleName();

    private static volatile AppUnzipUtil sInstance;
    public static AppUnzipUtil getInstance() {
        if (sInstance == null) {
            synchronized (AppUnzipUtil.class) {
                if (sInstance == null)
                    sInstance = new AppUnzipUtil();
            }
        }
        return sInstance;
    }
    private AppUnzipUtil() {}


    @Override
    public boolean hasZipFiles(@NonNull String rootDir) {
        return getZipFiles(rootDir).size() > 0 ? true : false;
    }

    @Override
    public void unzipFiles(@NonNull String rootDir, UnzipProcess unzipProcess) {
        unzipContentsProcess(rootDir, unzipProcess);
    }


    private HashSet<String> getZipFiles(String rootDir) {
        File dir = new File(rootDir);
        HashSet<String> zipFiles = new HashSet<>();
        getZipFiles(dir, zipFiles);
        return zipFiles;
    }
    private void getZipFiles(File dir, HashSet<String> zipFiles) {
        File[] files = dir.listFiles(new ZipFileFilter());
        for(File file : files) {
            if (file.isFile())
                zipFiles.add(file.getPath());
            else if (file.isDirectory()) {
                getZipFiles(file, zipFiles);
            }
        }
    }

    Disposable disposable;
    private void unzipContentsProcess(String rootDir, final UnzipProcess unzipProcess) {
        HashSet<String> zipFiles = getZipFiles(rootDir);
        if (zipFiles.size() > 0) {
            if(this.disposable != null && !this.disposable.isDisposed())
                this.disposable.dispose();

            this.disposable = Observable.fromIterable(zipFiles)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(Schedulers.newThread())
                    .subscribe(
                            v -> {
                                try {
                                    if (v == null) return;
                                    if (unzipProcess != null)
                                        unzipProcess.onNext(v);

                                    if (FileUtil.getInstance().unzipToFolder(v)) {
                                        File deletingZip = new File(v);
                                        if (deletingZip != null) {
                                            if (deletingZip.exists())
                                                deletingZip.delete();
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            },
                            err -> {
                                if (unzipProcess != null)
                                    unzipProcess.onError();
                            },
                            () -> {
                                unzipContentsProcess(rootDir, unzipProcess);
                            }
                    );
        }
        else {
            if(this.disposable != null && !this.disposable.isDisposed())
                this.disposable.dispose();

            if (unzipProcess != null)
                unzipProcess.onCompleted();
        }
    }
}
