package com.tecace.retail.util.gesture;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

/**
 * Created by smheo on 8/9/2016.
 */
public class GestureDetectorListener implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {
    private static final String TAG = GestureDetectorListener.class.getSimpleName();

    private final int mMinFlingVelocity;
    private static final int SWIPE_THRESHOLD = 100;

    // Listener for gesture
    private final OnCustomGestureListener mGestureViewListener;
    public interface OnCustomGestureListener {
        boolean onTap(MotionEvent event);
        boolean onDoubleTap(MotionEvent event);
        void onHorizontalScroll(MotionEvent event, float delta);
        void onVerticalScroll(MotionEvent event, float delta);
        void onSwipeRight();
        void onSwipeLeft();
        void onSwipeDown();
        void onSwipeUp();
    }

    public GestureDetectorListener(OnCustomGestureListener listener, ViewConfiguration viewConfiguration) {
        this.mGestureViewListener = listener;
        this.mMinFlingVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        if (e1 == null || e2 == null) return false;

        float deltaY = e2.getY() - e1.getY();
        float deltaX = e2.getX() - e1.getX();

        if (Math.abs(deltaX) > Math.abs(deltaY)) {
            if (Math.abs(deltaX) > SWIPE_THRESHOLD) {
                if (mGestureViewListener != null) {
                    mGestureViewListener.onHorizontalScroll(e2, deltaX);
                }
            }
        } else {
            if (Math.abs(deltaY) > SWIPE_THRESHOLD) {
                if (mGestureViewListener != null) {
                    mGestureViewListener.onVerticalScroll(e2, deltaY);
                }
            }
        }

        return false;
    }


    /**
     * Fling event occurred.  Notification of this one happens after an "up" event.
     */
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (e1 == null || e2 == null) return false;

        boolean result = false;
        try {
            float diffY = e2.getY() - e1.getY();
            float diffX = e2.getX() - e1.getX();
            if (Math.abs(diffX) > Math.abs(diffY)) {
                if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > mMinFlingVelocity) {
                    if (mGestureViewListener != null) {
                        if (diffX > 0) {
                            mGestureViewListener.onSwipeRight();
                        } else {
                            mGestureViewListener.onSwipeLeft();
                        }
                    }
                }
                result = true;
            } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > mMinFlingVelocity) {
                if (mGestureViewListener != null) {
                    if (diffY > 0) {
                        mGestureViewListener.onSwipeDown();
                    } else {
                        mGestureViewListener.onSwipeUp();
                    }
                }
            }
            result = true;

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }

    @Override
    public void onShowPress(MotionEvent e) {
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        if (mGestureViewListener != null) {
            return mGestureViewListener.onDoubleTap(e);
        }
        return false;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        if (mGestureViewListener != null) {
            return mGestureViewListener.onTap(e);
        }
        return false;
    }

}
