package com.tecace.retail.util;

import android.content.Context;
import android.media.AudioDeviceInfo;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.util.Log;
import android.view.SoundEffectConstants;

import java.io.File;

import static android.content.Context.VIBRATOR_SERVICE;

/**
 * Created by smheo on 6/2/2016.
 */
public class SoundUtil {
    private static final String TAG = SoundUtil.class.getSimpleName();

    private static volatile SoundUtil sInstance = null;
    private static final int[] AUDIO_TYPES = {
//            AudioManager.STREAM_ALARM,
//            AudioManager.STREAM_DTMF,
//            AudioManager.STREAM_NOTIFICATION,
//            AudioManager.STREAM_RING,
//            AudioManager.STREAM_VOICE_CALL,
//            AudioManager.STREAM_SYSTEM,
            AudioManager.STREAM_MUSIC
    };

    public static SoundUtil getInstance() {
        if (sInstance == null) {
            synchronized (SoundUtil.class) {
                if (sInstance == null)
                    sInstance = new SoundUtil();
            }
        }
        return sInstance;
    }
    private SoundUtil() {}

    /**
     * setVolumeAsPercentage()
     * @param context
     * @param percent - It is volume percent with a value between 0 and 100 (0% ~ 100%)
     */
    public void setVolumeAsPercentage(Context context, int percent) {
        if (context == null) return;

        int getVolumePercent = percent;
        if (getVolumePercent < 0) {
            getVolumePercent = 0;
        } else if (getVolumePercent > 100) {
            getVolumePercent = 100;
        }

        Log.d(TAG, "##### Volume is " + getVolumePercent + "%");

        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            for (int streamType : AUDIO_TYPES) {
                audioManager.adjustStreamVolume(streamType, AudioManager.ADJUST_SAME, 0);

                int maxVol = audioManager.getStreamMaxVolume(streamType);
                int setVolumeValue = (int) (maxVol * (((float) getVolumePercent) / 100.0f));
                audioManager.setStreamVolume(streamType, setVolumeValue, 0);
            }
        }
    }

    /**
     * Get the devices current media volume as a percentage
     * @param context the context of the application
     * @return the percentage value as an int
     */
    public int getVolumeAsPercentage(Context context) {
        if(context == null) return -1;

        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            float maxVol = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            float currVol = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

            return (int) ((currVol / maxVol) * 100.0f);
        }

        return -1;
    }

    public void playAndroidDefaultClickSound(Context context) {
        if (context == null) return;

        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            audioManager.playSoundEffect(SoundEffectConstants.CLICK);
        }
    }

    public static final int UNKNOWN_AUDIO_DEVICE = -1;
    public static final int DISCONNECTED_HEADSET = 0;
    public static final int CONNECTED_HEADSET = 1;
    public int isHeadsetConnected(Context context) {
        if (context == null) return UNKNOWN_AUDIO_DEVICE;

        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager == null) return UNKNOWN_AUDIO_DEVICE;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (audioManager.isWiredHeadsetOn() || audioManager.isBluetoothA2dpOn()) {
                return CONNECTED_HEADSET;
            } else {
                return DISCONNECTED_HEADSET;
            }
        } else {
            AudioDeviceInfo[] deviceInfos = audioManager.getDevices(AudioManager.GET_DEVICES_OUTPUTS);
            for (int i = 0; i < deviceInfos.length; i++) {
                AudioDeviceInfo deviceInfo = deviceInfos[i];
                // Log.d(TAG, "Current Audio Device Type : " + deviceInfo.getType());
                if (deviceInfo.getType() == AudioDeviceInfo.TYPE_WIRED_HEADSET
                        || deviceInfo.getType() == AudioDeviceInfo.TYPE_WIRED_HEADPHONES
                        || deviceInfo.getType() == AudioDeviceInfo.TYPE_BLUETOOTH_A2DP
                        || deviceInfo.getType() == AudioDeviceInfo.TYPE_BLUETOOTH_SCO) {
                    return CONNECTED_HEADSET;
                }
            }
        }
        return DISCONNECTED_HEADSET;
    }

    public void vibrate(Context context, long milliseconds) {
        Vibrator vib = (Vibrator) context.getSystemService(VIBRATOR_SERVICE);
        if (vib != null && vib.hasVibrator())
            vib.vibrate(milliseconds);
    }

    public static class VoiceOver {
        MediaPlayer mAudioPlayer;
        private Context mContext;
        private String mAudioFile;
        private OnAudioLoadListener mListener;

        public VoiceOver(Context c, String audioFile) {
            mContext = c;
            mAudioFile = audioFile;
        }

        public void setOnAudioLoadListener(OnAudioLoadListener listener) { this.mListener = listener; }

        public void load() {
            mAudioPlayer = MediaPlayer.create(mContext, Uri.fromFile(new File(FileUtil.getFileFullPath(mContext, mAudioFile))));
            if(mListener != null) {
                if(mAudioPlayer != null) {
                    mAudioPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mListener.loadSucceeded();
                        }
                    });
                }
                else {
                    mListener.loadFailed(mAudioFile);
                }
            }
        }

        public void play() {
            if(mAudioPlayer != null && !mAudioPlayer.isPlaying()) {
                mAudioPlayer.start();
            }
        }

        public void stop() {
            if(mAudioPlayer != null) {
                mAudioPlayer.stop();
            }
        }

        public void pause() {
            if(mAudioPlayer != null && mAudioPlayer.isPlaying()) {
                mAudioPlayer.pause();
            }
        }

        public void resume() {
            play();
        }

        public void seek(int ms) {
            if(mAudioPlayer != null) {
                mAudioPlayer.seekTo(ms);
            }
        }

        public void release() {
            if(mAudioPlayer != null) {
                mAudioPlayer.reset();
                mAudioPlayer.release();
                mAudioPlayer = null;
            }
        }

        public int getCurrentPosition() {
            if(mAudioPlayer != null) {
                return mAudioPlayer.getCurrentPosition();
            }
            return -1;
        }

        public boolean isPlaying() {
            if(mAudioPlayer != null) {
                return mAudioPlayer.isPlaying();
            }
            return false;
        }

        public interface OnAudioLoadListener {
            void loadSucceeded();
            void loadFailed(String audioFile);
        }
    }
}
