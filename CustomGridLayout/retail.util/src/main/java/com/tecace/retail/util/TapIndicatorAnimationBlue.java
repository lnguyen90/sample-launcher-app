package com.tecace.retail.util;

/**
 * Created by JW on 3/3/2017.
 */

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

/***************************************************************************************************
 EXAMPLE:
             mTapIndicatorAnimation
               .showTapIndicator(x position, y position)
               .setOnIndicatorTap(new TapIndicatorAnimation.OnIndicatorTap() {
                          @Override
                          public void onIndicatorTap() {
                          // Implement callback for tap event.

                          }
               });

  *  AFTER CREATING AND SHOWING ONCE, USE setPosition or setPositionAfter method to show/hide.      *
  ***************************************************************************************************
  *     ATTENTION: Add [ android:clipChildren="false" ] to parent view that contains this view.     *
 ***************************************************************************************************/

public class TapIndicatorAnimationBlue extends RelativeLayout {
    private static final String TAG = TapIndicatorAnimationBlue.class.getSimpleName();

    private static final int INNER_CIRCLE_ANIM_DURATION = 300;
    private static final int OUTER_CIRCLE_ANIM_DURATION = 700;
    private static final int OUTER_CIRCLE_REPEAT_COUNT = 1;
    private static final int SWIPE_ANIM_DURATION = 1000;

    private TapIndicatorAnimationBlue.OnIndicatorTap mOnIndicatorTap;

    private TimerHandler mShowTimerHandler;

    private ImageButton mPulseButton;
    private ObjectAnimator mSwipeAnimator;

    private ObjectAnimator mInnerCircleAnimXPos;
    private ObjectAnimator mInnerCircleAnimYPos;

    private ObjectAnimator mOuterCircleAnimXReverse;
    private ObjectAnimator mOuterCircleAnimYReverse;

    private ObjectAnimator mOuterCircleAnimXPos;
    private ObjectAnimator mOuterCircleAnimYPos;

    private ObjectAnimator mOuterCircleFadeAnim;
    private ObjectAnimator mOuterCircleFadeAnimReverse;

    private AnimatorSet mOuterCircleReverseAnimSet;
    private AnimatorSet mInnerCircleAnimSet;
    private AnimatorSet mOuterCircleAnimSet;

    private boolean mIsPaused = false;
    private boolean mhasFirstShowTapIndicator;

    public TapIndicatorAnimationBlue(Context context) {
        super(context);
        init();
    }

    public TapIndicatorAnimationBlue(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TapIndicatorAnimationBlue(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        init();
    }

    private void init() {
        // Initialize the view.
        this.bringToFront();

        //init position
        this.setX(0);
        this.setY(0);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        killTapIndicator();
    }

    private TapIndicatorAnimationBlue initializePulseButton() {
        if(mPulseButton == null) {
            mPulseButton = new ImageButton(getContext());

            LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            mPulseButton.setLayoutParams(params);

            int resID = getResources().getIdentifier("blue_pulse_anim" , "drawable", getContext().getPackageName());

            mPulseButton.setImageResource(resID);
            mPulseButton.setBackgroundColor(Color.TRANSPARENT);
            this.addView(mPulseButton);

            mPulseButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mIsPaused) return;

                    if (mOnIndicatorTap != null) {
//                        mOnIndicatorTap.onIndicatorTap(view);
                        hide();
                        mOnIndicatorTap.onIndicatorTap(TapIndicatorAnimationBlue.this);
                    }
                }
            });
        }

        return this;
    }

    public void setPulseButtonClickable(final boolean clickable) {
        if(mPulseButton == null) return;
        mPulseButton.setClickable(clickable);
    }

    private void initializeAnimSets() {
        if (mOuterCircleAnimSet == null) {
            mOuterCircleAnimSet = new AnimatorSet();
            mOuterCircleAnimSet.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    if (mPulseButton != null)
                        mPulseButton.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (mOuterCircleReverseAnimSet != null)
                        mOuterCircleReverseAnimSet.start();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }

        if (mOuterCircleReverseAnimSet == null) {
            mOuterCircleReverseAnimSet = new AnimatorSet();
            mOuterCircleReverseAnimSet.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (mOuterCircleAnimSet != null)
                        mOuterCircleAnimSet.start();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }
    }

    private TapIndicatorAnimationBlue setAnimProperties() {
        if (mOuterCircleAnimXPos == null) {
            mOuterCircleAnimXPos = ObjectAnimator.ofFloat(mPulseButton, "scaleX", 0.7f, 1.0f);
            mOuterCircleAnimXPos.setDuration(OUTER_CIRCLE_ANIM_DURATION);
        }

        if (mOuterCircleAnimYPos == null) {
            mOuterCircleAnimYPos = ObjectAnimator.ofFloat(mPulseButton, "scaleY", 0.7f, 1.0f);
            mOuterCircleAnimYPos.setDuration(OUTER_CIRCLE_ANIM_DURATION);
        }

        if (mOuterCircleFadeAnim == null) {
            mOuterCircleFadeAnim = ObjectAnimator.ofFloat(mPulseButton, "alpha", 0.2f, 1.0f);
            mOuterCircleFadeAnim.setDuration(OUTER_CIRCLE_ANIM_DURATION);
        }

        if (mOuterCircleAnimXReverse == null ) {
            mOuterCircleAnimXReverse = ObjectAnimator.ofFloat(mPulseButton, "scaleX", 1.0f, 0.7f);
            mOuterCircleAnimXReverse.setDuration(OUTER_CIRCLE_ANIM_DURATION);
        }

        if (mOuterCircleAnimYReverse == null ) {
            mOuterCircleAnimYReverse = ObjectAnimator.ofFloat(mPulseButton, "scaleY", 1.0f, 0.7f);
            mOuterCircleAnimYReverse.setDuration(OUTER_CIRCLE_ANIM_DURATION);
        }

        if (mOuterCircleFadeAnimReverse == null) {
            mOuterCircleFadeAnimReverse = ObjectAnimator.ofFloat(mPulseButton, "alpha", 1.0f, 0.2f);
            mOuterCircleFadeAnimReverse.setDuration(OUTER_CIRCLE_ANIM_DURATION);
        }

        mOuterCircleAnimSet.playTogether(mOuterCircleAnimXPos, mOuterCircleAnimYPos, mOuterCircleFadeAnim);
        mOuterCircleReverseAnimSet.playTogether(mOuterCircleAnimXReverse, mOuterCircleAnimYReverse, mOuterCircleFadeAnimReverse);

        return this;
    }

    public TapIndicatorAnimationBlue startAnimation() {
        mIsPaused = false;
        if (mOuterCircleAnimSet != null)
            mOuterCircleAnimSet.start();
        return this;
    }


    public TapIndicatorAnimationBlue stopAnimation() {
        if (mOuterCircleAnimSet != null || mOuterCircleReverseAnimSet != null) {
            if (mPulseButton != null)
                mPulseButton.clearAnimation();

            if (mInnerCircleAnimXPos != null) {
                mInnerCircleAnimXPos.cancel();
                mInnerCircleAnimXPos = null;
            }
            if (mInnerCircleAnimYPos != null) {
                mInnerCircleAnimYPos.cancel();
                mInnerCircleAnimYPos = null;
            }
            if (mOuterCircleAnimXReverse != null) {
                mOuterCircleAnimXReverse.cancel();
                mOuterCircleAnimXReverse = null;
            }
            if (mOuterCircleAnimYReverse != null) {
                mOuterCircleAnimYReverse.cancel();
                mOuterCircleAnimYReverse = null;
            }
            if (mOuterCircleAnimXPos != null) {
                mOuterCircleAnimXPos.cancel();
                mOuterCircleAnimXPos = null;
            }
            if (mOuterCircleAnimYPos != null) {
                mOuterCircleAnimYPos.cancel();
                mOuterCircleAnimYPos = null;
            }
            if (mOuterCircleFadeAnim != null) {
                mOuterCircleFadeAnim.cancel();
                mOuterCircleFadeAnim = null;
            }
            if (mInnerCircleAnimSet != null) {
                mInnerCircleAnimSet.cancel();
                mInnerCircleAnimSet = null;
            }
            if (mOuterCircleAnimSet != null) {
                mOuterCircleAnimSet.cancel();
                mOuterCircleAnimSet = null;
            }
            if (mOuterCircleReverseAnimSet != null) {
                mOuterCircleReverseAnimSet.cancel();
                mOuterCircleReverseAnimSet = null;
            }
        }
        return this;
    }

    public TapIndicatorAnimationBlue pauseAnimation() {
        mIsPaused = true;
        if (mOuterCircleAnimSet != null || mOuterCircleReverseAnimSet != null) {

            if (mInnerCircleAnimXPos != null) {
                mInnerCircleAnimXPos.pause();
            }
            if (mInnerCircleAnimYPos != null) {
                mInnerCircleAnimYPos.pause();
            }
            if (mOuterCircleAnimXReverse != null) {
                mOuterCircleAnimXReverse.pause();
            }
            if (mOuterCircleAnimYReverse != null) {
                mOuterCircleAnimYReverse.pause();
            }
            if (mOuterCircleAnimXPos != null) {
                mOuterCircleAnimXPos.pause();
            }
            if (mOuterCircleAnimYPos != null) {
                mOuterCircleAnimYPos.pause();
            }
            if (mOuterCircleFadeAnim != null) {
                mOuterCircleFadeAnim.pause();
            }
            if (mInnerCircleAnimSet != null) {
                mInnerCircleAnimSet.pause();
            }
            if (mOuterCircleAnimSet != null) {
                mOuterCircleAnimSet.pause();
            }
            if (mOuterCircleReverseAnimSet != null) {
                mOuterCircleReverseAnimSet.pause();
            }
        }
        return this;
    }

    public TapIndicatorAnimationBlue resumeAnimation() {
        mIsPaused = false;
        if (mOuterCircleAnimSet != null || mOuterCircleReverseAnimSet != null) {

            if (mInnerCircleAnimXPos != null) {
                mInnerCircleAnimXPos.resume();
            }
            if (mInnerCircleAnimYPos != null) {
                mInnerCircleAnimYPos.resume();
            }
            if (mOuterCircleAnimXReverse != null) {
                mOuterCircleAnimXReverse.resume();
            }
            if (mOuterCircleAnimYReverse != null) {
                mOuterCircleAnimYReverse.resume();
            }
            if (mOuterCircleAnimXPos != null) {
                mOuterCircleAnimXPos.resume();
            }
            if (mOuterCircleAnimYPos != null) {
                mOuterCircleAnimYPos.resume();
            }
            if (mOuterCircleFadeAnim != null) {
                mOuterCircleFadeAnim.resume();
            }
            if (mInnerCircleAnimSet != null) {
                mInnerCircleAnimSet.resume();
            }
            if (mOuterCircleAnimSet != null) {
                mOuterCircleAnimSet.resume();
            }
            if (mOuterCircleReverseAnimSet != null) {
                mOuterCircleReverseAnimSet.resume();
            }
        }
        return this;
    }

    public TapIndicatorAnimationBlue hide() {
        this.setVisibility(GONE);

        return this;
    }

    private int displayW;
    private int displayH;
    public TapIndicatorAnimationBlue setPosition(int xPos, int yPos) {
        this.measure(0, 0);
        displayW = this.getMeasuredWidth();
        displayH = this.getMeasuredHeight();
        this.setX(xPos - displayW/2);
        this.setY(yPos - displayH/2);
        this.setVisibility(VISIBLE);
        this.bringToFront();

        return this;
    }

    public TapIndicatorAnimationBlue setMovePosition(int xPos, int yPos) {
        this.setX(xPos);
        this.setY(yPos);
        this.setVisibility(VISIBLE);
        this.bringToFront();

        return this;
    }

    public static final int Swipe_Left  = 1;
    public static final int Swipe_Up    = 2;
    public static final int Swipe_Right = 3;
    public static final int Swipe_Down  = 4;
    public static final int Swipe_Left_Right = 5;
    public static final int Swipe_Up_Down = 6;
    public void setSwipePosition(Rect rect, int dir) {
        if (dir < Swipe_Left || dir > Swipe_Up_Down) return;

        stopAnimation();
        if (mOnIndicatorTap != null)
            mOnIndicatorTap = null;

        if (mSwipeAnimator != null && (mSwipeAnimator.isStarted() || mSwipeAnimator.isRunning()))
            mSwipeAnimator.cancel();

        rect.right = (rect.right-displayW) > 0 ? (rect.right-displayW) : rect.right;
        rect.bottom = (rect.bottom-displayH) > 0 ? (rect.bottom-displayH) : rect.bottom;
        switch(dir) {
            case Swipe_Left:
                mSwipeAnimator = ObjectAnimator.ofFloat(this, "x", rect.right, rect.left);
                break;

            case Swipe_Right:
                mSwipeAnimator = ObjectAnimator.ofFloat(this, "x", rect.left, rect.right);
                break;

            case Swipe_Up:
                mSwipeAnimator = ObjectAnimator.ofFloat(this, "y", rect.bottom, rect.top);
                break;

            case Swipe_Down:
                mSwipeAnimator = ObjectAnimator.ofFloat(this, "y", rect.top, rect.bottom);
                break;

            case Swipe_Left_Right:
                mSwipeAnimator = ObjectAnimator.ofFloat(this, "x", rect.right, rect.left);
                mSwipeAnimator.setRepeatMode(ValueAnimator.REVERSE);
                break;

            case Swipe_Up_Down:
                mSwipeAnimator = ObjectAnimator.ofFloat(this, "y", rect.bottom, rect.top);
                mSwipeAnimator.setRepeatMode(ValueAnimator.REVERSE);
                break;
        }
        mSwipeAnimator.setDuration(SWIPE_ANIM_DURATION);
        mSwipeAnimator.setRepeatCount(ValueAnimator.INFINITE);
        mSwipeAnimator.start();
    }


    public TapIndicatorAnimationBlue setPositionAfter(final int xPos, final int yPos, int duration) {
        if (mShowTimerHandler == null)
            mShowTimerHandler = new TimerHandler();

        mShowTimerHandler.setOnTimeoutListener(new TimerHandler.OnTimeoutListener() {
            @Override
            public void onTimeout() {
                setPosition(xPos, yPos);
            }
        });
        mShowTimerHandler.start(duration);
        return this;
    }

    /**
     * Show after some duration.
     * */
    public TapIndicatorAnimationBlue showAnimationAfter(final float xPos, final float yPos, int duration) {
        if (mShowTimerHandler == null)
            mShowTimerHandler = new TimerHandler();

        mShowTimerHandler.setOnTimeoutListener(new TimerHandler.OnTimeoutListener() {
            @Override
            public void onTimeout() {
                showTapIndicator(xPos, yPos);
            }
        });

        mShowTimerHandler.start(duration);
        return this;
    }

    public TapIndicatorAnimationBlue showTapIndicator() {
        return showTapIndicator(this.getX(), this.getY());
    }

    public boolean hasCalledShowTapIndicator() {
        return this.mhasFirstShowTapIndicator;
    }

    public TapIndicatorAnimationBlue showTapIndicator(float xPos, float yPos) {

        mhasFirstShowTapIndicator = true;

        this.setVisibility(VISIBLE);
        this.setX(xPos);
        this.setY(yPos);

        initializePulseButton();
        initializeAnimSets();
        setAnimProperties();

        startAnimation();

        return this;
    }

    public TapIndicatorAnimationBlue prepareTapIndicator() {
        initializePulseButton();
        initializeAnimSets();
        setAnimProperties();

        return this;
    }

    public void killTapIndicator() {
        Log.d(TAG, "*** TAP INDICATOR IS DEAD ***");
        if (mOnIndicatorTap != null)
            mOnIndicatorTap = null;
        if (mPulseButton != null)
            mPulseButton = null;

        if (mInnerCircleAnimXPos != null) {
            mInnerCircleAnimXPos.cancel();
            mInnerCircleAnimXPos = null;
        }
        if (mInnerCircleAnimYPos != null) {
            mInnerCircleAnimYPos.cancel();
            mInnerCircleAnimYPos = null;
        }
        if (mOuterCircleAnimXReverse != null) {
            mOuterCircleAnimXReverse.cancel();
            mOuterCircleAnimXReverse = null;
        }
        if (mOuterCircleAnimYReverse != null) {
            mOuterCircleAnimYReverse.cancel();
            mOuterCircleAnimYReverse = null;
        }
        if (mOuterCircleAnimXPos != null) {
            mOuterCircleAnimXPos.cancel();
            mOuterCircleAnimXPos = null;
        }
        if (mOuterCircleAnimYPos != null) {
            mOuterCircleAnimYPos.cancel();
            mOuterCircleAnimYPos = null;
        }
        if (mOuterCircleFadeAnim != null) {
            mOuterCircleFadeAnim.cancel();
            mOuterCircleFadeAnim = null;
        }
        if (mInnerCircleAnimSet != null) {
            mInnerCircleAnimSet.cancel();
            mInnerCircleAnimSet = null;
        }
        if (mOuterCircleAnimSet != null) {
            mOuterCircleAnimSet.cancel();
            mOuterCircleAnimSet = null;
        }
        if (mOuterCircleReverseAnimSet != null) {
            mOuterCircleReverseAnimSet.cancel();
            mOuterCircleReverseAnimSet = null;
        }
        if (mShowTimerHandler!= null) {
            mShowTimerHandler.stop();
            mShowTimerHandler = null;
        }

        if (mSwipeAnimator != null) {
            mSwipeAnimator.cancel();
            mSwipeAnimator = null;
        }
    }

    public TapIndicatorAnimationBlue setOnIndicatorTap(TapIndicatorAnimationBlue.OnIndicatorTap callback) {
        this.mOnIndicatorTap = callback;
        return this;
    }

    public interface OnIndicatorTap {
        void onIndicatorTap(View view);
    }
}