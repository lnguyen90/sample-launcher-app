package com.tecace.retail.util.exception;

/**
 * Created by icanmobile on 1/26/17.
 */

public class MissingContentException extends Exception {
    private final static String TAG = MissingContentException.class.getSimpleName();

    public MissingContentException() {
        super(TAG);
    }

    public MissingContentException(String filename) {
        super(filename);
    }
}
