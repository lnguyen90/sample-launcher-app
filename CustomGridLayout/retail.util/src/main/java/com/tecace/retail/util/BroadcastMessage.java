package com.tecace.retail.util;

import android.content.Context;
import android.content.Intent;


/**
 * Created by icanmobile on 2/27/17.
 */

public class BroadcastMessage {
    private static final String TAG = BroadcastMessage.class.getSimpleName();

    private static volatile BroadcastMessage sInstance = null;

    public static BroadcastMessage getInstance() {
        if (sInstance == null) {
            synchronized (AssetUtil.class) {
                if (sInstance == null)
                    sInstance = new BroadcastMessage();
            }
        }
        return sInstance;
    }
    private BroadcastMessage() {}

    public void sendFragmentChange(Context context,
                                   String prevJson,
                                   String nextJson,
                                   String causeType) {
        try {
            Intent i = new Intent(Consts.ACTION_CHANGE_FRAGMENT);
            i.putExtra(Consts.ARG_PREVIOUS_FRAGMENT, prevJson);
            i.putExtra(Consts.ARG_NEXT_FRAGMENT, nextJson);
            i.putExtra(Consts.ARG_CHANGE_CAUSE, causeType);
            context.sendBroadcast(i);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
