package com.tecace.retail.res;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.AnyRes;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.View;
import android.widget.ImageView;

import com.tecace.retail.res.content.AppContent;
import com.tecace.retail.res.observer.ResFileObserver;
import com.tecace.retail.res.receiver.ContentSyncReceiver;
import com.tecace.retail.res.receiver.LanguageSettingReceiver;
import com.tecace.retail.res.util.BitmapAsyncLoader;
import com.tecace.retail.res.util.DrawableAsyncLoader;
import com.tecace.retail.res.util.ResFileUtil;
import com.tecace.retail.res.util.ResUtil;

import java.util.ArrayList;

/**
 * Created by icanmobile on 4/24/17.
 */

public class Res {
    private static final String TAG = Res.class.getSimpleName();

    /**
     * Prevent to create Res object
     */
    private Res() {
        throw new AssertionError();
    }

    //region static methods of initialize and deinitialize

    public static BroadcastReceiver languageSettingReceiver;
    public static BroadcastReceiver contentSyncReceiver;
    /**
     * initializing retail.res module.
     * @param context
     */
    public static void initialize(@NonNull Context context) {
        // watching contents
        ResFileObserver.getInstance().init(context);
        ResFileObserver.getInstance().startWatching();

        // clear cache using Glide
        ResDrawable.getInstance().init(context);

        // register language setting receiver
        languageSettingReceiver = ResUtil.getInstance().registerBroadcastReceiver(context,
                LanguageSettingReceiver.class,
                new String[]{LanguageSettingReceiver.ACTION_LOCALE_CHANGED});

        // register content sync receiver
        contentSyncReceiver = ResUtil.getInstance().registerBroadcastReceiver(context,
                ContentSyncReceiver.class,
                new String[]{ ContentSyncReceiver.ACTION_SYNC_STARTED,
                        ContentSyncReceiver.ACTION_UPDATE,
                        ContentSyncReceiver.ACTION_SYNC_COMPLETED,
                        ContentSyncReceiver.ACTION_SYNC_ABORTED});
    }
    /**
     * de-initializing retail.res.module.
     * @param context
     */
    public static void deinitialize(@NonNull Context context) {
        ResFileObserver.getInstance().stopWatching();
        ResFileObserver.getInstance().deinit();

        if (languageSettingReceiver != null) {
            context.unregisterReceiver(languageSettingReceiver);
            languageSettingReceiver = null;
        }
        if (contentSyncReceiver != null) {
            context.unregisterReceiver(contentSyncReceiver);
            contentSyncReceiver = null;
        }
    }
    @Deprecated
    public static void init(@NonNull Context context) {
        // watching contents
        ResFileObserver.getInstance().init(context);
        ResFileObserver.getInstance().startWatching();

        // initialize Universal Image Loader
        ResDrawable.getInstance().initImageLoader(context);
    }
    @Deprecated
    public static void deinit(Context context) {
        ResFileObserver.getInstance().stopWatching();
        ResFileObserver.getInstance().deinit();
    }
    //endregion




    //region static methods of resource id and name
    /**
     * get resource id
     * @param context
     * @param data
     * @return
     */
    public static @AnyRes int getResId(@NonNull Context context, @NonNull String data) {
        return ResUtil.getInstance().getResId(context, data);
    }
    /**
     * get resource name
     * @param context
     * @param resId
     * @return
     */
    public static @Nullable String getEntryName(@NonNull Context context, @AnyRes int resId) {
        return ResUtil.getInstance().getEntryName(context, resId);
    }
    //endregion




    //region static methods of missing content files
    /**
     * missing content
     * @param context
     * @param file
     * @return
     */
    public static boolean isMissingContentFile(@NonNull Context context, @NonNull String file) {
        return ResUtil.getInstance().isMissingContentFile(context, file);
    }
    //endregion




    //region static methods of preparing contents
    /**
     *  http://jicon.tecace.com:8090/display/RBP/Documents
     *  Please reference "Content Downloader Process.docx" file
     */
    private static ResContent resContent = AppContent.getInstance();
    public static void prepareContent(ResContent.Source source,
                                      @NonNull Activity activity,
                                      String popup) {
        prepareContent(source, activity, popup, null, null, null);
    }
    public static void prepareContent(ResContent.Source source,
                                      @NonNull Activity activity,
                                      String popup,
                                      String url,
                                      String wifiDisconnectedMessage,
                                      String contentSizeWarningMessage) {
        resContent.prepareContent(source, activity, popup, url, wifiDisconnectedMessage, contentSizeWarningMessage);
    }
    public static void deinitDialog() {
        resContent.deinitDialog();
    }
    //endregion




    //region static methods of preload contents
    /**
     * load preload removed file list
     * @param json
     */
    public static void removedPreloadContentList(Context context, String json) {
        ResFileUtil.getInstance().removedPreloadContentList(context, json);
    }
    public static void removedPreloadContentList(Context context) {
        ResFileUtil.getInstance().removedPreloadContentList(context);
    }
    //endregion




    //region static methods of language and string
    /**
     * set language
     * @param context
     * @param language
     * @param country
     */
    public static void setLanguage(@NonNull Context context, @NonNull String language, @NonNull String country) {
        ArrayList<String> stringsXMLs = new ArrayList<>();
        setLanguage(context, language, country, stringsXMLs);
    }
    public static void setLanguage(@NonNull Context context, @NonNull String language, @NonNull String country, String stringsXML) {
        ArrayList<String> stringsXMLs = new ArrayList<>();
        stringsXMLs.add(stringsXML);
        setLanguage(context, language, country, stringsXMLs);
    }
    public static void setLanguage(@NonNull Context context, @NonNull String language, @NonNull String country, ArrayList<String> stringsXMLs) {
        ResString.getInstance().setLanguage(context, language, country, stringsXMLs);
        ResDimen.getInstance().setDimenXmlFile(context, language, country);
        ResFileUtil.getInstance().resetFileSets();
    }

    @Deprecated
    public static void setLanguage(@NonNull Context context, @NonNull String file) {
        ResString.getInstance().setLanguage(context, file);
        ResDimen.getInstance().setDimenXmlFile(context, file);
    }

    /**
     * get language
     * @return
     */
    @Deprecated
    public static @Nullable String getLanguage() {
        return ResString.getInstance().getLanguage();
    }
    /**
     * get string resource
     * @param context
     * @param data
     * @return
     */
    public static @Nullable String getString(@NonNull Context context, @NonNull String data) {
        return ResString.getInstance().getString(context, data);
    }
    /**
     * get string resource
     * @param context
     * @param resId
     * @return
     */
    public static @Nullable String getString(@NonNull Context context, @StringRes int resId) {
        return ResString.getInstance().getString(context, resId);
    }
    //endregion




    //region static methods of dimens
    /**
     * get dimen resource based on pixel
     * @param context
     * @param data
     * @return if dimen resource is not found, return null as a default value.
     */
    public static @Nullable Integer getPixelDimen(@NonNull Context context, @NonNull String data) {
        return ResDimen.getInstance().getPixelDimen(context, data);
    }
    /**
     * get dimen resource based on pixel
     * @param context
     * @param resId
     * @return if dimen resource is not found, return null as a default value.
     */
    public static @Nullable Integer getPixelDimen(@NonNull Context context, @DimenRes int resId) {
        return ResDimen.getInstance().getPixelDimen(context, resId);
    }
    /**
     * get dimen resource
     * @param context
     * @param data
     * @return if dimen resource is not found, return null as a default value.
     */
    public static @Nullable Float getDimen(@NonNull Context context, @NonNull String data) {
        return ResDimen.getInstance().getDimen(context, data);
    }
    /**
     * get dimen resource
     * @param context
     * @param resId
     * @return if dimen resource is not found, return null as a default value.
     */
    public static @Nullable Float getDimen(@NonNull Context context, @DimenRes int resId) {
        return ResDimen.getInstance().getDimen(context, resId);
    }
    //endregion




    //region static methods of color resource.
    /**
     * get color resource
     * @param context
     * @param data
     * @return if color resource is not found, return -1 as a default value.
     */
    public static int getColor(@NonNull Context context, @NonNull String data) {
        return ResColor.getInstance().getColor(context, data);
    }
    /**
     * get color resource
     * @param context
     * @param resId
     * @return if color resource is not found, return -1 as a default value.
     */
    public static int getColor(@NonNull Context context, @ColorRes int resId) {
        return ResColor.getInstance().getColor(context, resId);
    }
    //endregion




    //region static methods of image resource using Glide
    /**
     * Get a bitmap using Glide
     * @param context
     * @param data
     * @param bitmapAsyncLoader
     */
    public static void getBitmap(Context context, Object data, BitmapAsyncLoader bitmapAsyncLoader) {
        ResDrawable.getInstance().getBitmap(context, data, bitmapAsyncLoader);
    }
    /**
     * Get a drawable using Glide
     * @param context
     * @param data
     * @param drawableAsyncLoader
     */
    public static void getDrawable(@NonNull Context context, @NonNull Object data, @NonNull DrawableAsyncLoader drawableAsyncLoader) {
        ResDrawable.getInstance().getDrawable(context, data, drawableAsyncLoader);
    }
    /**
     * show image to image control using Glide
     * @param context
     * @param imageView
     * @param data
     */
    public static void showImage(@NonNull Context context, @NonNull ImageView imageView, @NonNull String data) {
        ResDrawable.getInstance().showImage(context, imageView, data);
    }
    /**
     * show image to image control using Glide
     * @param context
     * @param imageView
     * @param resId
     */
    public static void showImage(@NonNull Context context, @NonNull ImageView imageView, @DrawableRes int resId) {
        ResDrawable.getInstance().showImage(context, imageView, resId);
    }

    /**
     * clear image cache based on Glide
     * @param context
     */
    public static void clearCache(@NonNull Context context) {
        ResDrawable.getInstance().clearCache(context);

        ResFileUtil.getInstance().resetFileSets();
    }
    /**
     * return whether image cache was cleared or not.
     * @param context
     * @return
     */
    public static boolean isCacheCleared(Context context) {
        return ResDrawable.getInstance().isCacheCleared(context);
    }

    /**
     * release process is supported by Glide already. We don't need to use this method.
     * @param context
     * @param view
     */
    public static void releaseViewRes(@NonNull Context context, @NonNull View view) {}
    //endregion




    //region deprecated static methods of image resource using universal image loader
    @Deprecated
    private static boolean sCacheOption = true;
    @Deprecated
    private static boolean getCacheOption() {
        return sCacheOption;
    }
    @Deprecated
    public static Bitmap getBitmap(@NonNull Context context, @NonNull String data) {
        return ResDrawable.getInstance().getBitmap(context, data, false, getCacheOption());
    }
    @Deprecated
    public static Bitmap getBitmap(@NonNull Context context, @NonNull String data, boolean originSize) {
        return ResDrawable.getInstance().getBitmap(context, data, originSize, getCacheOption());
    }
    @Deprecated
    public static Bitmap getBitmap(@NonNull Context context, @NonNull String data, int sampleSize) {
        return ResDrawable.getInstance().getBitmap(context, data, sampleSize);
    }
    @Deprecated
    public static Bitmap getBitmap(@NonNull Context context, @DrawableRes int resId) {
        return ResDrawable.getInstance().getBitmap(context, resId, false, getCacheOption());
    }
    @Deprecated
    public static Bitmap getBitmap(@NonNull Context context, @DrawableRes int resId, boolean originSize) {
        return ResDrawable.getInstance().getBitmap(context, resId, originSize, getCacheOption());
    }
    @Deprecated
    public static Bitmap getBitmap(@NonNull Context context, @DrawableRes int resId, int sampleSize) {
        return ResDrawable.getInstance().getBitmap(context, resId, sampleSize);
    }
    @Deprecated
    public static void releaseBitmap(@NonNull Bitmap bitmap) {
        ResDrawable.getInstance().releaseBitmap(bitmap, getCacheOption());
    }

    @Deprecated
    public static Drawable getDrawable(@NonNull Context context, @NonNull String data) {
        return ResDrawable.getInstance().getDrawable(context, data, false, getCacheOption());
    }
    @Deprecated
    public static Drawable getDrawable(@NonNull Context context, @NonNull String data, boolean originSize) {
        return ResDrawable.getInstance().getDrawable(context, data, originSize, getCacheOption());
    }
    @Deprecated
    public static Drawable getDrawable(@NonNull Context context, @NonNull String data, int sampleSize) {
        return ResDrawable.getInstance().getDrawable(context, data, sampleSize);
    }
    @Deprecated
    public static Drawable getDrawable(@NonNull Context context, @DrawableRes int resId) {
        return ResDrawable.getInstance().getDrawable(context, resId, false, getCacheOption());
    }
    @Deprecated
    public static Drawable getDrawable(@NonNull Context context, @DrawableRes int resId, boolean originSize) {
        return ResDrawable.getInstance().getDrawable(context, resId, originSize, getCacheOption());
    }
    @Deprecated
    public static Drawable getDrawable(@NonNull Context context, @DrawableRes int resId, int sampleSize) {
        return ResDrawable.getInstance().getDrawable(context, resId, sampleSize);
    }
    @Deprecated
    public static void releaseDrawableRes(@NonNull View view) {
        ResDrawable.getInstance().releaseDrawable(view, getCacheOption());
    }

    @Deprecated
    public static void displayImage(@NonNull Context context, @NonNull ImageView imageView, @NonNull String data) {
        ResDrawable.getInstance().displayImage(context, imageView, data, false);
    }
    @Deprecated
    public static void displayImage(@NonNull Context context, @NonNull ImageView imageView, @NonNull String data, boolean originSize) {
        ResDrawable.getInstance().displayImage(context, imageView, data, originSize);
    }
    @Deprecated
    public static void displayImage(@NonNull Context context, @NonNull ImageView imageView, @NonNull String data, boolean originSize, boolean removeFromCache) {
        ResDrawable.getInstance().displayImage(context, imageView, data, originSize, removeFromCache);
    }
    @Deprecated
    public static void displayImage(@NonNull Context context, @NonNull ImageView imageView, @DrawableRes int resId) {
        ResDrawable.getInstance().displayImage(context, imageView, resId, false);
    }
    @Deprecated
    public static void displayImage(@NonNull Context context, @NonNull ImageView imageView, @DrawableRes int resId, boolean originSize) {
        ResDrawable.getInstance().displayImage(context, imageView, resId, originSize);
    }
    @Deprecated
    public static void clearCache() {
        ResDrawable.getInstance().clearCache();
        ResFileUtil.getInstance().resetFileSets();
    }
    //endregion




    //region observers of retail.res module.
    @Deprecated
    public static void registerContentSyncObserver(@NonNull ContentSyncReceiver.ContentSyncObserver observer) {
        ResObserver.getInstance().registerContentSyncObserver(observer);
    }

    @Deprecated
    public static void removeContentSyncObserver(@NonNull ContentSyncReceiver.ContentSyncObserver observer) {
        ResObserver.getInstance().removeContentSyncObserver(observer);
    }

    @Deprecated
    public static void registerLanguageSettingObserver(@NonNull LanguageSettingReceiver.LanguageSettingObserver observer) {
        ResObserver.getInstance().registerLanguageSettingObserver(observer);
    }

    @Deprecated
    public static void removeLanguageSettingObserver(@NonNull LanguageSettingReceiver.LanguageSettingObserver observer) {
        ResObserver.getInstance().removeLanguageSettingObserver(observer);
    }



    public static void registerFileObserver(@NonNull ResFileObserver.BaseFileObserver observer) {
        ResObserver.getInstance().registerFileObserver(observer);
    }
    public static void removeFileObserver(@NonNull ResFileObserver.BaseFileObserver observer) {
        ResObserver.getInstance().removeFileObserver(observer);
    }
    //endregion

}
