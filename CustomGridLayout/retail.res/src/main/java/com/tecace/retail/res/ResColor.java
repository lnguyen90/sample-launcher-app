package com.tecace.retail.res;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;

import com.tecace.retail.res.util.ResUtil;

/**
 * Created by lnguye68 on 12/6/17.
 * Updated by smheo on 12/14/2017 : Added localization functionality
 */

class ResColor {

    private static final String TAG = ResColor.class.getSimpleName();

    private static volatile ResColor sInstance = null;
    static ResColor getInstance() {
        if (sInstance == null) {
            synchronized (ResColor.class) {
                if (sInstance == null)
                    sInstance = new ResColor();
            }
        }
        return sInstance;
    }
    private ResColor(){}

    /**
     * getColor
     * @param context
     * @param data
     * @return if color resource is not found, return -1 as a default value.
     */
    int getColor(@NonNull Context context, @NonNull String data) {
        if (context == null || data == null || data.length() == 0) return -1;

        int resId = ResUtil.getInstance().getResId(context, data);
        if (resId <= 0) {
            return Color.parseColor(data);
        }

        String name = ResUtil.getInstance().getEntryName(context, resId);
        if (name == null) return -1;  //not found resource id so return data

        return context.getColor(resId);

    }

    /**
     * getColor
     * @param context
     * @param resId
     * @return if color resource is not found, return -1 as a default value.
     */
    int getColor(@NonNull Context context, @ColorRes int resId) {
        if (context == null || resId <= 0) return -1;

        String name = ResUtil.getInstance().getEntryName(context, resId);
        if (name == null) return -1;

        return context.getColor(resId);
    }
}
