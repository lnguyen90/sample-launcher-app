package com.tecace.retail.res.content;

public interface PrepareContentListener {
    PrepareContent getPrepareContent();
}
