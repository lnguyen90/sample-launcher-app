package com.tecace.retail.res.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.support.annotation.AnyRes;
import android.support.annotation.NonNull;

import com.tecace.retail.util.exception.MissingContentException;

import java.io.File;

/**
 * Created by icanmobile on 5/10/17.
 */

public class ResUtil {
    private static final String TAG = ResUtil.class.getSimpleName();

    private static volatile ResUtil sInstance;
    public static ResUtil getInstance() {
        if (sInstance == null) {
            synchronized (ResUtil.class) {
                if (sInstance == null)
                    sInstance = new ResUtil();
            }
        }
        return sInstance;
    }

    /**
     * getResId
     * @param context
     * @param data
     * @return
     */
    public int getResId(@NonNull Context context, @NonNull String data) {
        int delimiter = data.indexOf('/');
        if (delimiter == -1 ) {
            return 0;
        }
        String type = data.substring(1, delimiter);
        String name = data.substring(delimiter + 1);
        return getResId(context, name, type);
    }

    /**
     * getResId
     * @param context
     * @param name
     * @param type
     * @return
     */
    private int getResId(@NonNull Context context, @NonNull String name, @NonNull String type) {
        return context.getResources().getIdentifier(name, type, context.getPackageName());
    }

    /**
     * getEntryName
     * @param context
     * @param resId
     * @return
     */
    public String getEntryName(@NonNull Context context, @AnyRes int resId) {
        if (context == null || resId <= 0) return null;
        return context.getResources().getResourceEntryName(resId);
    }


    //**********************************************************************************************

    /**
     * isMissingContentFile
     * @param context
     * @param filename
     * @return
     */
    public boolean isMissingContentFile(@NonNull Context context, @NonNull String filename) {
        if (context == null || filename == null || filename.length() == 0) return true;
        File current = new File(ResFileUtil.getInstance().getResourcePathWithLocale(context, filename));
//        return !current.exists();
        return !(current.exists() || ResAssetUtil.getInstance().hasAssetFile(context, filename));
    }

    public boolean isContentExist(@NonNull Context context, @NonNull String filename) throws MissingContentException {
        if (filename == null || filename.length() == 0) return false;
        File current = new File(ResFileUtil.getInstance().getResourcePathWithLocale(context, filename));
        if (!current.exists() && !ResAssetUtil.getInstance().hasAssetFile(context, filename)) {
            throw new MissingContentException(filename);
        }
        return true;
    }

    public BroadcastReceiver registerBroadcastReceiver(@NonNull Context context, @NonNull Class c, @NonNull String[] actions) {
        final IntentFilter filter = new IntentFilter();
        try {
            for (String action : actions)
                filter.addAction(action);
            BroadcastReceiver receiver = (BroadcastReceiver)c.newInstance();
            context.registerReceiver(receiver, filter);
            return receiver;
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
