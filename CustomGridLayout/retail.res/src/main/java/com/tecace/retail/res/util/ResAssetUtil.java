package com.tecace.retail.res.util;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.tecace.retail.res.Res;
import com.tecace.retail.util.AssetUtil;
import com.tecace.retail.util.FileUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by TaeSeok on 6/19/2017.
 * Class to obtain resources files that are located in asset folder
 */

public class ResAssetUtil {
    private static final String TAG = ResAssetUtil.class.getSimpleName();

    private static volatile ResAssetUtil sInstance;

    /**
     * Get ResAssetUtil singleton object.
     * @return ResAssetUtil singleton object.
     */
    public static ResAssetUtil getInstance() {
        if (sInstance == null) {
            synchronized (ResAssetUtil.class) {
                if (sInstance == null)
                    sInstance = new ResAssetUtil();
            }
        }
        return sInstance;
    }

    private ResAssetUtil() {}

    /**
     * Check if resources files are located in asset folder.
     *
     * @param context Application or activity context.
     * @param fileName String which is for file name.
     * @return true if resource with name exists, false if no resource exists.
     */
    public boolean hasAssetFile(@NonNull Context context, @NonNull String fileName){
        if (context == null || fileName == null || fileName.length() == 0) return false;

        InputStream is = null;
        try {
            is = context.getResources().getAssets().open(fileName);
            Log.d(TAG,  "######################## hasAssetFile : " + fileName.toString());
            return true;
        } catch (IOException e) {
//            e.printStackTrace();
        } finally {
            try {
                if(is != null) {
                    is.close();
                }
            } catch (IOException e) {
//                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * Create and return bitmap with data file name.
     *
     * @param context Application or activity context.
     * @param data String with is for file name.
     * @return Bitmap with given data (file) name. null if error happens.
     */
    public Bitmap getBitmap(@NonNull Context context, @NonNull String data)
    {
        InputStream is = null;
        try {
            is = context.getResources().getAssets().open(data);
            return BitmapFactory.decodeStream(is);
        } catch (IOException e) {
//            e.printStackTrace();
        } finally {
            try {
                if(is != null) {
                    is.close();
                }
            } catch (IOException e) {
//                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Create and return AssetFileDescriptor.
     *
     * @param context Application or activity context.
     * @param videoFileName Video file name to create AssetFileDesctiptor.
     * @return assetFileDescriptor.
     */
    public AssetFileDescriptor getVideoAssetFileDescriptor (@NonNull Context context, @NonNull String videoFileName) {
        if (context == null || videoFileName == null || videoFileName.length() == 0) return null;

        AssetFileDescriptor assetFileDescriptor = null;
        try {
            assetFileDescriptor = context.getAssets().openFd(videoFileName);
        } catch (IOException e) {
//            e.printStackTrace();
        }
        return assetFileDescriptor;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    //[MOVE_TO_RESMANAGER]

    public String getJsonText(Context context, String json) {
        String text = getText(context, json);
        if (Strings.isNullOrEmpty(text)) {
            //[MOVE_TO_RESMANAGER]
            text = GetTextFromAssetWithLocale(context, json);
        }
        return text;
    }

    public <T> T loadJsonModel(Context context, String json, Class<T> classOfT) {
        String data = getJsonText(context, json);
        if (Strings.isNullOrEmpty(data)) {
            return null;
        }

        Gson gson = new Gson();
        try {
            return gson.fromJson(data, classOfT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public <T> T loadJsonModel(Context context, String json, Type type) {
        String data = getJsonText(context, json);
        if (Strings.isNullOrEmpty(data)) {
            return null;
        }

        Gson gson = new Gson();
        try {
            return gson.fromJson(data, type);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private String getText(Context context, String filename) {
        StringBuilder ret = new StringBuilder();

        //[MOVE_TO_RESMANAGER]
//        String fullFilePath = FileUtil.getFileFullPathWithLocale(context, filename);
        String fullFilePath = ResFileUtil.getInstance().getResourcePathWithLocale(context, filename);

        //[MOVE_TO_RESMANAGER]
//        boolean fileExist = FileUtil.exist(fullFilePath);
        boolean fileExist = ResFileUtil.getInstance().exist(fullFilePath);

        if (!fileExist) {
            Log.d(TAG, "File " + filename + " is not available in files folder");
            return "";
        }

        try (BufferedReader br = new BufferedReader(new FileReader(fullFilePath))) {
            String line = br.readLine();

            while (line != null) {
                ret.append(line);
                line = br.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ret.toString();
    }

    //[MOVE_TO_RESMANAGER]
    public String GetTextFromAssetWithLocale(Context context, String filename) {
        StringBuilder ret = new StringBuilder();

        //If file name does not include "model"  (which means chapter json), don't search asset folder
        if(!filename.contains(ResConst.MODEL_FOLDER)) return ret.toString();

        ArrayList<String> filePaths = this.getAssetPathsWithLocale(filename);
        for (int i = 0; i < filePaths.size(); i++) {

            if (hasAssetFile(context, filePaths.get(i))) { //hasAssetFile
                try {
                    InputStream is = context.getAssets().open(filePaths.get(i));
                    InputStreamReader inputStreamReader = new InputStreamReader(is);
                    BufferedReader f = new BufferedReader(inputStreamReader);
                    String line = f.readLine();
                    while (line != null) {
                        ret.append(line);
                        line = f.readLine();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return ret.toString();
            }
        }
        return ret.toString();
    }

    //[MOVE_TO_RESMANAGER]
    private final static int SEARCH_FOLDER_LANG_COUNTRY = 0;
    private final static int SEARCH_FOLDER_LANG = 1;
    private final static int SEARCH_FOLDER_ROOT = 2;
    /**
     * @param fileName
     */
    public ArrayList<String> getAssetPathsWithLocale (@NonNull String fileName) {
        if (fileName == null || fileName.length() == 0) return null;

        String language = Locale.getDefault().getLanguage();
        String country = Locale.getDefault().getCountry();
        String languageAndCountry = String.format("%s-r%s", language, country);

        ArrayList<String> filePaths = new ArrayList<String>();

        filePaths.add(SEARCH_FOLDER_LANG_COUNTRY, new StringBuilder().append(languageAndCountry).append("/").append(fileName).toString());
        filePaths.add(SEARCH_FOLDER_LANG, new StringBuilder().append(language).append("/").append(fileName).toString());
        filePaths.add(SEARCH_FOLDER_ROOT, new StringBuilder().append(fileName).toString());

        return filePaths;
    }
}
