package com.tecace.retail.res.observer;

import android.content.Context;
import android.os.FileObserver;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;

import com.tecace.retail.res.Res;
import com.tecace.retail.res.ResObserver;
import com.tecace.retail.res.util.ResFileUtil;
import com.tecace.retail.res.util.config.EnvironmentManager;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.tecace.retail.res.util.ResConst.AUDIO_FOLDER;
import static com.tecace.retail.res.util.ResConst.CHAPTER_FOLDER;
import static com.tecace.retail.res.util.ResConst.CONFIGS_MODEL;
import static com.tecace.retail.res.util.ResConst.FILES_FOLDER;
import static com.tecace.retail.res.util.ResConst.FRAME_FOLDER;
import static com.tecace.retail.res.util.ResConst.IMAGE_FOLDER;
import static com.tecace.retail.res.util.ResConst.MODEL_FOLDER;
import static com.tecace.retail.res.util.ResConst.VIDEO_FOLDER;

/**
 * Created by icanmobile on 7/12/17.
 */

public class ResFileObserver {
    private static final String TAG = ResFileObserver.class.getSimpleName();

    private static volatile ResFileObserver sInstance;
    public static ResFileObserver getInstance() {
        if (sInstance == null) {
            synchronized (ResFileObserver.class) {
                if (sInstance == null)
                    sInstance = new ResFileObserver();
            }
        }
        return sInstance;
    }
    private ResFileObserver() {
    }


    public interface BaseFileObserver {
        void updateJsons();
        void updateImages();
    }


    private boolean isWatching;
    private boolean isWatching() {
        return this.isWatching;
    }
    private void setWatching(Boolean isWatchingContents) {
        this.isWatching = isWatchingContents;
    }

    public void startWatching() {
        if (isWatching()) return;
        setWatching(true);

        for(Map.Entry<String, ContentObserver> entry : this.fileObservers.entrySet())
            entry.getValue().startWatching();
    }

    public void stopWatching() {
        if (!isWatching()) return;
        setWatching(false);

        for(Map.Entry<String, ContentObserver> entry : this.fileObservers.entrySet())
            entry.getValue().stopWatching();
    }



    private static WeakReference<Context> weakContext;
    private static HashMap<String, ContentObserver> fileObservers = new HashMap<>();

    public void init(@NonNull Context context) {
        if (isWatching()) return;

        this.weakContext = new WeakReference<>(context);

        addFileObservers(this.fileObservers, ResFileUtil.getInstance().getExternalFilesDir(context));
        addFileObservers(this.fileObservers, ResFileUtil.getInstance().getExternalStorageDir(context));
//        print(this.fileObservers);
    }

    void addFileObservers(HashMap<String, ContentObserver> fileObservers, String dir) {
        if (!fileObservers.containsKey(dir)) {
            fileObservers.put(dir, new ContentObserver(dir, dir.substring(dir.lastIndexOf("/") + 1, dir.length())));
        }

        File file = new File(dir);
        if (file == null) return;

        String[] names = file.list();
        if (names == null) return;  //if storage access permission is off

        for(String name : names) {
            String key = new StringBuilder().append(dir).append("/").append(name).toString();
            if (new File(key).isDirectory()) {
                fileObservers.put(key, new ContentObserver(key, name));
                addFileObservers(fileObservers, key);
            }
        }
    }

    public void deinit() {
        removeFileObservers(fileObservers);
    }
    void removeFileObservers(HashMap<String, ContentObserver> fileObservers) {
        if (fileObservers == null) return;
        for (Map.Entry<String, ContentObserver> entry : fileObservers.entrySet()) {
            ContentObserver fileObserver = entry.getValue();
            fileObserver.removeMessages();
            fileObserver = null;
        }
        fileObservers.clear();
    }

    void print(HashMap<String, ContentObserver> fileObservers) {
        for(Map.Entry<String, ContentObserver> entry : fileObservers.entrySet())
            Log.d(TAG, "##### ICANMOBILE TEST : File Observer = " + entry.getKey());
    }

    //[TA] 11/08/2017. Use Language - Country
    public void reset(@NonNull Context context) {
        if (isWatching()) {
            stopWatching();
        }

        if(this.weakContext != null && this.weakContext.get() != null) {
            this.weakContext = new WeakReference<>(null);
        }
        this.weakContext = new WeakReference<>(context);

        removeFileObservers(fileObservers);
        addFileObservers(fileObservers, ResFileUtil.getInstance().getExternalFilesDir(context));
        addFileObservers(fileObservers, ResFileUtil.getInstance().getExternalStorageDir(context));
//        print(this.fileObservers);

        this.startWatching();
    }

    private static class ContentObserver extends FileObserver {
        private String path;
        private String folder;
        public ContentObserver(@NonNull String path, @NonNull String folder) {
            super(path);
            this.path = path;
            this.folder = folder;
        }

        @Override
        public void onEvent(int event, String filename) {
            if (filename == null) return;

            event &= FileObserver.ALL_EVENTS;

            if (event == CREATE ||
                event == DELETE || event == DELETE_SELF ||
                event == MODIFY ||
                event == MOVED_FROM || event == MOVE_SELF || event == MOVED_TO) {
                    executeEvent(event, this.folder, filename);
            }
        }

        void executeEvent(int event, String folder, String filename) {
            ResFileObserver.getInstance().executeEvent(event, folder, filename);
        }

        void removeMessages() {
            ResFileObserver.getInstance().removeMessages();
        }
    }




    Handler mHandler = new Handler();
    private final int MESSAGE_DELAY_TIME  = 2000;
    UpdatedJsonsRunnable updatedJsonsRunnable;
    UpdatedImagesRunnable updatedImagesRunnable;
    UpdatedFoldersRunnable updatedFoldersRunnable;
    public void executeEvent(int event, String folder, String filename) {
//        Log.d(TAG, "#### ICANMOBILE TEST : executeEvent)+ folder = " + folder + ", file = " + filename);

        switch (folder) {
            case FILES_FOLDER:
                if (filename.equals(CONFIGS_MODEL)){
                    if (updatedJsonsRunnable != null)
                        mHandler.removeCallbacks(updatedJsonsRunnable);
                    updatedJsonsRunnable = new UpdatedJsonsRunnable(weakContext);
                    mHandler.postDelayed(updatedJsonsRunnable, MESSAGE_DELAY_TIME);
                }
                else if (filename.equalsIgnoreCase(CHAPTER_FOLDER) ||
                         filename.equalsIgnoreCase(MODEL_FOLDER) ||
                         filename.equalsIgnoreCase(FRAME_FOLDER) ||
                         filename.equalsIgnoreCase(IMAGE_FOLDER) ||
                         filename.equalsIgnoreCase(AUDIO_FOLDER) ||
                         filename.equalsIgnoreCase(VIDEO_FOLDER) ) {
                    if (updatedFoldersRunnable != null)
                        mHandler.removeCallbacks(updatedFoldersRunnable);
                    updatedFoldersRunnable = new UpdatedFoldersRunnable();
                    mHandler.postDelayed(updatedFoldersRunnable, MESSAGE_DELAY_TIME);
                }
                break;

            case CHAPTER_FOLDER:
            case MODEL_FOLDER:
                if (updatedJsonsRunnable != null)
                    mHandler.removeCallbacks(updatedJsonsRunnable);
                updatedJsonsRunnable = new UpdatedJsonsRunnable(weakContext);
                mHandler.postDelayed(updatedJsonsRunnable, MESSAGE_DELAY_TIME);
                break;

            case IMAGE_FOLDER:
            case FRAME_FOLDER:
            case AUDIO_FOLDER:
            case VIDEO_FOLDER:
                if (updatedImagesRunnable != null)
                    mHandler.removeCallbacks(updatedImagesRunnable);
                updatedImagesRunnable = new UpdatedImagesRunnable();
                mHandler.postDelayed(updatedImagesRunnable, MESSAGE_DELAY_TIME);
                break;

            default:
            {
                switch (filename) {
                    case CHAPTER_FOLDER:
                    case MODEL_FOLDER:
                        if (updatedJsonsRunnable != null)
                            mHandler.removeCallbacks(updatedJsonsRunnable);
                        updatedJsonsRunnable = new UpdatedJsonsRunnable(weakContext);
                        mHandler.postDelayed(updatedJsonsRunnable, MESSAGE_DELAY_TIME);
                        break;

                    case IMAGE_FOLDER:
                    case FRAME_FOLDER:
                    case AUDIO_FOLDER:
                    case VIDEO_FOLDER:
                        if (updatedImagesRunnable != null)
                            mHandler.removeCallbacks(updatedImagesRunnable);
                        updatedImagesRunnable = new UpdatedImagesRunnable();
                        mHandler.postDelayed(updatedImagesRunnable, MESSAGE_DELAY_TIME);
                        break;

                    default:
                        if (updatedFoldersRunnable != null)
                            mHandler.removeCallbacks(updatedFoldersRunnable);
                        updatedFoldersRunnable = new UpdatedFoldersRunnable();
                        mHandler.postDelayed(updatedFoldersRunnable, MESSAGE_DELAY_TIME);
                }
            }
            break;
        }
    }

    public void removeMessages() {
        mHandler.removeCallbacksAndMessages(null);
    }
    private static class UpdatedJsonsRunnable implements Runnable {
        private WeakReference<Context> weakContext;
        public UpdatedJsonsRunnable(WeakReference<Context> weakContext) {
            this.weakContext = weakContext;
        }

        @Override
        public void run() {
            if (this.weakContext == null || this.weakContext.get() == null) return;
            Log.d(TAG, "##### RESET LANGUAGE !!!");

            //update the environment file
            EnvironmentManager.getInstance().updateAppEnvironment(weakContext.get());

            //update language
            Res.setLanguage(weakContext.get(), Locale.getDefault().getLanguage(), Locale.getDefault().getCountry());

            //update removed preload content list
            Res.removedPreloadContentList(weakContext.get());

            ResObserver.getInstance().notifyUpdatedJsons();
        }
    }
    private static class UpdatedImagesRunnable implements Runnable {
        @Override
        public void run() {
            Log.d(TAG, "##### CLEAR CACHE !!!");
            Res.clearCache(weakContext.get());

            ResObserver.getInstance().notifyUpdatedImages();
        }
    }
    private static class UpdatedFoldersRunnable implements Runnable {
        @Override
        public void run() {
            Log.d(TAG, "##### UPDATE FOLDERS !!!");
            resetFolders();
        }
    }

    private static void resetFolders() {
        //reset file observers including new folders
        ResFileObserver.getInstance().reset(weakContext.get());
    }
}
