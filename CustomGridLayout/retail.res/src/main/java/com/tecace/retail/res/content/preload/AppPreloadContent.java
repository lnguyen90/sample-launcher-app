package com.tecace.retail.res.content.preload;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.reflect.TypeToken;
import com.tecace.retail.res.util.ResJsonUtil;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AppPreloadContent implements PreloadContent {

    private static final String TAG = AppPreloadContent.class.getSimpleName();

    private static volatile AppPreloadContent sInstance;
    public static AppPreloadContent getInstance() {
        if (sInstance == null) {
            synchronized (AppPreloadContent.class) {
                if (sInstance == null)
                    sInstance = new AppPreloadContent();
            }
        }
        return sInstance;
    }
    private AppPreloadContent() {}


    private String removedListJson;
    private Set<String> removedList;

    /**
     *
     * @param context
     * @param json
     *
     * example of removed list Json file as below
     * [
     *      "en-rUS/image/fragment_sub_decision_security_fingerprint_scanner_land.png",
     *      "en/image/fragment_sub_decision_security_fingerprint_scanner_land.png",
     *      "image/fragment_sub_decision_security_fingerprint_scanner_land.png"
     * ]
     */
    @Override
    public void removedList(@NonNull Context context, String json) {
        if (json == null || json.length() == 0) return;
        removedListJson = json;

        List<String> removedListModel = ResJsonUtil.getInstance().loadJsonModel(context,
                json, new TypeToken<List<String>>(){}.getType());
        if (removedListModel == null) return;

        if (removedList != null) {
            removedList.clear();
            removedList = null;
        }
        removedList = new HashSet<>();
        for(String item : removedListModel)
            removedList.add(item);
    }

    @Override
    public void removedList(@NonNull Context context) {
        removedList(context, removedListJson);
    }

    @Override
    public boolean isRemoved(String filePath) {
        return removedList == null ? false : removedList.contains(filePath);
    }
}
