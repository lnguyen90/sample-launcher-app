package com.tecace.retail.res.util.config;

import android.content.Context;
import android.util.Log;

import com.google.common.base.Strings;
import com.tecace.retail.res.util.ResJsonUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by smheo on 9/29/2015
 * Updated by smheo on 5/06/2017
 * Updated by smheo on 12/09/2017
 */
public class EnvironmentManager {
    private final String TAG = EnvironmentManager.class.getSimpleName();

    private JSONObject mAppEnvJsonMap;
    private String mEnvFilePath;

    private static volatile EnvironmentManager sInstance = null;
    public static EnvironmentManager getInstance() {
        if (sInstance == null) {
            synchronized (EnvironmentManager.class) {
                if (sInstance == null)
                    sInstance = new EnvironmentManager();
            }
        }
        return sInstance;
    }

    private EnvironmentManager() {}

    public EnvironmentManager setFilePath(String envJson) {
        this.mEnvFilePath = envJson;
        return sInstance;
    }

    public void updateAppEnvironment(Context context) {
        Log.d(TAG, "##ENV Updated teh application environment file++");
        if (context == null || Strings.isNullOrEmpty(mEnvFilePath)) {
            Log.w(TAG, "##ENV The app environment file path is empty");
            return;
        }

        try {
            String jsonData = ResJsonUtil.getInstance().getJsonText(context, mEnvFilePath);
            if (jsonData == null) {
                Log.w(TAG, "##ENV The environment json data is null");
                return;
            }
            mAppEnvJsonMap = new JSONObject(jsonData);
            // Log.d(TAG, "##ENV App Env Json : " + mAppEnvJsonMap.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

    }

    public String getStringValue(Context context, String key, String defaultValue){
        if (context == null) return defaultValue;
        if(mAppEnvJsonMap != null && mAppEnvJsonMap.has(key)) {
            Object getObject;
            String getValue;
            try {
                getObject = mAppEnvJsonMap.get(key);
                if (getObject == null) {
                    return defaultValue;
                }
                getValue = (String) getObject;
            } catch (Exception e) {
                e.printStackTrace();
                return defaultValue;
            }
            // Log.d(TAG, "##ENV - Key : " + key + ", Value : " + getValue);
            return getValue;
        } else {
            return defaultValue;
        }
    }

    public List<Object> getListValue(Context context, String key, List<Object> defaultValue) {
        if (context == null) return defaultValue;
        if(mAppEnvJsonMap != null && mAppEnvJsonMap.has(key)) {
            try {
                JSONArray jsonArray = mAppEnvJsonMap.getJSONArray(key);
                if (jsonArray == null || jsonArray.length() <= 0) {
                    Log.w(TAG, "##ENV env list value is null");
                    return defaultValue;
                }

                List<Object> list = new ArrayList<Object>();
                for (int index = 0; index < jsonArray.length(); index++) {
                    if (jsonArray.isNull(index)) continue;
                    list.add(jsonArray.get(index));
                }
                // Log.d(TAG, "##ENV - Key : " + key + ", Value : " + list.toString());
                return list;
            } catch (JSONException e) {
                    e.printStackTrace();
                    return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    public int getIntValue(Context context, String key, int defaultValue){
        if (context == null) return defaultValue;
        if(mAppEnvJsonMap != null && mAppEnvJsonMap.has(key)) {
            Object getObject;
            int getValue;
            try {
                getObject = mAppEnvJsonMap.get(key);
                if (getObject == null) {
                    return defaultValue;
                }
                getValue = (int) getObject;
            } catch (Exception e) {
                e.printStackTrace();
                return defaultValue;
            }
            // Log.d(TAG, "##ENV - Key : " + key + ", Value : " + getValue);
            return getValue;
        } else {
            return defaultValue;
        }
    }

    public boolean getBooleanValue(Context context, String key, boolean defaultValue) {
        if (context == null) return defaultValue;
        if(mAppEnvJsonMap != null && mAppEnvJsonMap.has(key)) {
            Object getObject;
            boolean getValue;
            try {
                getObject = mAppEnvJsonMap.get(key);
                if (getObject == null) {
                    return defaultValue;
                }
                getValue = (boolean) getObject;
            } catch (Exception e) {
                e.printStackTrace();
                return defaultValue;
            }
            // Log.d(TAG, "##ENV - Key : " + key + ", Value : " + getValue);
            return getValue;
        } else {
            return defaultValue;
        }
    }

}
