package com.tecace.retail.res.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.tecace.retail.res.ResObserver;
import com.tecace.retail.res.util.ResFileUtil;

/**
 * Created by icanmobile on 1/11/18.
 */

public class ContentSyncReceiver extends BroadcastReceiver {

    private static final String TAG = ContentSyncReceiver.class.getSimpleName();

    public interface ContentSyncObserver {
        void startedContentSync(String rootDir);
        void abortedContentSync(String rootDir);
        void completedContentSync(String rootDir);
        void updatedContent(String rootDir, String filename, boolean deleted);
    }

    public static final String ACTION_SYNC_STARTED = "com.samsung.retailcloud.content.sync.STARTED";
    public static final String ACTION_SYNC_ABORTED = "com.samsung.retailcloud.content.sync.ABORTED";
    public static final String ACTION_SYNC_COMPLETED = "com.samsung.retailcloud.content.sync.COMPLETED";

    public static final String ACTION_UPDATE = "com.samsung.retailcloud.content.UPDATE";
    private static final String EXTRA_FILENAME = "filename";
    private static final String EXTRA_DELETED = "deleted";

    //https://stackoverflow.com/questions/28083430/communication-between-broadcastreceiver-and-activity-android
    //adb shell am broadcast -a com.samsung.retailcloud.content.sync.STARTED
    //adb shell am broadcast -a com.samsung.retailcloud.content.sync.ABORTED
    //adb shell am broadcast -a com.samsung.retailcloud.content.sync.COMPLETED
    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case ACTION_SYNC_STARTED :
                Log.i(TAG, "#### RMS CONTENT SYNC STARTED");
                ResObserver.getInstance().notifyStartedContentSync(
                        ResFileUtil.getInstance().getExternalFilesDir(context)
                );
                break;

            case ACTION_SYNC_ABORTED :
                Log.e(TAG, "#### RMS CONTENT SYNC ABORTED");
                ResObserver.getInstance().notifyAbortedContentSync(
                        ResFileUtil.getInstance().getExternalFilesDir(context)
                );
                break;

            case ACTION_SYNC_COMPLETED :
                Log.i(TAG, "#### RMS CONTENT SYNC COMPLETED");
                ResObserver.getInstance().notifyCompletedContentSync(
                        ResFileUtil.getInstance().getExternalFilesDir(context)
                );
                break;

            case ACTION_UPDATE :    //The file observer will detected the delete event.
                Log.i(TAG, "#### RMS CONTENT SYNC UPDATED");
                String filename = intent.getStringExtra(EXTRA_FILENAME);
                boolean isDeleted = intent.getBooleanExtra(EXTRA_DELETED, false);
                Log.i(TAG, filename + ":" + isDeleted);

                ResObserver.getInstance().notifyUpdatedContent(
                        ResFileUtil.getInstance().getExternalFilesDir(context),
                        filename, isDeleted);
                break;
        }
    }
}
