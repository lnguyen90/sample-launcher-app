package com.tecace.retail.res.util;

import android.graphics.Bitmap;

/**
 * Created by JW on 4/19/18.
 */
public interface BitmapAsyncLoader {
    void onBitmapReady(Bitmap bitmap);
}
