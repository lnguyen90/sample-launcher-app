package com.tecace.retail.res;

import android.support.annotation.NonNull;

import com.tecace.retail.res.observer.ResFileObserver;
import com.tecace.retail.res.receiver.ContentSyncReceiver;
import com.tecace.retail.res.receiver.LanguageSettingReceiver;

import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

/**
 * Created by icanmobile on 1/11/18.
 */

public class ResObserver {

    private static final String TAG = ResObserver.class.getSimpleName();

    private static volatile ResObserver sInstance;
    public static ResObserver getInstance() {
        if (sInstance == null) {
            synchronized (ResObserver.class) {
                if (sInstance == null)
                    sInstance = new ResObserver();
            }
        }
        return sInstance;
    }
    private ResObserver() {}


    //region ContentSyncObserver
    private static Set<ContentSyncReceiver.ContentSyncObserver> contentSyncObservers
            = Collections.newSetFromMap(new WeakHashMap<ContentSyncReceiver.ContentSyncObserver, Boolean>());
    public void registerContentSyncObserver(@NonNull ContentSyncReceiver.ContentSyncObserver observer) {
        if (!contentSyncObservers.contains(observer))
            contentSyncObservers.add(observer);
    }
    public void removeContentSyncObserver(@NonNull ContentSyncReceiver.ContentSyncObserver observer) {
        if (contentSyncObservers.contains(observer))
            contentSyncObservers.remove(observer);
    }
    public void notifyStartedContentSync(String rootDir) {
        for(ContentSyncReceiver.ContentSyncObserver observer : contentSyncObservers)
            observer.startedContentSync(rootDir);
    }
    public void notifyAbortedContentSync(String rootDir) {
        for(ContentSyncReceiver.ContentSyncObserver observer : contentSyncObservers)
            observer.abortedContentSync(rootDir);
    }
    public void notifyCompletedContentSync(String rootDir) {
        for(ContentSyncReceiver.ContentSyncObserver observer : contentSyncObservers)
            observer.completedContentSync(rootDir);
    }
    public void notifyUpdatedContent(String rootDir, String filename, boolean deleted) {
        for(ContentSyncReceiver.ContentSyncObserver observer : contentSyncObservers)
            observer.updatedContent(rootDir, filename, deleted);
    }
    //endregion




    //region LanguageSettingObserver
    private static Set<LanguageSettingReceiver.LanguageSettingObserver> languageSettingObservers
            = Collections.newSetFromMap(new WeakHashMap<LanguageSettingReceiver.LanguageSettingObserver, Boolean>());
    public void registerLanguageSettingObserver(@NonNull LanguageSettingReceiver.LanguageSettingObserver observer) {
        if (!languageSettingObservers.contains(observer))
            languageSettingObservers.add(observer);
    }
    public void removeLanguageSettingObserver(@NonNull LanguageSettingReceiver.LanguageSettingObserver observer) {
        if (languageSettingObservers.contains(observer))
            languageSettingObservers.remove(observer);
    }
    public void notifyChangedLanguageSetting(String rootDir) {
        for(LanguageSettingReceiver.LanguageSettingObserver observer : languageSettingObservers)
            observer.changedLanguage();
    }
    //endregion




    //region ResContentObserver
    private static Set<ResFileObserver.BaseFileObserver> fleObservers
            = Collections.newSetFromMap(new WeakHashMap<ResFileObserver.BaseFileObserver, Boolean>());
    public void registerFileObserver(@NonNull ResFileObserver.BaseFileObserver observer) {
        if (!fleObservers.contains(observer))
            fleObservers.add(observer);
    }
    public void removeFileObserver(@NonNull ResFileObserver.BaseFileObserver observer) {
        if (fleObservers.contains(observer))
            fleObservers.remove(observer);
    }
    public void notifyUpdatedJsons() {
        for(ResFileObserver.BaseFileObserver observer : fleObservers)
            observer.updateJsons();
    }
    public void notifyUpdatedImages() {
        for(ResFileObserver.BaseFileObserver observer : fleObservers)
            observer.updateImages();
    }
    //endregion

}
