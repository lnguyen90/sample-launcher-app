package com.tecace.retail.res.util;


import android.content.Context;
import android.util.Log;

import com.google.common.base.Strings;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Type;

/**
 * Created by TaeSeok on 12/5/2017.
 */

//[MOVE_TO_RESMANAGER]
public class ResJsonUtil {
    private static final String TAG = ResJsonUtil.class.getSimpleName();

    private static volatile ResJsonUtil sInstance = null;
    public static ResJsonUtil getInstance() {
        if (sInstance == null) {
            synchronized (ResJsonUtil.class) {
                if (sInstance == null)
                    sInstance = new ResJsonUtil();
            }
        }
        return sInstance;
    }
    private ResJsonUtil() {}

    public String getJsonText(Context context, String json) {
        String text = getText(context, json);
        if (Strings.isNullOrEmpty(text)) {
            text = ResAssetUtil.getInstance().GetTextFromAssetWithLocale(context, json);
        }
        return text;
    }

    public <T> T loadJsonModel(Context context, String json, Class<T> classOfT) {
        String data = getJsonText(context, json);
        if (Strings.isNullOrEmpty(data)) {
            return null;
        }

        Gson gson = new Gson();
        try {
            return gson.fromJson(data, classOfT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public <T> T loadJsonModel(Context context, String json, Type type) {
        String data = getJsonText(context, json);
        if (Strings.isNullOrEmpty(data)) {
            return null;
        }

        Gson gson = new Gson();
        try {
            return gson.fromJson(data, type);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private String getText(Context context, String filename) {
        StringBuilder ret = new StringBuilder();

        String fullFilePath = ResFileUtil.getInstance().getResourcePathWithLocale(context, filename);
        boolean fileExist = ResFileUtil.getInstance().exist(fullFilePath);

        if (!fileExist) {
            Log.d(TAG, "File " + filename + " is not available in files folder");
            return "";
        }

        try (BufferedReader br = new BufferedReader(new FileReader(fullFilePath))) {
            String line = br.readLine();

            while (line != null) {
                ret.append(line);
                line = br.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ret.toString();
    }
}
