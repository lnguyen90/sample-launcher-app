package com.tecace.retail.res.util.config;

/**
 * Created by smheo on 9/29/2015.
 * Updated by smheo on 5/06/2017
 */
public class Environments {
    public static final String FLAVOR = "flavor";

    public static final String SUPPORT_LANGUAGES = "supportLanguages";
    public static final String SUPPORT_CARRIERS = "supportCarriers";

    public static final String MAJOR_LANGUAGES = "majorLanguages";
    public static final String IGNORE_MAJOR_LANGUAGES = "ignoreMajorLanguages";
    public static final String MAJOR_VIDEO_RESOURCE_FOLDER = "majorVideoResourceFolders";

    // Brightness control
    public static final String ENABLE_BRIGHTNESS_CONTROL = "enableBrightnessControl";
    public static final String APPLY_DEFAULT_BRIGHTNESS = "applyDefaultBrightness";
    public static final String KEY_DEFAULT_BRIGHTNESS_RATIO = "defaultBrightnessRatio";
    // Default brightness ratio
    public static final int VALUE_DEFAULT_BRIGHTNESS = 100;

    // Volume control
    public static final String ENABLE_VOLUME_CONTROL = "enableVolumeControl";
    public static final String APPLY_DEFAULT_VOLUME = "applyDefaultVolume";
    public static final String KEY_DEFAULT_SPEAKER_VOLUME = "defaultSpeakerVolumeRatio";
    public static final String KEY_DEFAULT_HEADSET_VOLUME = "defaultHeadsetVolumeRatio";
    // Default speaker volume ratio
    public static final int VALUE_DEFAULT_SPEAKER_VOLUME = 80;
    // Default headset volume ratio
    public static final int VALUE_DEFAULT_HEADSET_VOLUME = 35;

    public static final String ENABLE_SCREENOFF_DETECTION = "enableScreenOffDetection";

    public static final String ENABLE_REBOOT_DETECTION = "enableRebootDetection";

    public static final String ENABLE_FACEDETECTION = "enableFaceDetection";

    public static final String ENABLE_WEARABLE = "enableWearableFunction";

    public static final String ENABLE_VIDEO_TITLE = "enableSubTitleOnVideo";

    public static final String ENABLE_FOREGROUND_DETECT_SERVICE = "enableForegroundDetect";

    // Settings for development
    public static final String SHOW_VIDEO_PAGE_INFO = "showTestVideoContentInfo";
    public static final String SHOW_VIDEO_TIMER = "showTestVideoTimer";
    public static final String SHOW_PLAY_PAUSE_SEEK_BUTTON = "showTestVideoControlButtons";
}
