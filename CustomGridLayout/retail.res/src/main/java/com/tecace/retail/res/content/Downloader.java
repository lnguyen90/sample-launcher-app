package com.tecace.retail.res.content;

//import com.squareup.okhttp.ResponseBody;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface Downloader {
    @Streaming
    @GET
    Call<ResponseBody> downloadFile(@Url String url);
}
