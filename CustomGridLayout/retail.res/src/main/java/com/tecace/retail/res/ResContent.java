package com.tecace.retail.res;

import android.app.Activity;
import android.support.annotation.NonNull;

public interface ResContent {

    enum Source {
        NONE    ("NONE"),
        RMS     ("RMS"),
        OBB     ("OBB"),
        AMAZON  ("AMAZON"),
        AMAZONS3 ("AMAZONS3");

        private String name;
        Source(String s) {
            this.name = s;
        }
        public String toString() {
            return this.name;
        }
    }

    void prepareContent(Source source,
                        @NonNull Activity activity,
                        String popup,
                        String url,
                        String wifiDisconnectedMessage,
                        String contentSizeWarningMessage);

    void deinitDialog();
}
