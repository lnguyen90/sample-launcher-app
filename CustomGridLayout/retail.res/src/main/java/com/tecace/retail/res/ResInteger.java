package com.tecace.retail.res;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.IntegerRes;
import android.support.annotation.NonNull;

import com.tecace.retail.res.util.ResUtil;

/**
 * Created by lnguye68 on 12/6/17.
 */

class ResInteger {

    private static final String TAG = ResInteger.class.getSimpleName();

    private static volatile ResInteger sInstance = null;
    static ResInteger getInstance() {
        if (sInstance == null) {
            synchronized (ResInteger.class) {
                if (sInstance == null)
                    sInstance = new ResInteger();
            }
        }
        return sInstance;
    }
    private ResInteger(){}

    /**
     * getInteger
     * @param context
     * @param data
     * @return if integer resource is not found, return -1 as a default value.
     */
    int getInteger(@NonNull Context context, @NonNull String data) {
        if (context == null || data == null || data.length() == 0) return -1;

        int resId = ResUtil.getInstance().getResId(context, data);
        if (resId <= 0) return -1;    //not found resource id so return data

        String name = ResUtil.getInstance().getEntryName(context, resId);
        if (name == null) return -1;  //not found resource id so return data

        return context.getResources().getInteger(resId);

    }

    /**
     * getInteger
     * @param context
     * @param resId
     * @return if integer resource is not found, return -1 as a default value.
     */
    int getInteger(@NonNull Context context, @IntegerRes int resId) {
        if (context == null || resId <= 0) return -1;

        String name = ResUtil.getInstance().getEntryName(context, resId);
        if (name == null) return -1;

        return context.getResources().getInteger(resId);
    }

}
