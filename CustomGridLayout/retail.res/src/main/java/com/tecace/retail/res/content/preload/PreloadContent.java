package com.tecace.retail.res.content.preload;

import android.content.Context;
import android.support.annotation.NonNull;

public interface PreloadContent {

    void removedList(@NonNull Context context);

    void removedList(@NonNull Context context, String json);

    boolean isRemoved(String filePath);
}
