package com.tecace.retail.res;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.tecace.retail.res.util.ResFileUtil;
import com.tecace.retail.res.util.ResUtil;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by icanmobile on 4/25/17.
 */

class ResString {
    private static final String TAG = ResString.class.getSimpleName();

    private static volatile ResString sInstance = null;
    static ResString getInstance() {
        if (sInstance == null) {
            synchronized (ResString.class) {
                if (sInstance == null)
                    sInstance = new ResString();
            }
        }
        return sInstance;
    }
    private ResString(){}



    private String languageFile = null;
    private ArrayList<String> languageFiles;
    private HashMap<String, String> strings = new HashMap<>();

    /**
     * getLaunguage
     * @return
     */
    @Deprecated
    String getLanguage() {
        return this.languageFile;
    }

    /**
     * setLanguage
     * @param context
     * @param file
     */
    @Deprecated
    void setLanguage(@NonNull Context context, @NonNull String file) {
        if (file == null || file.length() == 0) return;
        //The language file already set, just return.
        if (this.languageFile != null && this.languageFile.equals(file)) return;
        try {
            this.strings.clear();
            this.strings = loadStringsFromXML(context, file);
            this.languageFile = file;
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * setLanguage
     * @param context
     * @param language
     * @param country
     */
    void setLanguage(@NonNull Context context, @NonNull String language, @NonNull String country, ArrayList<String> stringsXMLs) {
        if (stringsXMLs == null) return;
        if (stringsXMLs.size() == 0 && this.languageFiles == null) return;

        try {
            if (stringsXMLs.size() == 0)
                stringsXMLs = this.languageFiles;

            this.strings.clear();
            for (String stringsXML : stringsXMLs) {
                ArrayList<String> filePaths = ResFileUtil.getInstance().getResourcePathsWithLocale(context, language, country, stringsXML);
                loadStringsFromXML(context, filePaths, this.strings);
            }

            this.languageFiles = stringsXMLs;

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * resetLanguage
     * @param context
     */
//    void resetLanguage(@NonNull Context context) {
//        resetLanguage(context, this.languageFile);
//    }

    /**
     * resetLanguage
     * @param context
     * @param file
     */
//    void resetLanguage(@NonNull Context context, @NonNull String file) {
//        if (file == null || file.length() == 0) return;
//        try {
//            this.strings.clear();
//            this.strings = loadStringsFromXML(context, file);
//            this.languageFile = file;
//        } catch (XmlPullParserException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * loadStringsFromXML
     * @param context
     * @param file
     * @return
     * @throws XmlPullParserException
     * @throws IOException
     */
    private HashMap<String, String> loadStringsFromXML(@NonNull Context context, @NonNull String file)
            throws XmlPullParserException, IOException {
        if (context == null || file == null || file.length() == 0) return null;

        HashMap<String, String> strings = new HashMap<>();
        String key = null;
        String value = null;

        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();
        xpp.setInput(new FileInputStream(ResFileUtil.getInstance().getPath(context, file)), "utf-8");
        int eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if(eventType == XmlPullParser.START_DOCUMENT) {
            } else if(eventType == XmlPullParser.END_DOCUMENT) {
            } else if(eventType == XmlPullParser.START_TAG) {
                key = xpp.getAttributeValue(null, "name");
            } else if(eventType == XmlPullParser.TEXT) {
                value = xpp.getText();
            } else if(eventType == XmlPullParser.END_TAG) {
                if (key != null && value != null)
                    strings.put(key, value);
                key = null;
                value = null;
            }

            eventType = xpp.next();
        }
        return strings;
    }
    private void loadStringsFromXML(@NonNull Context context, @NonNull ArrayList<String> filePaths, HashMap<String, String> strings)
            throws XmlPullParserException, IOException {
        if (context == null || filePaths == null || filePaths.isEmpty()) return;

        String key = null;
        String value = null;

        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();
        for(int i = filePaths.size() - 1; i > -1; i--) {
            String filePath = filePaths.get(i);
//            Log.d(TAG, "******* file path : " + filePath.toString() + ", index : " + i + ", out of : " + filePaths.size());
            if(filePath != null && ResFileUtil.getInstance().exist(filePath)) {
//                Log.d(TAG, "******* file path : " + filePath.toString());
                xpp.setInput(new FileInputStream(filePath.toString()), "utf-8");
                int eventType = xpp.getEventType();
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if(eventType == XmlPullParser.START_DOCUMENT) {
                    } else if(eventType == XmlPullParser.END_DOCUMENT) {
                    } else if(eventType == XmlPullParser.START_TAG) {
                        key = xpp.getAttributeValue(null, "name");
                    } else if(eventType == XmlPullParser.TEXT) {
                        value = xpp.getText();
                    } else if(eventType == XmlPullParser.END_TAG) {
                        if (key != null && value != null)
                            strings.put(key, value);
                        key = null;
                        value = null;
                    }
                    eventType = xpp.next();
                }
            }
        }
//        Map<?,?> map = strings;
//        for(Map.Entry<?, ?> e: map.entrySet()){
//            Log.d(TAG, "******* Key : " + e.getKey() + ", Value : " + e.getValue());
//        }
    }



    private String replaces(String s) {
        return s.replace("\n", "").replace("\\n", "\n").replace("\\", "");
    }


    /**
     * getLanguageString
     * @param key
     * @return
     */
    private String getLanguageString(@NonNull String key) {
        if (this.strings == null || this.strings.size() == 0 || key == null || key.length() == 0) return null;

        // By SMHEO (12/18/2017) - Modified the hashmap routine
        return this.strings.get(key);
        // We do not need to use the containsKey method
//        if (this.strings.containsKey(key))
//            return this.strings.get(key);
//        return null;
    }


    /**
     * getString
     * @param context
     * @param data
     * @return
     */
    String getString(@NonNull Context context, @NonNull String data) {
        if (context == null || data == null || data.length() == 0) return null;

        int resId = ResUtil.getInstance().getResId(context, data);
        if (resId <= 0) return data;    //not found resource id so return data

        String name = ResUtil.getInstance().getEntryName(context, resId);
        if (name == null) return data;  //not found resource id so return data

        //find string from the external xml file
        String str = getLanguageString(name);
        return (str != null && str.length() > 0) ? str : (resId > 0) ? context.getString(resId) : null;
    }

    /**
     * getString
     * @param context
     * @param resId
     * @return
     */
    String getString(@NonNull Context context, @StringRes int resId) {
        if (context == null) return null;

        String name = ResUtil.getInstance().getEntryName(context, resId);
        if (name == null) return null;

        //find string from the external xml file
        String str = getLanguageString(name);
        return (str != null && str.length() > 0) ? str : context.getString(resId);
    }
}
