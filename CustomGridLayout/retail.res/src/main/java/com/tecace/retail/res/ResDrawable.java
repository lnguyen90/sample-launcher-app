package com.tecace.retail.res;


import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.tecace.retail.res.util.BitmapAsyncLoader;
import com.tecace.retail.res.util.DrawableAsyncLoader;
import com.tecace.retail.res.util.ResAssetUtil;
import com.tecace.retail.res.util.ResConst;
import com.tecace.retail.res.util.ResFileUtil;
import com.tecace.retail.res.util.ResUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by icanmobile on 4/27/17.
 */

class ResDrawable {
    private static final String TAG = ResDrawable.class.getSimpleName();

    private static volatile ResDrawable sInstance;
    static ResDrawable getInstance() {
        if (sInstance == null) {
            synchronized (ResDrawable.class) {
                if (sInstance == null)
                    sInstance = new ResDrawable();
            }
        }
        return sInstance;
    }
    private ResDrawable() {}




    //region init method of ResDrawable class
    public void init(Context context) {
        clearCache(context);
    }
    //endregion




    //region image caching methods using Glide
    /**
     * get bitmap using Glide
     * @param context
     * @param data
     * @param bitmapAsyncLoader
     */
    void getBitmap(@NonNull Context context, @NonNull Object data, @NonNull BitmapAsyncLoader bitmapAsyncLoader) {
        Glide.with(context)
                .asBitmap()
                .load(getDataPath(context, data))
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        bitmapAsyncLoader.onBitmapReady(resource);
                    }
                });
    }

    /**
     * get drawable using Glide
     * @param context
     * @param data
     * @param drawableAsyncLoader
     */
    void getDrawable(@NonNull Context context, @NonNull Object data, @NonNull DrawableAsyncLoader drawableAsyncLoader) {
        getBitmap(context, data, new BitmapAsyncLoader() {
            @Override
            public void onBitmapReady(Bitmap bitmap) {
                Drawable drawable;
                if (bitmap.getNinePatchChunk() != null) {
                    byte[] chunk = bitmap.getNinePatchChunk();
                    drawable = new NinePatchDrawable(context.getResources(), bitmap, chunk, new Rect(), null);
                } else {
                    drawable = new BitmapDrawable(context.getResources(), bitmap);
                }
                drawableAsyncLoader.onDrawableReady(drawable);
            }
        });
    }

    /**
     * release process is supported by Glide already. We don't need to use this method.
     * @param context
     * @param view
     */
    void releaseView(@NonNull Context context, @NonNull View view) {
//        Glide.with(context).clear(view);
    }

    /**
     * show image to specific image control
     * @param context
     * @param imageView
     * @param data
     */
    void showImage(@NonNull Context context, @NonNull ImageView imageView, @NonNull String data) {
        if (data.length() == 0) return;

        int resId;
        ArrayList<String> filePaths = getCandidateFilePaths(data);
        for( String filePath : filePaths) {
            if (ResFileUtil.getInstance().exist(ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath))) {
                loadImageIntoView(context, "file://"+ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath), imageView);
                return;
            }
            else if (ResAssetUtil.getInstance().hasAssetFile(context, filePath)) {
                loadImageIntoView(context, "assets://"+filePath, imageView);
                return;
            }
        }

        resId = ResUtil.getInstance().getResId(context, data);
        if (resId > 0)
            showImage(context, imageView, resId);
    }
    /**
     * show image to specific image control
     * @param context
     * @param imageView
     * @param resId
     */
    void showImage(@NonNull Context context, @NonNull ImageView imageView, @DrawableRes int resId) {
        String name = ResUtil.getInstance().getEntryName(context, resId);
        if (name == null) return;

        // get file name without extension from resource id
        ArrayList<String> filePaths = getCandidateFilePaths(name);
        for(String filePath : filePaths) {
            if (ResFileUtil.getInstance().exist(ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath))) {
                loadImageIntoView(context, "file://"+ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath), imageView);
                return;
            }
            else if (ResAssetUtil.getInstance().hasAssetFile(context, filePath)) {
                loadImageIntoView(context, "assets://"+filePath, imageView);
                return;
            }
        }
        loadImageIntoView(context, resId, imageView);
    }

    /**
     * get data path to load image resource
     * @param context
     * @param data
     * @return
     */
    private Object getDataPath(@NonNull Context context, @NonNull Object data) {
        //support "@drawable/FILE_NAME", "image/FILE_NAME.png", "image/FILE_NAME.9.png", "image/FILE_NAME.jpg", "image/FILE_NAME.bmp", and "@drawable/FILE_NAME"
        int resId;
        ArrayList<String> filePaths = getCandidateFilePaths(String.valueOf(data));
        for( String filePath : filePaths) {
            //[TA] 11/10/2017 - Find in language with country -> language -> default folder
            if (ResFileUtil.getInstance().exist(ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath))) {
                return "file://" + ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath);
            }
            else if (ResAssetUtil.getInstance().hasAssetFile(context, filePath)) {
                return "assets://" + filePath;
            }
        }
        resId = ResUtil.getInstance().getResId(context, String.valueOf(data));
        if (resId > 0) return resId;
        return null;
    }

    /**
     * loadImageIntoView using Glide
     * @param context
     * @param data
     * @param imageView
     * Can apply options like these:
    .apply(new RequestOptions()
    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
    .skipMemoryCache(true)
    .format(DecodeFormat.PREFER_ARGB_8888))
     */
    private void loadImageIntoView(@NonNull Context context, @NonNull Object data, @NonNull ImageView imageView) {
        Glide.with(context)
                .load(data)
                .into(imageView);
    }

    /**
     * Check whether the cache folder is cleared
     * @param context
     * @return
     */
    boolean isCacheCleared(@NonNull Context context) {
        long size = 0;
        size += getDirSize(context.getCacheDir());
        size += getDirSize(context.getExternalCacheDir());

        return size < 6_500_000; // 6.5 Mb
    }

    /**
     * clear cache using Glide
     * @param context
     */
    void clearCache(@NonNull Context context) {
        new CacheClearAsyncTask(context).execute();
    }
    private static class CacheClearAsyncTask extends AsyncTask<Void, Void, Void> {
        WeakReference<Context> weakContext;

        public CacheClearAsyncTask(@NonNull Context context) {
            this.weakContext = new WeakReference<>(context);
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Void doInBackground(Void... params) {
            if (this.weakContext == null || this.weakContext.get() == null) return null;
            Glide.get(this.weakContext.get()).clearDiskCache();
            return null;
        }
        @Override
        protected void onPostExecute (Void result)    {
            if (this.weakContext == null || this.weakContext.get() == null) return;
            Glide.get(this.weakContext.get()).clearMemory();
        }
    }

    /**
     * get directory size
     * @param dir
     * @return
     */
    private long getDirSize(File dir){
        long size = 0;
        for (File file : dir.listFiles()) {
            if (file != null && file.isDirectory()) {
                size += getDirSize(file);
            } else if (file != null && file.isFile()) {
                size += file.length();
            }
        }
        return size;
    }

    /**
     * print file size
     * @param size
     * @return
     */
    private static String readableFileSize(long size) {
        if (size <= 0) return "0 Bytes";
        final String[] units = new String[]{"Bytes", "kB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }
    //endregion





    //region candidate file paths
    /**
     * get candidate file paths
     * @param name
     * @return
     */
    private ArrayList<String> getCandidateFilePaths(@NonNull String name) {
        if (name == null || name.length() == 0) return null;

        // when name is as "@drawable/FILE_NAME", extract "FILE_NAME and save it to name
        if (name.contains("@")) {
            int idx = name.replaceAll("\\\\", "/").lastIndexOf("/");
            name = idx >= 0 ? name.substring(idx + 1) : name;
        }

        // when name includes the folder path such as "image/FILE_NAME.png", just return the name.
        if (name.contains("/")) {
            ArrayList<String> candidates = new ArrayList<>();
            candidates.add(name);
            return candidates;
        }

        ArrayList<String> candidates = new ArrayList<>();
        String[] folders = new String[]{ResConst.IMAGE_FOLDER, ResConst.FRAME_FOLDER};
        if (folders == null) return null;
        for(String folder : folders) {
            StringBuilder fileName = new StringBuilder().append(folder).append("/").append(name);
            StringBuilder fileNameWithExt = new StringBuilder();

            //9 patch file
            fileNameWithExt.setLength(0);
            candidates.add(fileNameWithExt.append(fileName.toString()).append(ResConst.NINE_PATCH_EXT).toString());

            //png file
            fileNameWithExt.setLength(0);
            candidates.add(fileNameWithExt.append(fileName.toString()).append(ResConst.PNG_EXT).toString());

            //jpeg file
            fileNameWithExt.setLength(0);
            candidates.add(fileNameWithExt.append(fileName.toString()).append(ResConst.JPEG_EXT).toString());

            //bmp file
            fileNameWithExt.setLength(0);
            candidates.add(fileNameWithExt.append(fileName.toString()).append(ResConst.BMP_EXT).toString());
        }
        return candidates;
    }
    //endregion






    //region image caching methods using universal image loader
    @Deprecated
    private ImageLoaderConfiguration.Builder config;
    @Deprecated
    public void initImageLoader(@NonNull Context context) {

        if (this.config != null) return;

        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        this.config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
//        config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());

        // clear cache
        clearCache();
    }
    @Deprecated
    private DisplayImageOptions originSizeOptions = null;
    @Deprecated
    private DisplayImageOptions getOriginSizeDisplayImageOption() {
        if (originSizeOptions == null) {
            BitmapFactory.Options resizeOptions = new BitmapFactory.Options();
            resizeOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
            resizeOptions.inDither = true;
            resizeOptions.inSampleSize = 1;
            originSizeOptions = new DisplayImageOptions.Builder()
                    .imageScaleType(ImageScaleType.NONE)
                    .cacheOnDisk(true)
                    .cacheInMemory(false)
                    .considerExifParams(true)
                    .decodingOptions(resizeOptions)
                    .build();
        }
        return originSizeOptions;
    }
    @Deprecated
    private DisplayImageOptions resizedOptions = null;
    @Deprecated
    private DisplayImageOptions getDisplayImageOption() {
        if (resizedOptions == null) {
            BitmapFactory.Options resizeOptions = new BitmapFactory.Options();
            resizeOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
            resizeOptions.inDither = true;
            resizeOptions.inSampleSize = 1;
            resizedOptions = new DisplayImageOptions.Builder()
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .cacheOnDisk(true)
                    .cacheInMemory(false)
                    .considerExifParams(true)
                    .decodingOptions(resizeOptions)
                    .build();
        }
        return resizedOptions;
    }
    @Deprecated
    private DisplayImageOptions getOption(boolean originSize) {
        if (originSize)
            return getOriginSizeDisplayImageOption();
        return getDisplayImageOption();
    }

    @Deprecated
    Bitmap getBitmap(@NonNull Context context, @NonNull String data, boolean originSize, boolean cacheOption) {
        if (context == null || data == null || data.length() == 0) return null;

        int resId = ResUtil.getInstance().getResId(context, data);
        if (resId > 0) {    //support "@drawable/FILE_NAME"
            return getBitmap(context, resId, originSize, cacheOption);
        } else { //support "image/FILE_NAME.png", "image/FILE_NAME.9.png", "image/FILE_NAME.jpg", "image/FILE_NAME.bmp", and "@drawable/FILE_NAME"
            if (cacheOption) {
                if (!ImageLoader.getInstance().isInited()) return null;

                ArrayList<String> filePaths = getCandidateFilePaths(data);
                for( String filePath : filePaths) {
                    //[TA] 11/10/2017 - Find in language with country -> language -> default folder
                    if (ResFileUtil.getInstance().exist(ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath))) {
                        return ImageLoader.getInstance().loadImageSync("file://" + ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath), getOption(originSize));
                    } else if (ResAssetUtil.getInstance().hasAssetFile(context, filePath)) {
                        return ImageLoader.getInstance().loadImageSync("assets://" + filePath, getOption(originSize));
                    } else if ( (resId = ResUtil.getInstance().getResId(context, filePath)) > 0){
                        return getBitmap(context, resId, originSize, cacheOption);
                    }
                }
            } else {
                ArrayList<String> filePaths = getCandidateFilePaths(data);
                for (String filePath : filePaths) {
                    //[TA] 11/10/2017 - Find in language with country -> language -> default folder
                    if (ResFileUtil.getInstance().exist(ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath))) {
                        return BitmapFactory.decodeFile(ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath));
                    } else if (ResAssetUtil.getInstance().hasAssetFile(context, filePath)) {
                        return ResAssetUtil.getInstance().getBitmap(context, filePath);
                    } else if ((resId = ResUtil.getInstance().getResId(context, filePath)) > 0) {
                        return getBitmap(context, resId, originSize, cacheOption);
                    }
                }
            }
        }
        return null;
    }
    @Deprecated
    Bitmap getBitmap(@NonNull Context context, @DrawableRes int resId, boolean originSize, boolean cacheOption) {
        if (context == null) return null;
        String name = ResUtil.getInstance().getEntryName(context, resId);
        if (name == null) return null;

        // get file name without extension from resource id
        if (cacheOption) {
            if (!ImageLoader.getInstance().isInited()) return null;

            ArrayList<String> filePaths = getCandidateFilePaths(name);
            for(String filePath : filePaths) {
                //[TA] 11/10/2017 - Find in language with country -> language -> default folder
                if (ResFileUtil.getInstance().exist(ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath))) {
                    return ImageLoader.getInstance().loadImageSync("file://" + ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath), getOption(originSize));
                } else if (ResAssetUtil.getInstance().hasAssetFile(context, filePath)) {
                    return ImageLoader.getInstance().loadImageSync("assets://" + filePath, getOption(originSize));
                }
            }
            return ImageLoader.getInstance().loadImageSync("drawable://"+resId, getOption(originSize));

        }
        else {
            ArrayList<String> filePaths = getCandidateFilePaths(name);
            for(String filePath : filePaths) {
                //[TA] 11/10/2017 - Find in language with country -> language -> default folder
                if (ResFileUtil.getInstance().exist(ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath))) {
                    return BitmapFactory.decodeFile(ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath));
                } else if (ResAssetUtil.getInstance().hasAssetFile(context, filePath)) {
                    return ResAssetUtil.getInstance().getBitmap(context, filePath);
                }
            }
            return BitmapFactory.decodeResource(context.getResources(), resId);
        }
    }
    @Deprecated
    Bitmap getBitmap(@NonNull Context context, @NonNull String data, int sampleSize) {
        if (context == null || data == null || data.length() == 0) return null;

        int resId = ResUtil.getInstance().getResId(context, data);
        if (resId > 0) {    //support "@drawable/FILE_NAME"
            return getBitmap(context, resId, sampleSize);
        }
        else { //support "image/FILE_NAME.png", "image/FILE_NAME.9.png", "image/FILE_NAME.jpg", "image/FILE_NAME.bmp", and "@drawable/FILE_NAME"
            ArrayList<String> filePaths = getCandidateFilePaths(data);
            for( String filePath : filePaths) {
                //[TA] 11/10/2017 - Find in language with country -> language -> default folder
                if (ResFileUtil.getInstance().exist(ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath))) {
                    return loadFile(ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath), sampleSize);
                } else if (ResAssetUtil.getInstance().hasAssetFile(context, filePath)) {
                    return loadAssetFile(context, filePath, sampleSize);
                } else if ( (resId = ResUtil.getInstance().getResId(context, filePath)) > 0){
                    return loadResourceFile(context, resId, sampleSize);
                }
            }
        }
        return null;
    }
    @Deprecated
    Bitmap getBitmap(@NonNull Context context, @DrawableRes int resId, int sampleSize) {
        if (context == null) return null;
        String name = ResUtil.getInstance().getEntryName(context, resId);
        if (name == null) return null;

        // get file name without extension from resource id
        ArrayList<String> filePaths = getCandidateFilePaths(name);
        for(String filePath : filePaths) {
            //[TA] 11/10/2017 - Find in language with country -> language -> default folder
            if (ResFileUtil.getInstance().exist(ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath))) {
                return loadFile(ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath), sampleSize);
            } else if (ResAssetUtil.getInstance().hasAssetFile(context, filePath)) {
                return loadAssetFile(context, filePath, sampleSize);
            }
        }
        return loadResourceFile(context, resId, sampleSize);
    }

    @Deprecated
    private Bitmap loadFile(String filePath, int sampleSize) {
        Bitmap bmp = null;
        File file=new File(filePath);
        FileInputStream fs=null;
        try {
            fs = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            //TODO do something intelligent
            e.printStackTrace();
        }

        try {
            if (fs!=null) bmp=BitmapFactory.decodeFileDescriptor(fs.getFD(), null, getOptions(sampleSize));
        } catch (IOException e) {
            //TODO do something intelligent
            e.printStackTrace();
        } finally{
            if(fs!=null) {
                try {
                    fs.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }

        return bmp;
    }
    @Deprecated
    private Bitmap loadAssetFile(@NonNull Context context, String filePath, int sampleSize) {
        AssetManager assetManager = context.getAssets();
        InputStream istr;
        Bitmap bmp = null;
        try {
            istr = assetManager.open(filePath);
            bmp = BitmapFactory.decodeStream(istr, null, getOptions(sampleSize));
        } catch (IOException e) {
            // handle exception
        }

        return bmp;
    }
    @Deprecated
    private Bitmap loadResourceFile(@NonNull Context context, int resId, int sampleSize) {
        return BitmapFactory.decodeResource(context.getResources(), resId, getOptions(sampleSize));
    }
    @Deprecated
    private BitmapFactory.Options getOptions(int sampleSize) {
        BitmapFactory.Options bfOptions = new BitmapFactory.Options();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            bfOptions.inDither = true;                     //Disable Dithering mode
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            bfOptions.inPurgeable = true;                   //Tell to gc that whether it needs free memory, the Bitmap can be cleared
            bfOptions.inInputShareable = true;              //Which kind of reference will be used to recover the Bitmap data after being clear, when it will be used in the future
        }

        bfOptions.inTempStorage=new byte[32 * 1024];
        bfOptions.inSampleSize=sampleSize;
        return bfOptions;
    }

    @Deprecated
    Drawable getDrawable(@NonNull Context context, @NonNull String data, boolean originSize, boolean cacheOption) {
        Bitmap bmp = getBitmap(context, data, originSize, cacheOption);
        if (bmp == null) return null;
        return getDrawable(context, bmp);
    }
    @Deprecated
    Drawable getDrawable(@NonNull Context context, @DrawableRes int resId, boolean originSize, boolean cacheOption) {
        Bitmap bmp = getBitmap(context, resId, originSize, cacheOption);
        if (bmp == null) return null;
        return getDrawable(context, bmp);
    }
    @Deprecated
    Drawable getDrawable(@NonNull Context context, @NonNull String data, int sampleSize) {
        Bitmap bmp = getBitmap(context, data, sampleSize);
        if (bmp == null) return null;
        return getDrawable(context, bmp);
    }
    @Deprecated
    Drawable getDrawable(@NonNull Context context, @DrawableRes int resId, int sampleSize) {
        Bitmap bmp = getBitmap(context, resId, sampleSize);
        if (bmp == null) return null;
        return getDrawable(context, bmp);
    }
    @Deprecated
    private Drawable getDrawable(@NonNull Context context, @NonNull Bitmap bmp) {
        if (bmp.getNinePatchChunk() != null) {
            byte[] chunk = bmp.getNinePatchChunk();
            return new NinePatchDrawable(context.getResources(), bmp, chunk, new Rect(), null);
        }
        return new BitmapDrawable(context.getResources(), bmp);
    }

    @Deprecated
    void releaseBitmap(@NonNull Bitmap bitmap) {
        if (!bitmap.isRecycled()) {
            bitmap.recycle();
            bitmap = null;
        }
    }
    @Deprecated
    void releaseDrawable(@NonNull Context context, @NonNull View view) {
        //Image View
        if (view instanceof ImageView) {
            releaseDrawable( ((ImageView)view).getDrawable());
            ((ImageView)view).setImageDrawable(null);
        }

        // View Background
        releaseDrawable(view.getBackground());
        view.setBackground(null);
    }
    @Deprecated
    private void releaseDrawable(@NonNull Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            releaseBitmap(bitmapDrawable.getBitmap());
        }
        else if (drawable instanceof NinePatchDrawable) {
            drawable = null;    //can't find the recycle method for NinePatchDrawable
        }
    }
    @Deprecated
    void releaseDrawable(@NonNull View view, boolean cacheOption) {
        if (cacheOption) return;
        if (view == null) return;
        //Image View
        if (view instanceof ImageView) {
            releaseDrawable( ((ImageView)view).getDrawable(), cacheOption );
            ((ImageView)view).setImageDrawable(null);
        }

        // View Background
        releaseDrawable(view.getBackground(), cacheOption);
        view.setBackground(null);
    }
    @Deprecated
    void releaseBitmap(@NonNull Bitmap bitmap, boolean cacheOption) {
        if (cacheOption) return;
        if (bitmap != null && !bitmap.isRecycled()) {
            bitmap.recycle();
            bitmap = null;
        }
    }
    @Deprecated
    private void releaseDrawable(@NonNull Drawable drawable, boolean cacheOption) {
        if (cacheOption) return;
        if (drawable != null) {
            if (drawable instanceof BitmapDrawable) {
                BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
                releaseBitmap(bitmapDrawable.getBitmap(), cacheOption);
            }
            else if (drawable instanceof NinePatchDrawable) {
                drawable = null;    //can't find the recycle method for NinePatchDrawable
            }
        }
    }
    @Deprecated
    void releaseDrawableRes(@NonNull View view, boolean cacheOption) {
        if (cacheOption) return;
        if (view == null) return;
        if ( !(view instanceof ViewGroup)) {
            if (view instanceof ImageView) {
                releaseDrawable(view, cacheOption);
            }
            return;
        }

        if (view instanceof ViewGroup) {
            for(int i=0; i<((ViewGroup) view).getChildCount(); i++) {
                View child = ((ViewGroup) view).getChildAt(i);
                if (child instanceof ImageView) {
                    releaseDrawable(view, cacheOption);
                }
                else {
                    releaseDrawableRes(child, cacheOption);
                }
            }
            releaseDrawable(view, cacheOption);
        }
    }
    @Deprecated
    void displayImage(@NonNull Context context, @NonNull ImageView imageView, @NonNull String data, boolean originSize) {
        displayImage(context, imageView, data, originSize, false);
    }
    @Deprecated
    void displayImage(@NonNull Context context, @NonNull ImageView imageView, @NonNull String data, boolean originSize, boolean removeFromCache) {
        if (!ImageLoader.getInstance().isInited()) return;

        if (context == null || data == null || data.length() == 0) return;

        int resId = ResUtil.getInstance().getResId(context, data);
        if (resId > 0) {    //support "@drawable/FILE_NAME"
            displayImage(context, imageView, resId, originSize);
            if (removeFromCache)
                DiskCacheUtils.removeFromCache("drawable://"+resId, ImageLoader.getInstance().getDiskCache());
        }
        else {  //support "image/FILE_NAME.png", "image/FILE_NAME.9.png", "image/FILE_NAME.jpg", "image/FILE_NAME.bmp", and "@drawable/FILE_NAME"
            ArrayList<String> filePaths = getCandidateFilePaths(data);
            for( String filePath : filePaths) {
                if (ResFileUtil.getInstance().exist(ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath))) {
                    ImageLoader.getInstance().displayImage("file://"+ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath), imageView, getOption(originSize));
                    if (removeFromCache)
                        DiskCacheUtils.removeFromCache("file://"+ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath), ImageLoader.getInstance().getDiskCache());
                } else if (ResAssetUtil.getInstance().hasAssetFile(context, filePath)) {
                    ImageLoader.getInstance().displayImage("assets://"+filePath, imageView, getOption(originSize));
                    if (removeFromCache)
                        DiskCacheUtils.removeFromCache("assets://"+filePath, ImageLoader.getInstance().getDiskCache());
                } else if ( (resId = ResUtil.getInstance().getResId(context, filePath)) > 0){
                    displayImage(context, imageView, resId, originSize);
                    if (removeFromCache)
                        DiskCacheUtils.removeFromCache("drawable://"+resId, ImageLoader.getInstance().getDiskCache());
                }
            }
        }
    }
    @Deprecated
    void displayImage(@NonNull Context context, @NonNull ImageView imageView, @DrawableRes int resId, boolean originSize) {
        if (!ImageLoader.getInstance().isInited()) return;

        if (context == null) return;

        String name = ResUtil.getInstance().getEntryName(context, resId);
        if (name == null) return;

        // get file name without extension from resource id
        ArrayList<String> filePaths = getCandidateFilePaths(name);
        for(String filePath : filePaths) {
            if (ResFileUtil.getInstance().exist(ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath))) {
                ImageLoader.getInstance().displayImage("file://"+ResFileUtil.getInstance().getResourcePathWithLocale(context, filePath), imageView, getOption(originSize));
            } else if (ResAssetUtil.getInstance().hasAssetFile(context, filePath)) {
                ImageLoader.getInstance().displayImage("assets://"+filePath, imageView, getOption(originSize));
            }
        }
        ImageLoader.getInstance().displayImage("drawable://"+resId, imageView, getOption(originSize));
    }

    @Deprecated
    void clearCache() {
        if (!ImageLoader.getInstance().isInited()) return;

        try {
            ImageLoader.getInstance().clearDiskCache();
        } catch (Exception e) {

        }
    }
    //endregion
}
