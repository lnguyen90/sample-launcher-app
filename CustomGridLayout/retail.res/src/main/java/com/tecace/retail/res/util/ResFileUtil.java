package com.tecace.retail.res.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import com.tecace.retail.res.content.preload.AppPreloadContent;
import com.tecace.retail.res.content.preload.PreloadContent;
import com.tecace.retail.res.util.config.EnvironmentManager;
import com.tecace.retail.res.util.config.Environments;
import com.tecace.retail.util.PreferenceUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

/**
 * Created by icanmobile on 5/10/17.
 */

public class ResFileUtil {
    private static final String TAG = ResFileUtil.class.getSimpleName();

    private static volatile ResFileUtil sInstance;

    /**
     * Get ResFileUtil singleton object.
     *
     * @return ResFileUtil singleton object.
     */
    public static ResFileUtil getInstance() {
        if (sInstance == null) {
            synchronized (ResFileUtil.class) {
                if (sInstance == null)
                    sInstance = new ResFileUtil();
            }
        }
        return sInstance;
    }

    private ResFileUtil() {}


    private static HashSet<String> existFileSet = new HashSet<>();
//    private static HashSet<String> nonExistFileSet = new HashSet<>();
    public void resetFileSets() {
        if (existFileSet != null)
            existFileSet.clear();
//        if (nonExistFileSet != null)
//            nonExistFileSet.clear();
    }

    /**
     * Check if file with given path exists.
     *
     * @param path file path.
     * @return true if file exists. false if file does not exist.
     */
    public boolean exist(@NonNull String path) {
        if (path == null) return false;

        if (existFileSet.contains(path))
            return true;

        File file = new File(path);
        if (file.exists()) {
            existFileSet.add(path);
            return true;
        }
        return false;
    }


//    public String getExternalFileDir(@NonNull Context context) {
//        return context.getExternalFilesDir(null).toString();
//    }

    /**
     * Get file path for external file location.
     *
     * @param context Application or activity context.
     * @param fileName file name.
     * @return full string path of file by adding external file directory.
     */
    public String getPath(@NonNull Context context, @NonNull String fileName){
        if (context == null || fileName == null || fileName.length() == 0) return null;
        final String dir = context.getExternalFilesDir(null).toString();

        //[TA] Remove unnecessary condition for additional check routine.
//        if (fileName.contains(dir)) return fileName;

        return String.format("%s/%s", dir, fileName);
    }

    public String getExternalFilesDir(@NonNull Context context) {
        return context.getExternalFilesDir(null).toString();
    }

    /**
     * To support the RMS contents as below path
     * "/sdcard/Android/.data/<exp-app-pkg-name>/files"  (Note it is ".data".)
     * /storage/emulated/0/Android/.data/<exp-app-pkg-name>/files
     * @return directory path
     */
    public String getExternalStorageDir(@NonNull Context context) {
        return new StringBuilder()
                .append(Environment.getExternalStorageDirectory().getAbsolutePath())
                .append(File.separator)
                .append("Android")
                .append(File.separator)
                .append(".data")
                .append(File.separator)
                .append(context.getPackageName())
                .append(File.separator)
                .append("files")
                .toString();
    }

    /**
     * To support the preloaded contents as below path
     * /sdcard/.RetailMedia/.preload/<exp-app-pkg-name>/files
     * /storage/emulated/0/.RetailMedia/.preload/<exp-app-pkg-name>/files
     * @param context
     * @return directory path
     */
    public String getPreloadFilesDir(@NonNull Context context) {
        return new StringBuilder()
                .append(Environment.getExternalStorageDirectory())
                .append(File.separator)
                .append(".RetailMedia")
                .append(File.separator)
                .append(".preload")
                .append(File.separator)
                .append(context.getPackageName())
                .append(File.separator)
                .append("files")
                .toString();
    }

    private PreloadContent preloadContent = AppPreloadContent.getInstance();
    public void removedPreloadContentList(@NonNull Context context, String json) {
        preloadContent.removedList(context, json);
    }

    public void removedPreloadContentList(@NonNull Context context) {
        preloadContent.removedList(context);
    }

    public boolean isRemovedPreloadContent(String content) {
        return preloadContent.isRemoved(content);
    }

    /**
     * Get File with given file name.
     * If default application external directory does not have file, it checks asset folder as well.
     *
     * @param context Aplication or activity context.
     * @param fileName file name to find
     * @return file which is found in application external directory or asset folder.
     */
    public File getFile(@NonNull Context context, @NonNull String fileName) {
        if (context == null || fileName == null || fileName.length() == 0) return null;

        File file = null;
        file = new File(getPath(context, fileName));
        if(!file.exists()) {
            file = new File(String.format("file:///android_asset/%s", fileName));
        }
        return file;
    }

    //[TA] 11/10/2017 - Find in language with country -> language -> default folder
    //[MOVE_TO_RESMANAGER]
    /**
     * Get video file path for application external directory.
     *
     * @param context Application or activity context.
     * @param fileName file name for viceo.
     * @return String path for video file. null if file does not exist.
     */
   /* public String getVideoPath(@NonNull Context context, @NonNull String fileName) {
        if (context == null || fileName == null || fileName.length() == 0) return null;

        String path;
        path = ResFileUtil.getInstance().getPath(context, fileName);

        if(new File(path).exists()) {
            return path;
        }
        return null;
    }*/

    //[TA] 11/10/2017 - Find in language with country -> language -> default folder
    //[MOVE_TO_RESMANAGER]
    /**
     * Get Uri for given file name.
     * It checks Application external directory first and then checks asset folder.
     *
     * @param context Application or activity context.
     * @param fileName file name for Uri.
     * @return Uri for file. null in case of any error.
     */
    public Uri getFileUri (@NonNull Context context, @NonNull String fileName) {
        if (context == null || fileName == null || fileName.length() == 0) return null;

        Uri fileUri = null;
        File file = null;
        file = new File(getResourcePathWithLocale(context, fileName));

        if(file.exists()) {
            fileUri = Uri.fromFile(file);
        } else {
            fileUri = Uri.parse(String.format("file:///android_asset/%s", fileName));
        }
        return fileUri;
    }

    //[TA] 11/08/2017. Use Language - Country
    private final static String DEFAULT_LANGUAGE = "en";
    private final static int SEARCH_FOLDER_LANG_COUNTRY = 0;
    private final static int SEARCH_FOLDER_LANG = 1;
    private final static int SEARCH_FOLDER_ROOT = 2;
    /**
     * @param context
     * @param language
     * @param country
     * @param fileName
     */
    public ArrayList<String> getResourcePathsWithLocale (@NonNull Context context, @NonNull String language, @NonNull String country, @NonNull String fileName) {
        if (context == null || fileName == null || fileName.length() == 0) return null;

        if(language == null) language = DEFAULT_LANGUAGE;

        ArrayList<String> filePaths = new ArrayList<String>();

        String languageAndCountry = String.format("%s-r%s", language, country);

        String externalFilesDir = getExternalFilesDir(context);
        String preloadFilesDir = getPreloadFilesDir(context);
        String preloadContent = null;

        String smallestWidth = getSmallestWidthPath(context, fileName);
        String subsidiary = getSubsidiaryPath(context, smallestWidth);
        String subsidiaryDefault = getSubsidiaryPath(context, fileName);

        String storageFilesDir = getExternalStorageDir(context);

        Configuration configuration = context.getResources().getConfiguration();
        if (configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            String smallestWidthPort = smallestWidth.replace(smallestWidth.substring(smallestWidth.lastIndexOf("/")+1), "port/" + smallestWidth.substring(smallestWidth.lastIndexOf("/")+1));
            String subsidiaryPort = getSubsidiaryPath(context, smallestWidthPort);

            filePaths.add(String.format("%s/%s/%s", externalFilesDir, languageAndCountry, subsidiaryPort));
            filePaths.add(String.format("%s/%s/%s", storageFilesDir, languageAndCountry, subsidiaryPort));
            {
                preloadContent = String.format("%s/%s", languageAndCountry, subsidiaryPort);
                if (!isRemovedPreloadContent(preloadContent))
                    filePaths.add(String.format("%s/%s/%s", preloadFilesDir, languageAndCountry, subsidiaryPort));
            }

            filePaths.add(String.format("%s/%s/%s", externalFilesDir, language, subsidiaryPort));
            filePaths.add(String.format("%s/%s/%s", storageFilesDir, language, subsidiaryPort));
            {
                preloadContent = String.format("%s/%s", language, subsidiaryPort);
                if (!isRemovedPreloadContent(preloadContent))
                    filePaths.add(String.format("%s/%s/%s", preloadFilesDir, language, subsidiaryPort));
            }

            filePaths.add(String.format("%s/%s", externalFilesDir, subsidiaryPort));
            filePaths.add(String.format("%s/%s", storageFilesDir, subsidiaryPort));
            {
                preloadContent = String.format("%s", subsidiaryPort);
                if (!isRemovedPreloadContent(preloadContent))
                    filePaths.add(String.format("%s/%s", preloadFilesDir, subsidiaryPort));
            }

            filePaths.add(String.format("%s/%s/%s", externalFilesDir, languageAndCountry, smallestWidthPort));
            filePaths.add(String.format("%s/%s/%s", storageFilesDir, languageAndCountry, smallestWidthPort));
            {
                preloadContent = String.format("%s/%s", languageAndCountry, smallestWidthPort);
                if (!isRemovedPreloadContent(preloadContent))
                    filePaths.add(String.format("%s/%s/%s", preloadFilesDir, languageAndCountry, smallestWidthPort));
            }

            filePaths.add(String.format("%s/%s/%s", externalFilesDir, language, smallestWidthPort));
            filePaths.add(String.format("%s/%s/%s", storageFilesDir, language, smallestWidthPort));
            {
                preloadContent = String.format("%s/%s", language, smallestWidthPort);
                if (!isRemovedPreloadContent(preloadContent))
                    filePaths.add(String.format("%s/%s/%s", preloadFilesDir, language, smallestWidthPort));
            }

            filePaths.add(String.format("%s/%s", externalFilesDir, smallestWidthPort));
            filePaths.add(String.format("%s/%s", storageFilesDir, smallestWidthPort));
            {
                preloadContent = String.format("%s", smallestWidthPort);
                if (!isRemovedPreloadContent(preloadContent))
                    filePaths.add(String.format("%s/%s", preloadFilesDir, smallestWidthPort));
            }
        }
        else {
            String smallestWidthLand = smallestWidth.replace(smallestWidth.substring(smallestWidth.lastIndexOf("/")+1), "land/" + smallestWidth.substring(smallestWidth.lastIndexOf("/")+1));
            String subsidiaryLand = getSubsidiaryPath(context, smallestWidthLand);

            filePaths.add(String.format("%s/%s/%s", externalFilesDir, languageAndCountry, subsidiaryLand));
            filePaths.add(String.format("%s/%s/%s", storageFilesDir, languageAndCountry, subsidiaryLand));
            {
                preloadContent = String.format("%s/%s", languageAndCountry, subsidiaryLand);
                if (!isRemovedPreloadContent(preloadContent))
                    filePaths.add(String.format("%s/%s/%s", preloadFilesDir, languageAndCountry, subsidiaryLand));
            }

            filePaths.add(String.format("%s/%s/%s", externalFilesDir, language, subsidiaryLand));
            filePaths.add(String.format("%s/%s/%s", storageFilesDir, language, subsidiaryLand));
            {
                preloadContent = String.format("%s/%s", language, subsidiaryLand);
                if (!isRemovedPreloadContent(preloadContent))
                    filePaths.add(String.format("%s/%s/%s", preloadFilesDir, language, subsidiaryLand));
            }

            filePaths.add(String.format("%s/%s", externalFilesDir, subsidiaryLand));
            filePaths.add(String.format("%s/%s", storageFilesDir, subsidiaryLand));
            {
                preloadContent = String.format("%s", subsidiaryLand);
                if (!isRemovedPreloadContent(preloadContent))
                    filePaths.add(String.format("%s/%s", preloadFilesDir, subsidiaryLand));
            }

            filePaths.add(String.format("%s/%s/%s", externalFilesDir, languageAndCountry, smallestWidthLand));
            filePaths.add(String.format("%s/%s/%s", storageFilesDir, languageAndCountry, smallestWidthLand));
            {
                preloadContent = String.format("%s/%s", languageAndCountry, smallestWidthLand);
                if (!isRemovedPreloadContent(preloadContent))
                    filePaths.add(String.format("%s/%s/%s", preloadFilesDir, languageAndCountry, smallestWidthLand));
            }

            filePaths.add(String.format("%s/%s/%s", externalFilesDir, language, smallestWidthLand));
            filePaths.add(String.format("%s/%s/%s", storageFilesDir, language, smallestWidthLand));
            {
                preloadContent = String.format("%s/%s", language, smallestWidthLand);
                if (!isRemovedPreloadContent(preloadContent))
                    filePaths.add(String.format("%s/%s/%s", preloadFilesDir, language, smallestWidthLand));
            }

            filePaths.add(String.format("%s/%s", externalFilesDir, smallestWidthLand));
            filePaths.add(String.format("%s/%s", storageFilesDir, smallestWidthLand));
            {
                preloadContent = String.format("%s", smallestWidthLand);
                if (!isRemovedPreloadContent(preloadContent))
                    filePaths.add(String.format("%s/%s", preloadFilesDir, smallestWidthLand));
            }
        }

        filePaths.add(String.format("%s/%s/%s", externalFilesDir, languageAndCountry, subsidiary));
        filePaths.add(String.format("%s/%s/%s", storageFilesDir, languageAndCountry, subsidiary));
        {
            preloadContent = String.format("%s/%s", languageAndCountry, subsidiary);
            if (!isRemovedPreloadContent(preloadContent))
                filePaths.add(String.format("%s/%s/%s", preloadFilesDir, languageAndCountry, subsidiary));
        }

        filePaths.add(String.format("%s/%s/%s", externalFilesDir, language, subsidiary));
        filePaths.add(String.format("%s/%s/%s", storageFilesDir, language, subsidiary));
        {
            preloadContent = String.format("%s/%s", language, subsidiary);
            if (!isRemovedPreloadContent(preloadContent))
                filePaths.add(String.format("%s/%s/%s", preloadFilesDir, language, subsidiary));
        }

        filePaths.add(String.format("%s/%s", externalFilesDir, subsidiary));
        filePaths.add(String.format("%s/%s", storageFilesDir, subsidiary));
        {
            preloadContent = String.format("%s", subsidiary);
            if (!isRemovedPreloadContent(preloadContent))
                filePaths.add(String.format("%s/%s", preloadFilesDir, subsidiary));
        }

        filePaths.add(String.format("%s/%s/%s", externalFilesDir, languageAndCountry, smallestWidth));
        filePaths.add(String.format("%s/%s/%s", storageFilesDir, languageAndCountry, smallestWidth));
        {
            preloadContent = String.format("%s/%s", languageAndCountry, smallestWidth);
            if (!isRemovedPreloadContent(preloadContent))
                filePaths.add(String.format("%s/%s/%s", preloadFilesDir, languageAndCountry, smallestWidth));
        }

        filePaths.add(String.format("%s/%s/%s", externalFilesDir, language, smallestWidth));
        filePaths.add(String.format("%s/%s/%s", storageFilesDir, language, smallestWidth));
        {
            preloadContent = String.format("%s/%s", language, smallestWidth);
            if (!isRemovedPreloadContent(preloadContent))
                filePaths.add(String.format("%s/%s/%s", preloadFilesDir, language, smallestWidth));
        }

        filePaths.add(String.format("%s/%s", externalFilesDir, smallestWidth));
        filePaths.add(String.format("%s/%s", storageFilesDir, smallestWidth));
        {
            preloadContent = String.format("%s", smallestWidth);
            if (!isRemovedPreloadContent(preloadContent))
                filePaths.add(String.format("%s/%s", preloadFilesDir, smallestWidth));
        }

        filePaths.add(String.format("%s/%s/%s", externalFilesDir, languageAndCountry, subsidiaryDefault));
        filePaths.add(String.format("%s/%s/%s", storageFilesDir, languageAndCountry, subsidiaryDefault));
        {
            preloadContent = String.format("%s/%s", languageAndCountry, subsidiaryDefault);
            if (!isRemovedPreloadContent(preloadContent))
                filePaths.add(String.format("%s/%s/%s", preloadFilesDir, languageAndCountry, subsidiaryDefault));
        }

        filePaths.add(String.format("%s/%s/%s", externalFilesDir, language, subsidiaryDefault));
        filePaths.add(String.format("%s/%s/%s", storageFilesDir, language, subsidiaryDefault));
        {
            preloadContent = String.format("%s/%s", language, subsidiaryDefault);
            if (!isRemovedPreloadContent(preloadContent))
                filePaths.add(String.format("%s/%s/%s", preloadFilesDir, language, subsidiaryDefault));
        }

        filePaths.add(String.format("%s/%s", externalFilesDir, subsidiaryDefault));
        filePaths.add(String.format("%s/%s", storageFilesDir, subsidiaryDefault));
        {
            preloadContent = String.format("%s", subsidiaryDefault);
            if (!isRemovedPreloadContent(preloadContent))
                filePaths.add(String.format("%s/%s", preloadFilesDir, subsidiaryDefault));
        }

        filePaths.add(String.format("%s/%s/%s", externalFilesDir, languageAndCountry, fileName));
        filePaths.add(String.format("%s/%s/%s", storageFilesDir, languageAndCountry, fileName));
        {
            preloadContent = String.format("%s/%s", languageAndCountry, fileName);
            if (!isRemovedPreloadContent(preloadContent))
                filePaths.add(String.format("%s/%s/%s", preloadFilesDir, languageAndCountry, fileName));
        }

        filePaths.add(String.format("%s/%s/%s", externalFilesDir, language, fileName));
        filePaths.add(String.format("%s/%s/%s", storageFilesDir, language, fileName));
        {
            preloadContent = String.format("%s/%s", language, fileName);
            if (!isRemovedPreloadContent(preloadContent))
                filePaths.add(String.format("%s/%s/%s", preloadFilesDir, language, fileName));
        }

        filePaths.add(String.format("%s/%s", externalFilesDir, fileName));
        filePaths.add(String.format("%s/%s", storageFilesDir, fileName));
        {
            preloadContent = String.format("%s", fileName);
            if (!isRemovedPreloadContent(preloadContent))
                filePaths.add(String.format("%s/%s", preloadFilesDir, fileName));
        }

        return filePaths;
    }

    /**
     * @param context
     * @param fileName
     */
    public ArrayList<String> getResourcePathsWithLocale (@NonNull Context context, @NonNull String fileName) {
        if (context == null || fileName == null || fileName.length() == 0) return null;

        String language = Locale.getDefault().getLanguage();
        String country = Locale.getDefault().getCountry();

        return getResourcePathsWithLocale(context, language, country, fileName);
    }

//    /**
//     * Get file path for external file location.
//     * Find in language_country -> language -> default
//     *
//     * @param context Application or activity context.
//     * @param fileName file name.
//     * @return full string path of file by adding external file directory.
//     */
//    public String getResourcePathWithLocale(@NonNull Context context, @NonNull String fileName){
//        if (context == null || fileName == null || fileName.length() == 0) return null;
//
//        ArrayList<String> filePaths = this.getResourcePathsWithLocale(context, fileName);
//        for (int i = 0; i < filePaths.size(); i++) {
//            if (this.exist(filePaths.get(i))) {
//                return filePaths.get(i);
//            }
//        }
//        return filePaths.get(filePaths.size()-1); //If File does not exist, return default folder path.
//    }

    /**
     * Get file path for external file location.
     * Find in language_country -> language -> default
     *
     * @param context Application or activity context.
     * @param fileName file name.
     * @return full string path of file by adding external file directory.
     */
    public String getResourcePathWithLocale(@NonNull Context context, @NonNull String fileName){
        if (context == null || fileName == null || fileName.length() == 0) return null;
        ArrayList<String> filePaths = this.getResourcePathsWithLocale(context, fileName);
        for (int i = 0; i < filePaths.size(); i++) {
            if (this.exist(filePaths.get(i))) {
                if(isMajorLanguage(context) && exist(getMajorVideoResourcesFilePath(context,filePaths.get(i)))) {
                    return getMajorVideoResourcesFilePath(context,filePaths.get(i));
                } else {
                    // ignored languages shouldn't have audio
//                    if(isLanguageIgnored(context) && filePaths.get(i).contains("audio")) {
//                        return "";
//                    }
                    if(isLanguageIgnored(context) && isVideoResourceFolder(context, filePaths.get(i))
                            || filePaths.get(i).contains("audio")) {
                        return filePathWithoutLocale(context, filePaths.get(i));
                    }
                    return filePaths.get(i);
                }
            }
        }
        return filePaths.get(filePaths.size()-1); //If File does not exist, return default folder path (.preload).
    }

    public String getExternalResourcePath(@NonNull Context context) {
        String language = Locale.getDefault().getLanguage();
        String country = Locale.getDefault().getCountry();
        String languageAndCountry = String.format("%s-r%s", language, country);

        return new StringBuilder().append(getExternalFilesDir(context)).append("/").append(languageAndCountry).toString();
    }

    private String getSmallestWidthPath(@NonNull Context context, @NonNull String fileName) {
        Configuration configuration = context.getResources().getConfiguration();
        return fileName.replace(fileName.substring(fileName.lastIndexOf("/")+1), "sw" + configuration.smallestScreenWidthDp + "dp" + "/" + fileName.substring(fileName.lastIndexOf("/")+1));
    }

    private String getSubsidiaryPath(@NonNull Context context, @NonNull String fileName) {
        String prefSubsidiary = PreferenceUtil.getInstance().getString(context, "PREFERENCE_SUBSIDIARY");
        return fileName.replace(fileName.substring(fileName.lastIndexOf("/")+1), prefSubsidiary + "/" + fileName.substring(fileName.lastIndexOf("/")+1));
    }

    /**
     * Check if the current locale is one of the major language that we support.
     *
     * @param context The context.
     * @return If the current locale is one of the major languages or not.
     */
    public boolean isMajorLanguage(Context context) {
        List<Object> majorLanguagesList = EnvironmentManager.getInstance()
                .getListValue(context, Environments.MAJOR_LANGUAGES, new ArrayList<Object>());

        if(majorLanguagesList == null || majorLanguagesList.isEmpty()) return false;

        String language = Locale.getDefault().getLanguage();
        String country = Locale.getDefault().getCountry();
        String languageAndCountry = String.format("%s-r%s", language, country);

        if(isLanguageIgnored(context)) return false;

        for(int i = 0; i < majorLanguagesList.size(); i++) {
            String locale = (String) majorLanguagesList.get(i);
            if(locale.equals(languageAndCountry) || locale.equals(language)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if the current locale is being ignored or not (for major languages).
     * We have the case when we need to ignore es-rES but es is one of the major language, we only
     * want to ignore es-rES and not other es lanuages.
     * @param context The context.
     * @return If the current locale is being ignored or not.
     */
    public boolean isLanguageIgnored(Context context) {
        List<Object> ignoreLanguagesList = EnvironmentManager.getInstance()
                .getListValue(context, Environments.IGNORE_MAJOR_LANGUAGES, new ArrayList<Object>());


        if(ignoreLanguagesList == null || ignoreLanguagesList.isEmpty()) return false;

        String language = Locale.getDefault().getLanguage();
        String country = Locale.getDefault().getCountry();
        String languageAndCountry = String.format("%s-r%s", language, country);

        return ignoreLanguagesList.contains(languageAndCountry) || ignoreLanguagesList.contains(language);
    }

    /**
     * Get the video resource file path. These resources (frame, video, chapter) are always
     * in "major" folder.
     *
     * @param context The context.
     * @param filePath The orginal file path.
     * @return The major file path.
     */
    public String getMajorVideoResourcesFilePath(Context context, String filePath) {

        if(isVideoResourceFolder(context, filePath)) {
            int lastIndexOfForwardSlash = filePath.lastIndexOf('/');
            StringBuilder majorFilePath = new StringBuilder(filePath);
            majorFilePath.insert(lastIndexOfForwardSlash+1, "major/");
            return majorFilePath.toString();
        }

        return filePath;
    }

    /**
     * Check if the current file path contains one of the video resource folder (video, chapter, frame)
     *
     * @param context The context.
     * @param filePath The original file path.
     * @return If the current file path contains one of the video resource folder.
     */
    public boolean isVideoResourceFolder(Context context, String filePath) {
        List<Object> majorVideoResourceFolder = EnvironmentManager.getInstance()
                .getListValue(context, Environments.MAJOR_VIDEO_RESOURCE_FOLDER, new ArrayList<Object>());

        if(majorVideoResourceFolder == null || majorVideoResourceFolder.isEmpty()) return false;

        for(Object folder: majorVideoResourceFolder) {
            String folderName = (String) folder;
            if(filePath.contains(folderName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Replace all locale substring from the current file path.
     *
     * @param context The context.
     * @param filePath The original file path.
     * @return The modified file path without locale substring.
     */
    public String filePathWithoutLocale(Context context, String filePath) {
        String language = Locale.getDefault().getLanguage();
        String country = Locale.getDefault().getCountry();
        String languageAndCountry = String.format("%s-r%s", language, country);

        if (filePath.contains(languageAndCountry)) {
            filePath = filePath.replace("/" + languageAndCountry + "/", "");
            ;
            filePath = filePath.replace("/files", "/files/");
            return filePath;
        }
        if (filePath.contains(language)) {
            filePath = filePath.replace("/" + language + "/", "");
            ;
            filePath = filePath.replace("/files", "/files/");
            return filePath;
        }

        return filePath;
    }
}
