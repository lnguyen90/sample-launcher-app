package com.tecace.retail.res;

import android.content.Context;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;

import com.tecace.retail.res.util.ResConst;
import com.tecace.retail.res.util.ResFileUtil;
import com.tecace.retail.res.util.ResUtil;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by lnguye68 on 12/6/17.
 * Updated by smheo on 12/14/2017 : Added localization functionality
 */

class ResDimen {

    private static final String TAG = ResDimen.class.getSimpleName();

    private static volatile ResDimen sInstance = null;
    static ResDimen getInstance() {
        if (sInstance == null) {
            synchronized (ResDimen.class) {
                if (sInstance == null)
                    sInstance = new ResDimen();
            }
        }
        return sInstance;
    }
    private ResDimen(){}



    private String dimenXmlFile = null;
    private HashMap<String, String> dimens = new HashMap<>();



    /**
     * getPixelDimen
     * @param context
     * @param data
     * @return if dimen resource is not found, return null as a default value.
     */
    Integer getPixelDimen(@NonNull Context context, @NonNull String data) {
        if (context == null || data == null || data.isEmpty()) return null;

        int resId = ResUtil.getInstance().getResId(context, data);
        if (resId <= 0) {
            Float pixel = convertStringNumberToFloatTypePixel(context, data);
            if (pixel == null) return null;
            return (int) pixel.floatValue();
        }

        String name = ResUtil.getInstance().getEntryName(context, resId);
        if (name == null) return null;  //not found resource id so return data

        if (BuildConfig.DEBUG) {
            Integer dimen = getDimenIntegerFromXml(context, name);
            return (dimen != null) ? dimen : context.getResources().getDimensionPixelSize(resId);
        } else {
            return context.getResources().getDimensionPixelSize(resId);
        }
    }

    /**
     * getPixelDimen
     * @param context
     * @param resId
     * @return if dimen resource is not found, return null as a default value.
     */
    Integer getPixelDimen(@NonNull Context context, @DimenRes int resId) {
        if (context == null || resId <= 0) return null;

        String name = ResUtil.getInstance().getEntryName(context, resId);
        if (name == null) return null;

        if (BuildConfig.DEBUG) {
            Integer dimen = getDimenIntegerFromXml(context, name);
            return (dimen != null) ? dimen : context.getResources().getDimensionPixelSize(resId);
        } else {
            return context.getResources().getDimensionPixelSize(resId);
        }
    }

    /**
     * getDimen
     * @param context
     * @param data
     * @return if dimen resource is not found, return null as a default value.
     */
    Float getDimen(@NonNull Context context, @NonNull String data) {
        if (context == null || data == null || data.length() == 0) return null;

        int resId = ResUtil.getInstance().getResId(context, data);
        if (resId <= 0) {
            return convertStringNumberToFloatTypePixel(context, data);
        }

        String name = ResUtil.getInstance().getEntryName(context, resId);
        if (name == null) return null;  //not found resource id so return data

        if (BuildConfig.DEBUG) {
            Float dimen = getDimenFloatFromXml(context, name);
            return (dimen != null) ? dimen : context.getResources().getDimension(resId);
        } else {
            return context.getResources().getDimension(resId);
        }
    }

    /**
     * getDimen
     * @param context
     * @param resId
     * @return if dimen resource is not found, return null as a default value.
     */
    Float getDimen(@NonNull Context context, @DimenRes int resId) {
        if (context == null || resId <= 0) return null;

        String name = ResUtil.getInstance().getEntryName(context, resId);
        if (name == null) return null;

        if (BuildConfig.DEBUG) {
            Float dimen = getDimenFloatFromXml(context, name);
            return (dimen != null) ? dimen : context.getResources().getDimension(resId);
        } else {
            return context.getResources().getDimension(resId);
        }
    }


    /**
     * convertStringNumberToFloatTypePixel
     *      - convert from the dimension number fo the string type to pixel value of float type
     *      - For example : "90px" -> 90.0 pixel, "-50" -> -50 pixel, "25.5dp" -> **.* pixel etc
     *
     * [Input]
     * context
     * inputNumber dimension number of the string type
     *
     * [Output]
     * pixel value of the Float type. If return value is null, this dimension number cannot support type
     */
    private final String NUMBER_REGEX = "^([+-]?[0-9]*\\.?[0-9]+)|(([+-]?[0-9]*\\.?[0-9]+)(px||dp|dip|sp|pt|mm|in)+)$";
    private final String[] UNIT_TYPE_ARRAY = {"px", "dp", "dip", "sp", "pt", "mm", "in"};

    private Float convertStringNumberToFloatTypePixel(Context context, String inputNumber) {
        if (context == null || inputNumber == null || inputNumber.isEmpty()) return null;

        if (inputNumber.matches(NUMBER_REGEX)) {

            boolean IsFindUnitType = false;
            String strNumber = null;
            String strUnitType = null;

            for (String unitType : UNIT_TYPE_ARRAY) {
                int subStrIndex = inputNumber.indexOf(unitType);
                if (subStrIndex > -1) {
                    strNumber = inputNumber.substring(0, subStrIndex);
                    strUnitType = inputNumber.substring(subStrIndex);
                    IsFindUnitType = true;
                    break;
                }
            }

            // If IsFindUnitType is false, The input string has only numbers
            // This means px type.
            if (!IsFindUnitType) {
                strNumber = inputNumber;
                strUnitType = "px";
            }

            // Convert from string to float
            float floatNumber;
            try {
                floatNumber = Float.parseFloat(strNumber);
            } catch (NumberFormatException ex) {
                Log.e(TAG, "This number is not float type!!");
                return null;
            }

            // Define unit type
            int unit_type;
            switch (strUnitType) {
                case "px":
                    unit_type = TypedValue.COMPLEX_UNIT_PX;
                    break;
                case "dp":
                case "dip":
                    unit_type = TypedValue.COMPLEX_UNIT_DIP;
                    break;
                case "sp":
                    unit_type = TypedValue.COMPLEX_UNIT_SP;
                    break;
                case "pt":
                    unit_type = TypedValue.COMPLEX_UNIT_PT;
                    break;
                case "mm":
                    unit_type = TypedValue.COMPLEX_UNIT_MM;
                    break;
                case "in":
                    unit_type = TypedValue.COMPLEX_UNIT_IN;
                    break;
                default:
                    return null;
            }

            if (context.getResources() == null) return null;
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            if (displayMetrics == null) return null;

            // Converts in pixel unit and return the pixel value
            return TypedValue.applyDimension(unit_type, floatNumber, displayMetrics);
        } else {
            return null;
        }
    }

    //--------------------- For update dimens.xml to the model of CMS folder -----------------------
    /**
     * setDimenXmlFile
     * @param context
     * @param file
     */
    void setDimenXmlFile(@NonNull Context context, @NonNull String file) {
        if (context == null || file == null || file.length() == 0) return;
        //The language file already set, just return.
        if (this.dimenXmlFile != null && this.dimenXmlFile.equals(file)) return;
        try {
            this.dimens.clear();
            this.dimens = loadDimensionFromXML(context, file);
            this.dimenXmlFile = file;
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //[TA] 11/08/2017. Use Language - Country
    /**
     * setDimenXmlFile
     * @param context
     * @param language
     * @param country
     */
    void setDimenXmlFile(@NonNull Context context, @NonNull String language, @NonNull String country) {
        if (context == null) return;
        try {
            this.dimens.clear();
            this.dimens = loadDimensionFromXML(
                    context,
                    ResFileUtil.getInstance().getResourcePathsWithLocale(context, language, country, ResConst.FOLDER_MODEL_DIMEN)
            );
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * resetDimenXmlFile
     * @param context
     */
    void resetDimenXmlFile(@NonNull Context context) {
        resetDimenXmlFile(context, this.dimenXmlFile);
    }

    /**
     * resetDimenXmlFile
     * @param context
     * @param file
     */
    void resetDimenXmlFile(@NonNull Context context, @NonNull String file) {
        if (context == null || file == null || file.length() == 0) return;
        try {
            this.dimens.clear();
            this.dimens = loadDimensionFromXML(context, file);
            this.dimenXmlFile = file;
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * loadDimensionFromXML
     * @param context
     * @param file
     * @return
     * @throws XmlPullParserException
     * @throws IOException
     */
    private HashMap<String, String> loadDimensionFromXML(@NonNull Context context, @NonNull String file)
            throws XmlPullParserException, IOException {
        if (context == null || file == null || file.length() == 0) return null;

        HashMap<String, String> dimens = new HashMap<>();
        String key = null;
        String value = null;

        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();
        xpp.setInput(new FileInputStream(ResFileUtil.getInstance().getPath(context, file)), "utf-8");
        int eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if(eventType == XmlPullParser.START_DOCUMENT) {
            } else if(eventType == XmlPullParser.END_DOCUMENT) {
            } else if(eventType == XmlPullParser.START_TAG) {
                key = xpp.getAttributeValue(null, "name");
            } else if(eventType == XmlPullParser.TEXT) {
                value = xpp.getText();
            } else if(eventType == XmlPullParser.END_TAG) {
                if (key != null && value != null)
                    dimens.put(key, value);
                key = null;
                value = null;
            }

            eventType = xpp.next();
        }
        return dimens;
    }

    /**
     * loadDimensionFromXML
     * @param context
     * @param filePaths
     * @return
     * @throws XmlPullParserException
     * @throws IOException
     */
    private HashMap<String, String> loadDimensionFromXML(@NonNull Context context, @NonNull ArrayList<String> filePaths)
            throws XmlPullParserException, IOException {
        if (context == null || filePaths == null || filePaths.isEmpty()) return null;

        HashMap<String, String> dimens = new HashMap<>();
        String key = null;
        String value = null;

        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();
        for(int i = filePaths.size() - 1; i > -1; i--) {
            String filePath = filePaths.get(i);
//            Log.d(TAG, "******* file path : " + filePath.toString() + ", index : " + i + ", out of : " + filePaths.size());
            if(filePath != null && ResFileUtil.getInstance().exist(filePath)) {
//                Log.d(TAG, "******* file path : " + filePath.toString());
                xpp.setInput(new FileInputStream(filePath.toString()), "utf-8");
                int eventType = xpp.getEventType();
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if(eventType == XmlPullParser.START_DOCUMENT) {
                    } else if(eventType == XmlPullParser.END_DOCUMENT) {
                    } else if(eventType == XmlPullParser.START_TAG) {
                        key = xpp.getAttributeValue(null, "name");
                    } else if(eventType == XmlPullParser.TEXT) {
                        value = xpp.getText();
                    } else if(eventType == XmlPullParser.END_TAG) {
                        if (key != null && value != null)
                            dimens.put(key, value);
                        key = null;
                        value = null;
                    }
                    eventType = xpp.next();
                }
            }
        }
        return dimens;
    }

    /**
     * getDimenInteger
     * @param key
     * @return
     */
    private Integer getDimenIntegerFromXml(@NonNull Context context, @NonNull String key) {
        if (context == null || this.dimens == null || this.dimens.size() == 0 || key == null || key.length() == 0) return null;
        Float dimen = convertStringNumberToFloatTypePixel(context, this.dimens.get(key));
        if (dimen != null)
            return (int) dimen.floatValue();
        else
            return null;
    }

    /**
     * getDimenFloat
     * @param key
     * @return
     */
    private Float getDimenFloatFromXml(@NonNull Context context, @NonNull String key) {
        if (context == null || this.dimens == null || this.dimens.size() == 0 || key == null || key.length() == 0) return null;
        return convertStringNumberToFloatTypePixel(context, this.dimens.get(key));
    }

}
