package com.tecace.retail.res.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.tecace.retail.res.Res;
import com.tecace.retail.res.ResObserver;
import com.tecace.retail.res.util.ResFileUtil;
import com.tecace.retail.res.util.config.EnvironmentManager;

import java.util.Locale;

/**
 * Created by icanmobile on 1/11/18.
 */

public class LanguageSettingReceiver extends BroadcastReceiver {

    private static final String TAG = LanguageSettingReceiver.class.getSimpleName();

    public static final String ACTION_LOCALE_CHANGED = "android.intent.action.LOCALE_CHANGED";

    public interface LanguageSettingObserver {
        void changedLanguage();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(Intent.ACTION_LOCALE_CHANGED)) {

            Log.e(TAG, "#### ACTION_LOCALE_CHANGED");

            EnvironmentManager.getInstance().updateAppEnvironment(context);

            Res.setLanguage(context, Locale.getDefault().getLanguage(), Locale.getDefault().getCountry());

            ResObserver.getInstance().notifyChangedLanguageSetting(ResFileUtil.getInstance().getExternalFilesDir(context));
        }
    }
}
