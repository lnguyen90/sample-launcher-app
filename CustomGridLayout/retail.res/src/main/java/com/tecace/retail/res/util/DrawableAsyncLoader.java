package com.tecace.retail.res.util;

import android.graphics.drawable.Drawable;

/**
 * Created by JW on 4/19/18.
 */
public interface DrawableAsyncLoader {
    void onDrawableReady(Drawable drawable);
}
