package com.tecace.retail.res.util.config.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.tecace.retail.res.util.config.Environments.VALUE_DEFAULT_BRIGHTNESS;
import static com.tecace.retail.res.util.config.Environments.VALUE_DEFAULT_HEADSET_VOLUME;
import static com.tecace.retail.res.util.config.Environments.VALUE_DEFAULT_SPEAKER_VOLUME;

/**
 * Created by smheo on 5/8/2017.
 */

public class EnvironmentModel implements Serializable {
    // this is the productFlavors value in gradle file
    @SerializedName("flavor")
    public String flavor;

    @SerializedName("supportLanguages")
    private List<String> supportLanguages = new ArrayList<String>();

    @SerializedName("majorLanguages")
    private List<String> majorLanguages = new ArrayList<String>();

    @SerializedName("ignoreMajorLanguages")
    private List<String> ignoreMajorLanguages = new ArrayList<String>();

    @SerializedName("majorVideoResourceFolders")
    private List<String> majorVideoResourceFolders = new ArrayList<String>();

    // supported device carrier names (verizon and att, tmo etc.)
    @SerializedName("supportCarriers")
    private List<String> supportCarriers = new ArrayList<String>();

    @SerializedName("enableBrightnessControl")
    private boolean enableBrightnessControl;

    @SerializedName("applyDefaultBrightness")
    private boolean applyDefaultBrightness;

    // percentage of default brightness ratio (0 ~ 100)
    // Defined Environments.VALUE_DEFAULT_BRIGHTNESS
    @SerializedName("defaultBrightnessRatio")
    private int defaultBrightnessRatio;

    @SerializedName("enableVolumeControl")
    private boolean enableVolumeControl;

    @SerializedName("applyDefaultVolume")
    private boolean applyDefaultVolume;

    // percentage of default speaker volume ratio (0 ~ 100)
    // Defined Environments.VALUE_DEFAULT_SPEAKER_VOLUME
    @SerializedName("defaultSpeakerVolumeRatio")
    private int defaultSpeakerVolumeRatio;

    // percentage of default headset volume ratio (0 ~ 100)
    // Defined Environments.VALUE_DEFAULT_HEADSET_VOLUME
    @SerializedName("defaultHeadsetVolumeRatio")
    private int defaultHeadsetVolumeRatio;

    // enable or disable ScreenOffReceiver
    @SerializedName("enableScreenOffDetection")
    private boolean enableScreenOffDetection;

    // enable or disable RebootReceiver
    @SerializedName("enableRebootDetection")
    private boolean enableRebootDetection;

    // enable or disable the face detection function in the retail application.
    // however, The actual face detection function is provided by the host application and
    // if this function is enabled, the temperature of the device is raised.
    @SerializedName("enableFaceDetection")
    private boolean enableFaceDetection;

    // enable or disable communication function with the wearable device (like to the gear s2 and gear s3)
    @SerializedName("enableWearableFunction")
    private boolean enableWearableFunction;

    // show or hide the subtitle text when user provide srt file to the mediaPlayer
    @SerializedName("enableSubTitleOnVideo")
    private boolean enableSubTitleOnVideo;

    // enable or disable foreground detect service
    @SerializedName("enableForegroundDetect")
    private boolean enableForegroundDetect;

    /**
     * FOR TEST & DEVELOPMENT MODE
     */
    // Development mode associated with video playback
    @SerializedName("showTestVideoContentInfo")
    private boolean showTestVideoContentInfo;

    @SerializedName("showTestVideoTimer")
    private boolean showTestVideoTimer;

    @SerializedName("showTestVideoControlButtons")
    private boolean showTestVideoControlButtons;



    public EnvironmentModel() {
        this(null, null, null, null,null,null,
                true, true, VALUE_DEFAULT_BRIGHTNESS,
                true, false, VALUE_DEFAULT_SPEAKER_VOLUME, VALUE_DEFAULT_HEADSET_VOLUME,
                false,
                false,
                false,
                false,
                false,
                false,
                false, false, false);
    }

    public EnvironmentModel(String flavor,
                            List<String> supportLanguages,
                            List<String> majorLanguages,
                            List<String> ignoreMajorLanguages,
                            List<String> majorVideoResourceFolders,
                            List<String> supportCarriers,
                            boolean enableBrightnessControl,
                            boolean applyDefaultBrightness,
                            int defaultBrightnessRatio,
                            boolean enableVolumeControl,
                            boolean applyDefaultVolume,
                            int defaultSpeakerVolumeRatio,
                            int defaultHeadsetVolumeRatio,
                            boolean enableScreenOffDetection,
                            boolean enableRebootDetection,
                            boolean enableFaceDetection,
                            boolean enableWearableFunction,
                            boolean enableSubTitleOnVideo,
                            boolean enableForegroundDetect,
                            boolean showTestVideoContentInfo,
                            boolean showTestVideoTimer,
                            boolean showTestVideoControlButtons
    ) {
        this.flavor = flavor;
        this.supportCarriers = supportCarriers;
        this.supportLanguages = supportLanguages;
        this.majorLanguages = majorLanguages;
        this.ignoreMajorLanguages = ignoreMajorLanguages;
        this.majorVideoResourceFolders = majorVideoResourceFolders;

        this.enableBrightnessControl = enableBrightnessControl;
        this.applyDefaultBrightness = applyDefaultBrightness;
        this.defaultBrightnessRatio = defaultBrightnessRatio;

        this.enableVolumeControl = enableVolumeControl;
        this.applyDefaultVolume = applyDefaultVolume;
        this.defaultSpeakerVolumeRatio = defaultSpeakerVolumeRatio;
        this.defaultHeadsetVolumeRatio = defaultHeadsetVolumeRatio;

        this.enableScreenOffDetection = enableScreenOffDetection;
        this.enableRebootDetection = enableRebootDetection;
        this.enableFaceDetection = enableFaceDetection;
        this.enableWearableFunction = enableWearableFunction;
        this.enableSubTitleOnVideo = enableSubTitleOnVideo;
        this.enableForegroundDetect = enableForegroundDetect;
        this.showTestVideoContentInfo = showTestVideoContentInfo;
        this.showTestVideoTimer = showTestVideoTimer;
        this.showTestVideoControlButtons = showTestVideoControlButtons;
    }

    public String getFlavor() {
        return this.flavor;
    }
    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }

    public List<String> getSupportLanguages() {
        return this.supportLanguages;
    }
    public void setSupportLanguages(List<String> supportLanguages) {
        this.supportLanguages = supportLanguages;
    }

    public List<String> getMajorLanguages() {
        return this.majorLanguages;
    }
    public void setMajorLanguages(List<String> majorLanguages) {
        this.majorLanguages = majorLanguages;
    }

    public List<String> getIgnoreMajorLanguages() {
        return this.ignoreMajorLanguages;
    }
    public void setIgnoreMajorLanguages(List<String> ignoreMajorLanguages) {
        this.ignoreMajorLanguages = ignoreMajorLanguages;
    }

    public List<String> majorVideoResourceFolders() {
        return this.majorVideoResourceFolders;
    }
    public void majorVideoResourceFolders(List<String> majorVideoResourceFolders) {
        this.majorVideoResourceFolders = majorVideoResourceFolders;
    }

    public List<String> getSupportCarriers() {
        return this.supportCarriers;
    }
    public void setSupportCarriers(List<String> supportCarriers) {
        this.supportCarriers = supportCarriers;
    }

    public boolean isEnableBrightnessControl() {
        return enableBrightnessControl;
    }
    public void setEnableBrightnessControl(boolean enableBrightnessControl) {
        this.enableBrightnessControl = enableBrightnessControl;
    }

    public boolean isApplyDefaultBrightness() {
        return this.applyDefaultBrightness;
    }
    public void setApplyDefaultBrightness(boolean applyDefaultBrightness) {
        this.applyDefaultBrightness = applyDefaultBrightness;
    }

    public int getDefaultBrightnessRatio() {
        return this.defaultBrightnessRatio;
    }
    public void setDefaultBrightnessRatio(int defaultBrightnessRatio) {
        this.defaultBrightnessRatio = defaultBrightnessRatio;
    }

    public boolean isEnableVolumeControl() {
        return enableVolumeControl;
    }
    public void setEnableVolumeControl(boolean enableVolumeControl) {
        this.enableVolumeControl = enableVolumeControl;
    }

    public boolean isApplyDefaultVolume() {
        return applyDefaultVolume;
    }
    public void setApplyDefaultVolume(boolean applyDefaultVolume) {
        this.applyDefaultVolume = applyDefaultVolume;
    }

    public int getDefaultSpeakerVolumeRatio() {
        return defaultSpeakerVolumeRatio;
    }
    public void setDefaultSpeakerVolumeRatio(int defaultSpeakerVolumeRatio) {
        this.defaultSpeakerVolumeRatio = defaultSpeakerVolumeRatio;
    }

    public int getDefaultHeadsetVolumeRatio() {
        return defaultHeadsetVolumeRatio;
    }
    public void setDefaultHeadsetVolumeRatio(int defaultHeadsetVolumeRatio) {
        this.defaultHeadsetVolumeRatio = defaultHeadsetVolumeRatio;
    }

    public boolean isEnableScreenOff() {
        return enableScreenOffDetection;
    }
    public void setEnableScreenOff(boolean enableScreenOffDetection) {
        this.enableScreenOffDetection = enableScreenOffDetection;
    }

    public boolean isEnableReboot() {
        return enableRebootDetection;
    }
    public void setEnableReboot(boolean enableRebootDetection) {
        this.enableRebootDetection = enableRebootDetection;
    }

    public boolean isEnableFaceDetection() {
        return enableFaceDetection;
    }
    public void setEnableFaceDetection(boolean enableFaceDetection) {
        this.enableFaceDetection = enableFaceDetection;
    }

    public boolean isEnableWearableFunction() {
        return enableWearableFunction;
    }
    public void setEnableWearableFunction(boolean enableWearableFunction) {
        this.enableWearableFunction = enableWearableFunction;
    }

    public boolean isEnableSubTitleOnVideo() {
        return enableSubTitleOnVideo;
    }
    public void setEnableSubTitleOnVideo(boolean enableSubTitleOnVideo) {
        this.enableSubTitleOnVideo = enableSubTitleOnVideo;
    }

    public boolean isEnableForegroundDetect() {
        return enableForegroundDetect;
    }
    public void setEnableForegroundDetect(boolean enableForegroundDetect) {
        this.enableForegroundDetect = enableForegroundDetect;
    }

    public boolean isShowTestVideoContentInfo() {
        return showTestVideoContentInfo;
    }
    public void setShowTestVideoContentInfo(boolean showTestVideoContentInfo) {
        this.showTestVideoContentInfo = showTestVideoContentInfo;
    }

    public boolean isShowTestVideoControlButtons() {
        return showTestVideoControlButtons;
    }
    public void setShowTestVideoControlButtons(boolean showTestVideoControlButtons) {
        this.showTestVideoControlButtons = showTestVideoControlButtons;
    }

    public boolean isShowTestVideoTimer() {
        return showTestVideoTimer;
    }
    public void setShowTestVideoTimer(boolean showTestVideoTimer) {
        this.showTestVideoTimer = showTestVideoTimer;
    }

    private void appendString(StringBuilder builder, String value) {
        builder.append(value + System.getProperty("line.separator"));
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        if (this.flavor != null) {
            appendString(builder, "flavor = " + this.flavor);
        }

        // support languages
        if (this.supportLanguages != null && this.supportLanguages.size() != 0) {
            for (int i = 0; i < this.supportLanguages.size(); i++) {
                appendString(builder, "supportLanguages[" + i + "] = " + this.supportLanguages.get(i));
            }
        } else {
            appendString(builder, "supportLanguages =");
        }

        // major languages
        if (this.majorLanguages != null && this.majorLanguages.size() != 0) {
            for (int i = 0; i < this.majorLanguages.size(); i++) {
                appendString(builder, "majorLanguages[" + i + "] = " + this.majorLanguages.get(i));
            }
        } else {
            appendString(builder, "majorLanguages =");
        }

        // ignore major languages
        if (this.ignoreMajorLanguages != null && this.ignoreMajorLanguages.size() != 0) {
            for (int i = 0; i < this.ignoreMajorLanguages.size(); i++) {
                appendString(builder, "ignoreMajorLanguages[" + i + "] = " + this.ignoreMajorLanguages.get(i));
            }
        } else {
            appendString(builder, "ignoreMajorLanguages =");
        }

        // major video resource folders
        if (this.ignoreMajorLanguages != null && this.ignoreMajorLanguages.size() != 0) {
            for (int i = 0; i < this.ignoreMajorLanguages.size(); i++) {
                appendString(builder, "ignoreMajorLanguages[" + i + "] = " + this.ignoreMajorLanguages.get(i));
            }
        } else {
            appendString(builder, "ignoreMajorLanguages =");
        }

        // support carriers
        if (this.supportCarriers != null && this.supportCarriers.size() != 0) {
            for (int i = 0; i < this.supportCarriers.size(); i++) {
                appendString(builder, "supportCarriers[" + i + "] = " + this.supportCarriers.get(i));
            }
        } else {
            appendString(builder, "supportCarriers =");
        }

        appendString(builder, "enableBrightnessControl = " + this.enableBrightnessControl);
        appendString(builder, "applyDefaultBrightness = " + this.applyDefaultBrightness);
        appendString(builder, "defaultBrightnessRatio = " + this.defaultBrightnessRatio);

        appendString(builder, "enableVolumeControl = " + this.enableVolumeControl);
        appendString(builder, "applyDefaultVolume = " + this.applyDefaultVolume);
        appendString(builder, "defaultSpeakerVolumeRatio = " + this.defaultSpeakerVolumeRatio);
        appendString(builder, "defaultHeadsetVolumeRatio = " + this.defaultHeadsetVolumeRatio);

        appendString(builder, "enableScreenOffDetection = " + this.enableScreenOffDetection);
        appendString(builder, "enableRebootDetection = " + this.enableRebootDetection);
        appendString(builder, "enableFaceDetection = " + this.enableFaceDetection);
        appendString(builder, "enableWearableFunction = " + this.enableWearableFunction);
        appendString(builder, "enableSubTitleOnVideo = " + this.enableSubTitleOnVideo);
        appendString(builder, "enableForegroundDetect = " + this.enableForegroundDetect);
        appendString(builder, "showTestVideoContentInfo = " + this.showTestVideoContentInfo);
        appendString(builder, "showTestVideoTimer = " + this.showTestVideoTimer);
        appendString(builder, "showTestVideoControlButtons = " + this.showTestVideoControlButtons);

        return builder.toString();
    }

}
