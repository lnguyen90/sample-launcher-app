package com.tecace.retail.res.content;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ProgressBar;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.AWSStartupHandler;
import com.amazonaws.mobile.client.AWSStartupResult;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.tecace.retail.res.ResContent;
import com.tecace.retail.res.content.obb.APKExpansionSupport;
import com.tecace.retail.res.content.obb.ZipResourceFile;
import com.tecace.retail.util.PreferenceUtil;
import com.tecace.retail.util.unzip.AppUnzipUtil;
import com.tecace.retail.util.unzip.UnzipUtil;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class AppContent implements ResContent {

    private static final String TAG = AppContent.class.getSimpleName();

    //region Singleton Pattern
    private static volatile AppContent sInstance;
    public static AppContent getInstance() {
        if (sInstance == null) {
            synchronized (AppContent.class) {
                if (sInstance == null)
                    sInstance = new AppContent();
            }
        }
        return sInstance;
    }
    private AppContent() {}
    //endregion


    private UnzipUtil unzipUtil = AppUnzipUtil.getInstance();
    private String root(Activity activity) {
        return activity.getExternalFilesDir(null).toString();
    }

    //region Content Unzip Process
    private void unzip(String root) {
        Log.d(TAG, "HJ### unzip !! ");
        unzip(root, null);
    }
    private void unzip(String root, Activity activity) {
        Log.d(TAG, "HJ### unzip !! ");
        if (root == null || root.length() == 0) return;

        if (unzipUtil.hasZipFiles(root)) {
            showDialog();

            if (activity != null && activity instanceof PrepareContentListener) {
                unzipUtil.unzipFiles(root, new UnzipProcess(((PrepareContentListener)activity).getPrepareContent()) {
                    @Override
                    public void onNext(String zipFile) {
                        Log.d(TAG, "##### onNext : unzipProcess : onNext)+ " + zipFile);
                    }

                    @Override
                    public void onError() {
                        Log.d(TAG, "##### unzipProcess : onError)+ ");
                    }

                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "##### unzipProcess : onCompleted)+ ");
                        deinitDialog();
                        this.prepareContent.onPreparedContent();
                    }
                });
            }
            else {
                unzipUtil.unzipFiles(root, unzipProcess);
            }
        }
        else {
            deinitDialog();
            if (activity != null && activity instanceof PrepareContentListener)
                ((PrepareContentListener) activity).getPrepareContent().onPreparedContent();
        }
    }
    public abstract class UnzipProcess implements UnzipUtil.UnzipProcess {
        PrepareContent prepareContent;
        public UnzipProcess(PrepareContent prepareContent) {
            this.prepareContent = prepareContent;
        }
    }
    UnzipUtil.UnzipProcess unzipProcess = new UnzipUtil.UnzipProcess() {
        @Override
        public void onNext(String zipFile) {
            Log.d(TAG, "##### onNext : unzipProcess : onNext)+ " + zipFile);
        }

        @Override
        public void onError() {
            Log.d(TAG, "##### unzipProcess : onError)+ ");
        }

        @Override
        public void onCompleted() {
            Log.d(TAG, "##### unzipProcess : onCompleted)+ ");
            deinitDialog();
        }
    };

    //endregion


    //region Message Dialog
    private ProgressDialog mDialog = null;
    private void initDialog(@NonNull Context context, String message) {
        Log.d(TAG, "##### createDialog)+ ");

        if (mDialog != null) {
            hideDialog();
            mDialog = null;
        }

        mDialog = new ProgressDialog(context);
        mDialog.setIndeterminate(false);
        mDialog.setCancelable(false);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        Drawable drawable = new ProgressBar(context).getIndeterminateDrawable().mutate();
        drawable.setColorFilter(Color.parseColor("#56bade"), PorterDuff.Mode.SRC_IN);
        mDialog.setIndeterminateDrawable(drawable);

        mDialog.setMessage(message);
    }
    public void deinitDialog() {
        if (mDialog != null) {
            hideDialog();
            mDialog = null;
        }
    }
    private void showDialog() {
        if(mDialog != null && !mDialog.isShowing()) {
            Log.d(TAG, "##### showDialog)+ SHOW");
            mDialog.show();
        }
    }
    private void hideDialog() {
        if(mDialog != null && mDialog.isShowing()) {
            Log.d(TAG, "##### hideDialog)+ DISMISS");
            mDialog.dismiss();
        }
    }
    //endregion



    private String contentsType;
    @Override
    public void prepareContent(Source source,
                               @NonNull Activity activity,
                               String popup,
                               String url,
                               String wifiDisconnectedMessage,
                               String contentSizeWarningMessage) {

        if (activity == null) return;

        contentsType = source.toString();
        switch (source) {
            case NONE:
                break;
            case RMS:
                prepareRMSContents(activity, popup);
                break;
            case OBB:
                prepareOBBContents(activity, popup);
                break;
            case AMAZON:
                prepareAmazonContents(activity, popup, url, wifiDisconnectedMessage, contentSizeWarningMessage);
                break;
            case AMAZONS3:
                prepareAmazonS3Contents(activity, popup, url, wifiDisconnectedMessage, contentSizeWarningMessage);
                break;
        }
    }




    //region RMS Contents
    private void prepareRMSContents(@NonNull Activity activity, String popup) {
        Log.d(TAG, "##### unzipRMSContents : root() = " + root(activity));
        initDialog(activity, popup);
        unzip(root(activity));
    }
    //endregion



    //region OBB Contents
    public static final String PREFERENCE_CONTENTS_VERSION = "PREFERENCE_CONTENTS_VERSION";
    private void prepareOBBContents(@NonNull Activity activity, String popup) {
        try {
            initDialog(activity, popup);

            int appVersionCode = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionCode;
            int obbVersionCode = appVersionCode;
            ZipResourceFile expansionFile = null;
            for(; obbVersionCode >0; obbVersionCode--){
                expansionFile = APKExpansionSupport.getAPKExpansionZipFile(activity, obbVersionCode, 0);
                if(expansionFile!=null) break;
            }

            //int versionCode = Consts.EXPANSION_VERSION_CODE;
            int lastCode = PreferenceUtil.getInstance().getInt(activity.getApplication(), PREFERENCE_CONTENTS_VERSION);
            Log.d(TAG, "#### prepareOBBContents : last version code:"+lastCode+")");

            if (expansionFile != null && lastCode != obbVersionCode) {
                ZipResourceFile.ZipEntryRO[] zip = expansionFile.getAllEntries();
                Log.d(TAG, "#### prepareOBBContents : zip[0].isUncompressed() : " + zip[0].isUncompressed());
                Log.d(TAG, "#### prepareOBBContents : mFile.getAbsolutePath() : " + zip[0].mFile.getAbsolutePath());
                Log.d(TAG, "#### prepareOBBContents : mFileName : " + zip[0].mFileName);
                Log.d(TAG, "#### prepareOBBContents : mZipFileName : " + zip[0].mZipFileName);
                Log.d(TAG, "#### prepareOBBContents : mCompressedLength : " + zip[0].mCompressedLength);

                String dataPath = activity.getApplication().getExternalFilesDir(null).toString();
                new UnzipOBBTask(activity, zip[0].mFile.getAbsolutePath(), dataPath, obbVersionCode).execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "#### prepareOBBContents : " + e.toString());
        }
    }
    private class UnzipOBBTask extends AsyncTask<Void, Integer, Boolean> {
        private String mOBBFile, mDataPath;
        private int mVersionCode;
        private WeakReference<Activity> activityRef;
        public UnzipOBBTask(Activity activity, String obbFile, String dataPath, int versionCode){
            this.activityRef = new WeakReference<>(activity);
            this.mOBBFile = obbFile;
            this.mDataPath = dataPath;
            this.mVersionCode = versionCode;
        }

        @Override
        protected void onPreExecute() {
            // TODO : show dialog
            Log.d(TAG, "#### onPreExecute)+");
            showDialog();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            // unzip obb file
            try {
                if (activityRef == null || activityRef.get() == null) return false;
                unzipOBB(mOBBFile, mDataPath);
                unzip(root(activityRef.get()));
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "#### doInBackground : " + e.toString());
                return false;
            }
        }

        @Override
        protected void onCancelled() {
            Log.d(TAG, "##### onCancelled : onFailure)+ ");
            deinitDialog();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            Log.d(TAG, "#### onPostExecute)+");
            if(result){
                Log.d(TAG, "#### onPostExecute : Save obb version code("+mVersionCode+") in the preference");
                if (this.activityRef == null || this.activityRef.get() == null) return;
                PreferenceUtil.getInstance().setInt(this.activityRef.get().getApplication(), PREFERENCE_CONTENTS_VERSION, mVersionCode);
            }
        }
    }
    static int BUFFER_SIZE = 2048;
    private boolean unzipOBB(String zipFile, String location) throws IOException {
        if(!location.endsWith(File.separator)) location = location + File.separator;
        if (zipFile == null || zipFile.length() == 0) return false;

        int size;
        byte[] buffer = new byte[BUFFER_SIZE];

        try {
            //String location = getFolderPath(zipFile);
            ZipInputStream zin = new ZipInputStream(
                    new BufferedInputStream(new FileInputStream(zipFile), BUFFER_SIZE));
            try {
                ZipEntry ze = null;
                int i = 0;
                while ((ze = zin.getNextEntry()) != null) {
                    String path = location + ze.getName();
                    File unzipFile = new File(path);

                    Log.d(TAG+"unzip", "["+(i++)+"]unzip:"+unzipFile.getAbsolutePath());
                    if (ze.isDirectory()) {
                        if (!unzipFile.isDirectory()) {
                            unzipFile.mkdirs();
                        }
                    } else {
                        // check for and create parent directories if they don't exist
                        File parentDir = unzipFile.getParentFile();
                        if (null != parentDir) {
                            if (!parentDir.isDirectory()) {
                                parentDir.mkdirs();
                            }
                        }

                        // unzip the file
                        FileOutputStream out = new FileOutputStream(unzipFile, false);
                        BufferedOutputStream fout = new BufferedOutputStream(out, BUFFER_SIZE);
                        try {
                            while ((size = zin.read(buffer, 0, BUFFER_SIZE)) != -1) {
                                fout.write(buffer, 0, size);
                            }
                            zin.closeEntry();
                        } finally {
                            fout.flush();
                            fout.close();
                        }
                    }
                }
            } finally {
                zin.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    //endregion



    //region Amazon Contents
    private void prepareAmazonContents(@NonNull Activity activity,
                                       String popup,
                                       String url,
                                       String wifiDisconnectedMessage,
                                       String contentSizeWarningMessage) {
        Log.d(TAG, "##### prepareAmazonContents)+ ");
        try {
            initDialog(activity, popup);

            int appVersionCode = activity.getApplication().getPackageManager().getPackageInfo(activity.getApplication().getPackageName(), 0).versionCode;
            int lastCode = PreferenceUtil.getInstance().getInt(activity.getApplication(), PREFERENCE_CONTENTS_VERSION);
            Log.d(TAG, "#### prepareAmazonContents : last version code:"+lastCode+")");
            Log.d(TAG, "HJ### lastCode = " + lastCode + "app = " + appVersionCode);

            if(lastCode == appVersionCode) {
//                return;
                unzip(root(activity));
            }
            else if(!isNetworkConnected(activity))
                NetworkDisconnectDialog(activity, wifiDisconnectedMessage);
            else if(asyncTask != null && asyncTask.getStatus() == AsyncTask.Status.RUNNING) {
                showDialog();
            }
            else
                QuestionDialog(activity, url, appVersionCode, wifiDisconnectedMessage, contentSizeWarningMessage);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "#### prepareAmazonContents : " + e.toString());
        }
    }
    public boolean isNetworkConnected(@NonNull Activity activity) {
        ConnectivityManager cm = (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
    private void NetworkDisconnectDialog(@NonNull Activity activity, String wifiDisconnectedMessage){
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(activity);
        alt_bld.setMessage(wifiDisconnectedMessage)
                .setCancelable(false)
                .setPositiveButton("OK",
                (dialog, id) -> {
                    deinitDialog();
                    activity.finish();
                });
        AlertDialog alert = alt_bld.create();
        alert.setOnShowListener(dialogInterface -> alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK));
        alert.show();
    }
    private void QuestionDialog(@NonNull Activity activity, String url, int versioncode, String wifiDisconnectedMessage, String contentSizeWarningMessage){
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(activity);
        alt_bld.setMessage(contentSizeWarningMessage)
                .setCancelable(false)
                .setNegativeButton("CANCEL",
                (dialog, id) -> {
                    deinitDialog();
                    activity.finish();
                })
                .setPositiveButton("OK",
                (dialog, id) -> {
                    // Action for 'OK' Button
                    if (!isNetworkConnected(activity))
                        NetworkDisconnectDialog(activity, wifiDisconnectedMessage);
                    else {
                        if (contentsType.equalsIgnoreCase("AMAZON"))
                            downloadFile(activity, url, versioncode);

                        else if (contentsType.equalsIgnoreCase("AMAZONS3"))
                            downloadWithTransferUtility(activity, url, versioncode);
                    }
                });
        AlertDialog alert = alt_bld.create();
        alert.setOnShowListener(dialogInterface -> {
            alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
            alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        });
        alert.show();
    }
    public AsyncTask asyncTask;
    private void downloadFile(Activity activity, String url, int versioncode) {
        if (url == null && url.length() == 0) return;
        String serverURL = url.substring(0, url.lastIndexOf("/")+1);
        String fileURL = url.substring(url.lastIndexOf("/")+1, url.length());


        showDialog();

        // create Retrofit instance
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(serverURL);

        Retrofit retrofit = builder.build();

        // todo get client & call object for the request
        Downloader downloader = retrofit.create(Downloader.class);

        Call<ResponseBody> call = downloader.downloadFile(fileURL);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                asyncTask = new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        boolean success = writeResponseBodyToDisk(activity, response.body());
                        Log.d(TAG, "HJ### downloadFile : onResponse)+ " + success);
                        if(success) {
                            PreferenceUtil.getInstance().setInt(activity.getApplication(), PREFERENCE_CONTENTS_VERSION, versioncode);
                            unzip(root(activity));
                        }
                        else {
                            deinitDialog();
                            activity.finish();
                        }
                        return null;
                    }
                }.execute();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "HJ### downloadFile : onFailure)+ ");
                deinitDialog();
            }
        });
    }
    private boolean writeResponseBodyToDisk(@NonNull Activity activity, ResponseBody body) {
        try {
            // todo change the file location/name according to your needs
            File contentFile = new File(activity.getApplication().getExternalFilesDir(null) + File.separator + "contents.zip");

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(contentFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Log.d(TAG, "#### writeResponseBodyToDisk : downloaded : " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            Log.d(TAG, "IOException : " );
            return false;
        }
    }
    //endregion


    //region Amazon S3 Contents
    private void prepareAmazonS3Contents(@NonNull Activity activity,
                                         String popup,
                                         String url,
                                         String wifiDisconnectedMessage,
                                         String contentSizeWarningMessage) {
        AWSMobileClient.getInstance().initialize(activity, new AWSStartupHandler() {
            @Override
            public void onComplete(AWSStartupResult awsStartupResult) {
                try {
                    initDialog(activity, popup);

                    int appVersionCode = activity.getApplication().getPackageManager().getPackageInfo(activity.getApplication().getPackageName(), 0).versionCode;
                    int lastCode = PreferenceUtil.getInstance().getInt(activity.getApplication(), PREFERENCE_CONTENTS_VERSION);
                    Log.d(TAG, "#### prepareAmazonContents : last version code:"+lastCode+")");
                    Log.d(TAG, "HJ### lastCode = " + lastCode + ",   appCode = " + appVersionCode);

                    if(lastCode == appVersionCode) {
                        unzip(root(activity), activity);
                    }
                    else if(!isNetworkConnected(activity))
                        NetworkDisconnectDialog(activity, wifiDisconnectedMessage);
                    else if(inProgress) {
                        Log.d(TAG, "HJ### inProgress now !! ");
                        showDialog();
                    }
                    else
                        QuestionDialog(activity, url, appVersionCode, wifiDisconnectedMessage, contentSizeWarningMessage);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, "#### prepareAmazonS3Contents : " + e.toString());
                }
            }
        }).execute();
    }

    private boolean inProgress;
    private void downloadWithTransferUtility(@NonNull Activity activity, String url, int versioncode) {
        Log.d(TAG, "HJ### downloadWithTransferUtility ");
        Log.d(TAG, "HJ### AWSMobileClient.getInstance().getCredentialsProvider() : " + AWSMobileClient.getInstance().getCredentialsProvider());
        Log.d(TAG, "HJ### AWSMobileClient.getInstance().getConfiguration() : " + AWSMobileClient.getInstance().getConfiguration());


        if (url == null && url.length() == 0) return;
        String serverURL = url.substring(0, url.lastIndexOf("/"));
        String fileURL = url.substring(url.lastIndexOf("/")+1, url.length());

        File contentFile = new File(activity.getApplication().getExternalFilesDir(null) + File.separator + fileURL);
        Log.d(TAG, "HJ### String.valueOf(contentFile) : " + String.valueOf(contentFile));

        TransferUtility transferUtility =
                TransferUtility.builder()
                        .context(activity.getApplicationContext())
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider()))
                        .build();

        TransferObserver downloadObserver =
                transferUtility.download(fileURL, new File(String.valueOf(contentFile)));


        // Attach a listener to the observer to get state update and progress notifications
        downloadObserver.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
                Log.d(TAG, "HJ### onStateChanged : " + state);
                if (TransferState.COMPLETED == state) {
                    // Handle a completed upload.
                    Log.d(TAG, "HJ### TransferState.COMPLETED ");
                    PreferenceUtil.getInstance().setInt(activity.getApplication(), PREFERENCE_CONTENTS_VERSION, versioncode);
                    unzip(root(activity), activity);
                }
                if (TransferState.IN_PROGRESS == state) {
                    Log.d(TAG, "HJ### TransferState.IN_PROGRESS ");
                    inProgress = true;
                    showDialog();
                }
                if (TransferState.PAUSED == state) {
                    inProgress = false;
                }
                if (TransferState.CANCELED == state) {
                    Log.d(TAG, "HJ### TransferState.CANCELED ");
                    inProgress = false;
                    deinitDialog();
                    activity.finish();
                }
                if (TransferState.FAILED == state) {
                    Log.d(TAG, "HJ### TransferState.FAILED ");
                    inProgress = false;
                    deinitDialog();
                    activity.finish();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float)bytesCurrent/(float)bytesTotal) * 100;
                int percentDone = (int)percentDonef;

                Log.d(TAG, "HJ###   ID:" + id + "   bytesCurrent: " + bytesCurrent + "   bytesTotal: " + bytesTotal + " " + percentDone + "%");
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.d(TAG, "HJ### onError : " + ex);
                // Handle errors
            }

        });

        // If you prefer to poll for the data, instead of attaching a
        // listener, check for the state and progress in the observer.
        if (TransferState.COMPLETED == downloadObserver.getState()) {
            // Handle a completed upload.
        }

        Log.d(TAG, "HJ### Bytes Transferrred: " + downloadObserver.getBytesTransferred());
        Log.d(TAG, "HJ### Bytes Total: " + downloadObserver.getBytesTotal());
    }
    //endregion

}