package com.tecace.retail.res.util;

/**
 * Created by icanmobile on 4/28/17.
 */

public class ResConst {
    public static final String FILES_FOLDER = "files";
    public static final String CHAPTER_FOLDER = "chapter";
    public static final String MODEL_FOLDER = "model";
    public static final String FRAME_FOLDER = "frame";
    public static final String IMAGE_FOLDER = "image";
    public static final String AUDIO_FOLDER = "audio";
    public static final String VIDEO_FOLDER = "video";

    public static String JPEG_EXT = ".jpg";
    public static String PNG_EXT = ".png";
    public static String NINE_PATCH_EXT = ".9.png";
    public static String BMP_EXT = ".bmp";

    //[TA] 11/08/2017. Use Language - Country
//    public final static String FOLDER_MODEL_STRING = "model/strings.xml";
    public final static String FOLDER_MODEL_DIMEN = "model/dimens.xml";
    public final static String CONFIGS_MODEL = "configs.json";
}
