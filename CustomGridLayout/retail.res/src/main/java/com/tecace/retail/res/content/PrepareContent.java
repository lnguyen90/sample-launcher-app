package com.tecace.retail.res.content;

public interface PrepareContent {
    void onPreparedContent();
}
