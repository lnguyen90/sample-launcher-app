package com.tecace.retail.dynamic.model;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.SerializedName;
import com.tecace.retail.dynamic.content.Config;

import java.lang.reflect.Type;


public class ConfigModel extends BaseModel implements JsonSerializer<ConfigModel>, Cloneable {
    private static final String TAG = ConfigModel.class.getSimpleName();

    private static final String FIELD_FLAVOR = "FLAVOR";
    private static final String FIELD_CONTENT = "CONTENT";
    private static final String FIELD_FOLDER = "FOLDER";
    private static final String FIELD_PAGE_1 = "PAGE_1";

    @SerializedName(FIELD_FLAVOR)
    private String flavor;

    @SerializedName(FIELD_CONTENT)
    private CONTENT content;

    @SerializedName(FIELD_PAGE_1)
    private PAGE_1 page_1;


    public String flavor() {
        return this.flavor;
    }

    public CONTENT content() {
        return this.content;
    }
    public void content(CONTENT content) {
        this.content = content;
    }

    public PAGE_1 page_1() {
        return this.page_1;
    }
    public void page_1(PAGE_1 main) {
        this.page_1 = page_1;
    }

    @Override
    public JsonElement serialize(ConfigModel src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.add(Config.FLAVOR.name(), context.serialize(src.flavor()));

        if (src.content() != null) {
            jsonObject.add(Config.CONTENT.name(),
                    context.serialize(
                            src.content().serialize(src.content(), typeOfSrc, context)
                    )
            );
        }

        if (src.page_1() != null) {
            jsonObject.add(Config.PAGE_1.name(),
                    context.serialize(
                            src.page_1().serialize(src.page_1(), typeOfSrc, context)
                    )
            );
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        StringBuilder sbuilder = new StringBuilder();
        appendString(sbuilder, Config.FLAVOR.name() + " : " + (flavor() != null ? flavor() : null));
        appendString(sbuilder, Config.CONTENT.name() + " : " + (content() != null ? content().toString() : null));
        appendString(sbuilder, Config.PAGE_1.name() + " : " + (page_1() != null ? page_1().toString() : null));
        return sbuilder.toString();
    }

    @Override
    public void print() {
        Log.d(ConfigModel.class.getName(), toString());
    }

    @Override
    public boolean equals(Object o) {
        ConfigModel configModel = (ConfigModel) o;
        if ( (flavor() == null && configModel.flavor() != null) || flavor() != null && configModel.flavor() == null)
            return false;
        if ( (content() == null && configModel.content() != null) || content() != null && configModel.content() == null)
            return false;
        if ( (page_1() == null && configModel.page_1() != null) || page_1() != null && configModel.page_1() == null)
            return false;

        boolean retValue = true;
        if (flavor() != null)
            retValue &= flavor().equals( ((ConfigModel)o).flavor() );
        if (content() != null)
            retValue &= content().equals( ((ConfigModel) o).content() );
        if (page_1() != null)
            retValue &= page_1().equals( ((ConfigModel) o).page_1() );
        return retValue;
    }

    @Override
    public ConfigModel clone() throws CloneNotSupportedException {
        return (ConfigModel) super.clone();
    }



    public static class CONTENT extends BaseModel implements JsonSerializer<CONTENT>, Cloneable {
        @SerializedName(FIELD_FOLDER)
        private String folder;


        public String folder() {
            return this.folder;
        }

        @Override
        public JsonElement serialize(CONTENT src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add(Config.FOLDER.name(), context.serialize(src.folder()));
            return jsonObject;
        }

        @Override
        public String toString() {
            StringBuilder sbuilder = new StringBuilder();
            appendString(sbuilder, Config.FOLDER.name() + " : " + folder());
            return sbuilder.toString();
        }


        @Override
        public void print() {
            Log.d(CONTENT.class.getName(), toString());
        }

        @Override
        public boolean equals(Object o) {
            return folder.equals(((CONTENT)o).folder());
        }

        @Override
        public CONTENT clone() throws CloneNotSupportedException {
            return (CONTENT) super.clone();
        }
    }


    public static class PAGE_1 extends BaseModel implements JsonSerializer<PAGE_1>, Cloneable {

        private static final String MAX_COLUMN = "MAX_COLUMN";
        private static final String MAX_ROW = "MAX_ROW";
        private static final String CHILD_VIEWS = "CHILD_VIEWS";

        @SerializedName(MAX_COLUMN)
        private int maxColumn;

        @SerializedName(MAX_ROW)
        private int maxRow;

        @SerializedName(CHILD_VIEWS)
        private GridInfoArrayModel childViews;

        public int getMaxColumn() {
            return maxColumn;
        }

        public int getMaxRow() {
            return maxRow;
        }

        public GridInfoArrayModel getChildViews() {
            return childViews;
        }

        @Override
        public JsonElement serialize(PAGE_1 src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add(MAX_COLUMN, context.serialize(src.getMaxColumn()));
            jsonObject.add(MAX_ROW, context.serialize(src.getMaxColumn()));
            JsonArray jsonArray = new JsonArray();
            for (int i=0; i<src.getChildViews().size(); i++) {
                jsonArray.add(src.getChildViews().get(i).serialize(src.getChildViews().get(i), typeOfSrc, context));
            }
            jsonObject.add(CHILD_VIEWS, jsonArray);
            return jsonObject;
        }

        @Override
        public String toString() {
            StringBuilder sbuilder = new StringBuilder();
            appendString(sbuilder, MAX_COLUMN + " : " + getMaxColumn());
            appendString(sbuilder, MAX_ROW + " : " + getMaxRow());
            appendString(sbuilder, CHILD_VIEWS + " : " + getChildViews().toString());
            return sbuilder.toString();
        }

        @Override
        public void print() {
            Log.d(PAGE_1.class.getName(), toString());
        }

        @Override
        public boolean equals(Object o) {
            return  maxColumn == ((PAGE_1)o).getMaxColumn() &&
                    maxRow    == ((PAGE_1)o).getMaxRow() &&
                    childViews.equals(((PAGE_1)o).getChildViews());
        }

        @Override
        public PAGE_1 clone() throws CloneNotSupportedException {
            return (PAGE_1) super.clone();
        }
    }
}
