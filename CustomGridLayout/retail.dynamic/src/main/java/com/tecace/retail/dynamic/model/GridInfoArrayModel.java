package com.tecace.retail.dynamic.model;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class GridInfoArrayModel extends ArrayList<GridInfoArrayModel.Item> implements JsonSerializer<GridInfoArrayModel>, Cloneable {


    @Override
    public JsonElement serialize(GridInfoArrayModel src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray jsonArray = new JsonArray();
        for (int i=0; i<src.size(); i++) {
            jsonArray.add(src.get(i).serialize(src.get(i), typeOfSrc, context));
        }
        return jsonArray;
    }

    @Override
    public String toString() {
        StringBuilder sbuilder = new StringBuilder();
       for(GridInfoArrayModel.Item item : this) {
           appendString(sbuilder, item.toString());
        }
        return sbuilder.toString();
    }

    @Override
    public GridInfoArrayModel clone() {
        return (GridInfoArrayModel) super.clone();
    }

    public void appendString(StringBuilder builder, String value) {
        builder.append(value + System.getProperty("line.separator"));
    }

    @Override
    public boolean equals(Object o) {
        GridInfoArrayModel gridInfoModel = (GridInfoArrayModel) o;

        if (size() != gridInfoModel.size()) return false;
        for(int i=0; i<size(); i++) {

            if (gridInfoModel.get(i).getType() == null) return false;
            if (!get(i).getType().equals(gridInfoModel.get(i).getType())) return false;

            if (gridInfoModel.get(i).getColumn() == null) return false;
            if (!get(i).getColumn().equals(gridInfoModel.get(i).getColumn())) return false;

            if (gridInfoModel.get(i).getRow() == null) return false;
            if (!get(i).getRow().equals(gridInfoModel.get(i).getRow())) return false;

            if (gridInfoModel.get(i).getColumnSpan() == null) return false;
            if (!get(i).getColumnSpan().equals(gridInfoModel.get(i).getColumnSpan())) return false;

            if (gridInfoModel.get(i).getRowSpan() == null) return false;
            if (!get(i).getRowSpan().equals(gridInfoModel.get(i).getRowSpan())) return false;

        }
        return true;
    }

    public static class Item implements JsonSerializer<GridInfoArrayModel.Item>, Cloneable {

        private static final String TYPE = "TYPE";
        private static final String COLUMN = "COLUMN";
        private static final String ROW = "ROW";
        private static final String COLUMN_SPAN = "COLUMN_SPAN";
        private static final String ROW_SPAN = "ROW_SPAN";

        @SerializedName(TYPE)
        private String type;

        @SerializedName(COLUMN)
        private Integer column;

        @SerializedName(ROW)
        private Integer row;

        @SerializedName(COLUMN_SPAN)
        private Integer columnSpan;

        @SerializedName(ROW_SPAN)
        private Integer rowSpan;


        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Integer getColumn() {
            return column;
        }

        public void setColumn(int column) {
            this.column = column;
        }

        public Integer getRow() {
            return row;
        }

        public void setRow(int row) {
            this.row = row;
        }

        public Integer getColumnSpan() {
            return columnSpan;
        }

        public void setColumnSpan(int columnSpan) {
            this.columnSpan = columnSpan;
        }

        public Integer getRowSpan() {
            return rowSpan;
        }

        public void setRowSpan(int rowSpan) {
            this.rowSpan = rowSpan;
        }

        public String toString() {
            StringBuilder sbuilder = new StringBuilder();
            appendString(sbuilder, TYPE + " : " + getType());
            appendString(sbuilder, COLUMN + " : " + getColumn());
            appendString(sbuilder, ROW + " : " + getRow());
            appendString(sbuilder, COLUMN_SPAN + " : " + getColumnSpan());
            appendString(sbuilder, ROW_SPAN + " : " + getRowSpan());
            return sbuilder.toString();
        }

        public void appendString(StringBuilder builder, String value) {
            builder.append(value + System.getProperty("line.separator"));
        }

        @Override
        public GridInfoArrayModel.Item clone() throws CloneNotSupportedException {
            return (GridInfoArrayModel.Item) super.clone();
        }

        @Override
        public boolean equals(Object o) {
            return  type.equals(((GridInfoArrayModel.Item)o).getType()) &&
                    column.equals(((GridInfoArrayModel.Item)o).getColumn()) &&
                    row.equals(((GridInfoArrayModel.Item)o).getRow()) &&
                    columnSpan.equals(((GridInfoArrayModel.Item)o).getColumnSpan()) &&
                    rowSpan.equals(((GridInfoArrayModel.Item)o).getRowSpan());
        }

        @Override
        public JsonElement serialize(GridInfoArrayModel.Item src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add(TYPE , context.serialize(src.getType()));
            jsonObject.add(COLUMN, context.serialize(src.getColumn()));
            jsonObject.add(ROW, context.serialize(src.getRow()));
            jsonObject.add(COLUMN_SPAN, context.serialize(src.getColumnSpan()));
            jsonObject.add(ROW_SPAN, context.serialize(src.getRowSpan()));
            return jsonObject;
        }
    }
}
