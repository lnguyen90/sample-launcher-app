package com.tecace.retail.dynamic;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.tecace.retail.dynamic.content.Config;
import com.tecace.retail.dynamic.model.CarriersModel;
import com.tecace.retail.dynamic.model.ConfigModel;
import com.tecace.retail.dynamic.model.ConfigsModel;
import com.tecace.retail.dynamic.model.PageModel;
import com.tecace.retail.dynamic.util.JsonUtil;
import com.tecace.retail.res.observer.ResFileObserver;

import java.io.File;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

/**
 * Created by icanmobile on 6/14/17.
 * Dynamic class supports to generate the JSON files for main, submenu, and device spec based on config.json file.
 * Dynamic class is Singleton class.
 */

public class Dynamic implements ResFileObserver.BaseFileObserver {
    private static final String TAG = Dynamic.class.getSimpleName();

    private static volatile Dynamic sInstance;

    public static final String SUBSIDIARY_DEFAULT = "default";
    public static final String SUBSIDIARY_SEA = "sea";
    public static final String SUBSIDIARY_SEAU = "seau";
    public static final String CARRIER_DEFAULT = "default";
    public static final String PRODUCT_FLAVOR_SAMPLE = "sample";
    /**
     * get Dynamic Singleton object.
     * @return Dynamic Singleton object.
     */
    public static Dynamic getInstance() {
        if (sInstance == null) {
            synchronized (Dynamic.class) {
                if (sInstance == null)
                    sInstance = new Dynamic();
            }
        }
        return sInstance;
    }
    private Dynamic(){
//        Res.registerContentObserver(this);
    }

    //region Res Content Observer
    @Override
    public void updateJsons() {

    }
    @Override
    public void updateImages() {

    }

    private static Set<DynamicObserver> weakObservers
            = Collections.newSetFromMap(new WeakHashMap<DynamicObserver, Boolean>());
    public void registerObserver(@NonNull DynamicObserver observer) {
        if (!weakObservers.contains(observer))
            weakObservers.add(observer);
    }
    public void removeObserver(@NonNull DynamicObserver observer) {
        if (weakObservers.contains(observer))
            weakObservers.remove(observer);
    }
    private static void notifyObservers() {
        Log.d(TAG, "##### notifyObservers)+ ");
        for(DynamicObserver observer : weakObservers) {
            observer.updated();
        }
    }

    /**
     * reset all config, main, submenus, device spec json files.
     * @param context application or activity context.
     */
    public void reset(Context context, String subsidiary, String carrier, String flavor) {
        Log.d(TAG, "##### reset : " + subsidiary + ", " + carrier + ", " + flavor);

        deleteAllConfigs(context);
        refresh(context, subsidiary, carrier, flavor);
    }

    public String getFlavor(Context context) {
        ConfigModel extConfigModel = (ConfigModel) JsonUtil.getInstance().loadJson(context,
                Config.CONFIG.getClassOfT(),
                Config.CONFIG.getInputFile(),
                Config.CONFIG.getInputStorageOpt()
        );

        if (extConfigModel == null)
            return getDevice();

        return extConfigModel.flavor();
    }

    public String getDevice() {
//        try {
//            //Method 1 - Substring 4 ~ 7 -> 960 or 965
//            int deviceInt = Integer.parseInt(Build.MODEL.substring(4, 7));
//
//            //Method 2 - Merge all integers -> 960 or 965
//            //int deviceInt = Integer.parseInt(Build.MODEL.replaceAll("[\\D]", ""));
//
//            switch (deviceInt) {
//                case (960):
//                    return PRODUCT_FLAVOR_CROWN;
//                case (965):
//                    return PRODUCT_FLAVOR_CROWN;
//                default:
//                    return PRODUCT_FLAVOR_CROWN;
//            }
//        } catch (NullPointerException e) {
//            return PRODUCT_FLAVOR_CROWN;
//        } catch (NumberFormatException e) {
//            return PRODUCT_FLAVOR_CROWN;
//        } catch (StringIndexOutOfBoundsException e) {
//            return PRODUCT_FLAVOR_CROWN;
//        } catch (Exception e) {
//            return PRODUCT_FLAVOR_CROWN;
//        }
        return PRODUCT_FLAVOR_SAMPLE;
    }

    /**
     * create main, submenus, and device spec json files based on config.json file.
     * @param context application or activity context.
     */
    public void refresh(Context context, String subsidiary, String carrier, String flavor) {
        if (subsidiary == null)
            subsidiary = SUBSIDIARY_DEFAULT;
        if (carrier == null)
            carrier = CARRIER_DEFAULT;
        if (flavor == null)
            flavor = getDevice();

        Log.d(TAG, "##### ICANMOIBLE : refresh)+ " + subsidiary + ", " + carrier + ", " + flavor);

        //Generate config.json file and get ConfigModel object.
        ConfigModel configModel = generateConfigs(context, subsidiary, carrier, flavor, true);
        if (configModel == null) return;
        saveConfig(context, configModel);

        //generate page 1
        generatePage1(context);

        notifyObservers();
    }

    /**
     * check the model/config.json file in external storage is modified or not.
     * @param context application or activity context.
     * @return if model/config.json file is modified  in external storage, return true.
     */
    public boolean isModified(Context context, String subsidiary, String carrier, String flavor) {
        if (subsidiary == null)
            subsidiary = SUBSIDIARY_DEFAULT;
        if (carrier == null)
            carrier = CARRIER_DEFAULT;
        if (flavor == null)
            flavor = getDevice();

        return isConfigsModified(context) || isConfigModified(context, subsidiary, carrier, flavor);
    }

    private String getConfigsFilePath(Context context) {
        return JsonUtil.getInstance().getExternalFileFullPath(context, Config.CONFIGS.getInputFile());
    }

    private ConfigModel generateConfigs(Context context, String subsidiary, String carrier, String flavor, boolean save) {
        String configsFile = getConfigsFilePath(context);

        //Load configs.json from external storage.
        ConfigsModel configsModel = (ConfigsModel) JsonUtil.getInstance().loadJson(context,
                Config.CONFIGS.getClassOfT(),
                configsFile,
                JsonUtil.Storage.UNKNOWN
        );
        //If not found configs.json in external storage, load configs.json in application asset folder.
        if (configsModel == null) {
            configsModel = (ConfigsModel) JsonUtil.getInstance().loadJson(context,
                    Config.CONFIGS.getClassOfT(),
                    Config.CONFIGS.getInitFile(),
                    Config.CONFIGS.getInitStorageOpt()
            );
        }

        if (save) {
            //Save configs.json file to external storage.
            JsonUtil.getInstance().saveJson(context, configsModel,
                    Config.CONFIGS.getClassOfT(),
                    configsFile,
                    JsonUtil.Storage.UNKNOWN
            );

            //Save configs.json file to internal storage to detect any modification from user.
            JsonUtil.getInstance().saveJson(context, configsModel,
                    Config.CONFIGS.getClassOfT(),
                    Config.CONFIGS.getInitFile(),
                    JsonUtil.Storage.INTERNAL
            );
        }

        //Find default and subsidiary configs
        ConfigModel defaultConfig = null;
        ConfigModel selectedConfig = null;
        for(ConfigsModel.Item subsidiaryConfig : configsModel) {
            //get default config
            if (subsidiaryConfig.subsidiary().equals(SUBSIDIARY_DEFAULT)) {
                for (CarriersModel.Item carrierConfig : subsidiaryConfig.carriers()) {
                    if (carrierConfig.carrier().equals(CARRIER_DEFAULT)) {
                        for (ConfigModel flavorConfig : carrierConfig.configs()) {
                            if (flavorConfig.flavor().equals(flavor)) {
                                try {
                                    defaultConfig = flavorConfig.clone();
                                } catch (CloneNotSupportedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }

            //get subsidiary config
            if (subsidiary != null && subsidiaryConfig.subsidiary().equals(subsidiary)) {
                for (CarriersModel.Item carrierConfig : subsidiaryConfig.carriers()) {
                    if (carrierConfig.carrier().equals(carrier)) {
                        for (ConfigModel flavorConfig : carrierConfig.configs()) {
                            if (flavorConfig.flavor().equals(flavor)) {
                                try {
                                    selectedConfig = flavorConfig.clone();
                                } catch (CloneNotSupportedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        }

        return selectedConfig != null ? mergeConfig(defaultConfig, selectedConfig) : defaultConfig;
    }

    private ConfigModel mergeConfig(ConfigModel defaultConfig, ConfigModel parcialConfig) {
        if (parcialConfig.content() != null)
            defaultConfig.content(parcialConfig.content());
        if (parcialConfig.page_1() != null)
            defaultConfig.page_1(parcialConfig.page_1());
        return defaultConfig;
    }

    private boolean isConfigsModified(Context context) {
        String rootConfigFile = JsonUtil.getInstance().getExternalFileFullPath(context, Config.CONFIGS.getInputFile());

        //Load configs.json from external storage.
        ConfigsModel extConfigsModel = (ConfigsModel) JsonUtil.getInstance().loadJson(context,
                Config.CONFIGS.getClassOfT(),
                rootConfigFile,
                JsonUtil.Storage.UNKNOWN
        );
        if (extConfigsModel == null) return true;

        //load internal model/config.json file
        ConfigsModel internalConfigsModel = (ConfigsModel) JsonUtil.getInstance().loadJson(context,
                Config.CONFIGS.getClassOfT(),
                Config.CONFIGS.getInitFile(),
                JsonUtil.Storage.INTERNAL
        );
        if (internalConfigsModel == null) return true;

        return !extConfigsModel.equals(internalConfigsModel);
    }

    private void deleteConfigs(Context context) {
        String configsFile = getConfigsFilePath(context);
        JsonUtil.getInstance().deleteFile(context,
                configsFile,
                JsonUtil.Storage.UNKNOWN
        );
    }

    private void deleteAllConfigs(Context context) {
        final String dir = context.getExternalFilesDir(null).toString();
        deleteFile(dir);
    }

    private void deleteFile(String rootDir) {
        File dir = new File(rootDir);
        String[] names = dir.list();
        for(String name : names) {
            String filePath = new StringBuilder().append(dir).append("/").append(name).toString();

            File file = new File(filePath);
            if (file.getName().equals("configs.json") || file.getName().equals("config.json")) {
                file.delete();
            }
            else if(file.isDirectory()) {
                deleteFile(file.getPath());
            }
        }
    }

    private boolean isConfigModified(Context context, String subsidiary, String carrier, String flavor) {
        //Load configs.json from external storage.
        ConfigModel extConfigModel = (ConfigModel) JsonUtil.getInstance().loadJson(context,
                Config.CONFIG.getClassOfT(),
                Config.CONFIG.getInputFile(),
                Config.CONFIG.getInputStorageOpt()
        );
        if (extConfigModel == null) return true;

        ConfigModel newConfig = generateConfigs(context, subsidiary, carrier, flavor, false);
        if (newConfig == null) return true;

        return !extConfigModel.equals(newConfig);
    }

    private void saveConfig(Context context, ConfigModel configModel) {
        //external storage
        JsonUtil.getInstance().saveJson(context, configModel,
                Config.CONFIG.getClassOfT(),
                Config.CONFIG.getOutputFile(),
                Config.CONFIG.getOutputStorageOpt()
        );
    }

    private void deleteConfig(Context context) {
        JsonUtil.getInstance().deleteFile(context,
                Config.CONFIG.getOutputFile(),
                Config.CONFIG.getOutputStorageOpt()
        );
    }


    private void generatePage1(Context context) {
        PageModel pageModel = (PageModel) JsonUtil.getInstance().loadJson(context,
                Config.PAGE_1.getClassOfT(),
                Config.PAGE_1.getInputFile(),
                Config.PAGE_1.getInputStorageOpt()
        );

        JsonUtil.getInstance().saveJson(context, pageModel,
                Config.PAGE_1.getClassOfT(),
                Config.PAGE_1.getOutputFile(),
                Config.PAGE_1.getOutputStorageOpt() );
    }


    private void deletePage1(Context context) {
        JsonUtil.getInstance().deleteFile(context,
                Config.PAGE_1.getOutputFile(),
                Config.PAGE_1.getOutputStorageOpt()
        );
    }

}
