package com.tecace.retail.dynamic.model;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by icanmobile on 12/1/17.
 */

public class ConfigsModel extends ArrayList<ConfigsModel.Item> implements JsonSerializer<ConfigsModel>, Cloneable {
    private static final String TAG = ConfigsModel.class.getSimpleName();

    private static final String FIELD_SUBSIDIARY = "subsidiary";
    private static final String FIELD_CARRIERS = "carriers";

    public static class Item implements JsonSerializer<Item>, Cloneable {

        @SerializedName(FIELD_SUBSIDIARY)
        private String subsidiary;

        @SerializedName(FIELD_CARRIERS)
        private CarriersModel carriers;

        public String subsidiary() {
            return this.subsidiary;
        }

        public CarriersModel carriers() {
            return this.carriers;
        }

        @Override
        public JsonElement serialize(Item src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add(FIELD_SUBSIDIARY, context.serialize(src.subsidiary()));
            JsonArray jsonArray = new JsonArray();
            for (int i=0; i<src.carriers().size(); i++) {
                jsonArray.add(src.carriers().get(i).serialize(src.carriers().get(i), typeOfSrc, context));
            }
            jsonObject.add(FIELD_CARRIERS, jsonArray);

            return jsonObject;
        }

        /**
         * toString
         * @return string values of SubmenuModel.Item object.
         */
        public String toString() {
            StringBuilder sbuilder = new StringBuilder();
            appendString(sbuilder, FIELD_SUBSIDIARY + " : " + subsidiary());
            for(CarriersModel.Item carrier : carriers)
                appendString(sbuilder, FIELD_CARRIERS + " : " + carrier.toString());
            return sbuilder.toString();
        }

        /**
         * append string with line.separator.
         * @param builder StringBuilder class object.
         * @param value string value.
         */
        public void appendString(StringBuilder builder, String value) {
            builder.append(value + System.getProperty("line.separator"));
        }

        /**
         * print
         */
        public void print() {
            Log.d(Item.class.getName(), toString());
        }

        @Override
        public Item clone() throws CloneNotSupportedException {
            return (Item) super.clone();
        }
    }

    @Override
    public JsonElement serialize(ConfigsModel src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray jsonArray = new JsonArray();
        for (int i=0; i<src.size(); i++) {
            jsonArray.add(src.get(i).serialize(src.get(i), typeOfSrc, context));
        }
        return jsonArray;
    }

    public String toString() {
        StringBuilder sbuilder = new StringBuilder();
        for(ConfigsModel.Item item : this) {
            appendString(sbuilder, item.toString());
        }
        return sbuilder.toString();
    }

    public void appendString(StringBuilder builder, String value) {
        builder.append(value + System.getProperty("line.separator"));
    }

    public void print() {
        Log.d(ConfigsModel.class.getName(), toString());
    }

    @Override
    public boolean equals(Object o) {
        ConfigsModel configsModel = (ConfigsModel) o;

        if (size() != configsModel.size()) return false;
        for(int i=0; i<size(); i++) {
            if (configsModel.get(i).subsidiary() == null) return false;
            if (!get(i).subsidiary().equals(configsModel.get(i).subsidiary())) return false;
            if (get(i).carriers().size() != configsModel.get(i).carriers().size()) return false;
            for(int j=0; j<get(i).carriers().size(); j++) {
                if (!get(i).carriers().get(j).equals(configsModel.get(i).carriers().get(j)))
                    return false;
            }
        }
        return true;
    }

    @Override
    public ConfigsModel clone() {
        return (ConfigsModel) super.clone();
    }
}
