package com.tecace.retail.dynamic.model;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class CarriersModel extends ArrayList<CarriersModel.Item> implements JsonSerializer<CarriersModel>, Cloneable {

    private static final String FIELD_CARRIER = "carrier";
    private static final String FIELD_CONFIGS = "configs";

    public static class Item implements JsonSerializer<Item>, Cloneable {
        @SerializedName(FIELD_CARRIER)
        private String carrier;

        @SerializedName(FIELD_CONFIGS)
        private ArrayList<ConfigModel> configs;

        public String carrier() {
            return this.carrier;
        }

        public ArrayList<ConfigModel> configs() {
            return this.configs;
        }


        @Override
        public JsonElement serialize(Item src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add(FIELD_CARRIER, context.serialize(src.carrier()));

            JsonArray jsonArray = new JsonArray();
            for (int i=0; i<src.configs().size(); i++) {
                jsonArray.add(src.configs().get(i).serialize(src.configs().get(i), typeOfSrc, context));
            }
            jsonObject.add(FIELD_CONFIGS, jsonArray);

            return jsonObject;
        }

        @Override
        public boolean equals(Object o) {
            CarriersModel.Item item = (CarriersModel.Item) o;

            if (item.carrier() == null) return false;
            if (!carrier().equals(item.carrier())) return false;
            for(int j=0; j<configs().size(); j++) {
                if (!configs.get(j).equals(item.configs().get(j)))
                    return false;
            }
            return true;
        }

        /**
         * toString
         * @return string values of SubmenuModel.Item object.
         */
        public String toString() {
            StringBuilder sbuilder = new StringBuilder();
            appendString(sbuilder, FIELD_CARRIER + " : " + carrier());
            for(ConfigModel config : configs)
                appendString(sbuilder, FIELD_CONFIGS + " : " + config.toString());
            return sbuilder.toString();
        }

        /**
         * append string with line.separator.
         * @param builder StringBuilder class object.
         * @param value string value.
         */
        public void appendString(StringBuilder builder, String value) {
            builder.append(value + System.getProperty("line.separator"));
        }

        /**
         * print
         */
        public void print() {
            Log.d(Item.class.getName(), toString());
        }

        @Override
        public Item clone() throws CloneNotSupportedException {
            return (Item) super.clone();
        }
    }

    @Override
    public JsonElement serialize(CarriersModel src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray jsonArray = new JsonArray();
        for (int i=0; i<src.size(); i++) {
            jsonArray.add(src.get(i).serialize(src.get(i), typeOfSrc, context));
        }
        return jsonArray;
    }

    public String toString() {
        StringBuilder sbuilder = new StringBuilder();
        for(CarriersModel.Item item : this) {
            appendString(sbuilder, item.toString());
        }
        return sbuilder.toString();
    }

    public void appendString(StringBuilder builder, String value) {
        builder.append(value + System.getProperty("line.separator"));
    }

    public void print() {
        Log.d(ConfigsModel.class.getName(), toString());
    }

    @Override
    public boolean equals(Object o) {
        CarriersModel carriersModel = (CarriersModel) o;

        if (size() != carriersModel.size()) return false;
        for(int i=0; i<size(); i++) {
            if (carriersModel.get(i).carrier() == null) return false;
            if (!get(i).carrier().equals(carriersModel.get(i).carrier())) return false;
        }
        return true;
    }

    @Override
    public CarriersModel clone() {
        return (CarriersModel) super.clone();
    }
}
