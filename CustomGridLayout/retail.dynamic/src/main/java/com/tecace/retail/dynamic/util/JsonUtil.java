package com.tecace.retail.dynamic.util;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tecace.retail.res.util.ResFileUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;

/**
 * Created by icanmobile on 3/1/16.
 * JsonUtil class supports the loading and saving functionalities for Json file.
 * JsonUtil class is Singleton class.
 */
public class JsonUtil {
    private static final String TAG = JsonUtil.class.getSimpleName();

    private static volatile JsonUtil sInstance = null;

    /**
     * get JsonUtil Singleton object.
     * @return JsonUtil object.
     */
    public static JsonUtil getInstance() {
        if (sInstance == null) {
            synchronized (JsonUtil.class) {
                if (sInstance == null)
                    sInstance = new JsonUtil();
            }
        }
        return sInstance;
    }
    private JsonUtil() {}

    /**
     * Storage enum class
     */
    public enum Storage {
        UNKNOWN, INTERNAL, EXTERNAL, ASSET
    }

    public String getExternalLocaleFilePath(Context context, String filename) {
        return new StringBuilder().append(ResFileUtil.getInstance().getExternalResourcePath(context)).append("/").append(filename).toString();
    }

    /**
     * load json file.
     * @param context application or activity context.
     * @param classOfT class type.
     * @param filename json file name.
     * @param storage storage option based on enum Storage class.
     * @param <T> generic class type.
     * @return created object based on class type.
     */
    public <T> T loadJson(Context context, Class<T> classOfT, String filename, Storage storage) {
        String str;
        if (storage == Storage.ASSET)
            str = getTextFromAsset(context, filename);
        else
            str = getText(context, filename, storage);

        Object jsonData = null;
        Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting()
                .create();
        try {
            jsonData = gson.fromJson(str, classOfT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (T)jsonData;
    }

    /**
     * load json file.
     * @param context application or activity context.
     * @param type java.lang.reflect.type interface.
     * @param filename json file name.
     * @param storage storage option based on enum Storage class.
     * @param <T> generic class type.
     * @return created object based on class type.
     */
    public <T> T loadJson(Context context, Type type, String filename, Storage storage) {
        String str;
        if (storage == Storage.ASSET)
            str = getTextFromAsset(context, filename);
        else
            str = getText(context, filename, storage);

        Object jsonData = null;
        Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting()
                .create();
        try {
            jsonData = gson.fromJson(str, type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (T)jsonData;
    }

    /**
     * save json file.
     * @param context application or activity context.
     * @param object source object.
     * @param classOfT class type.
     * @param filename json file name.
     * @param storage storage option based on enum Storage class.
     * @param <T> generic class type.
     */
    public <T> void saveJson(Context context, Object object, Class<T> classOfT, String filename, Storage storage) {
        if (storage == Storage.ASSET) return;

        File file;
        if (storage == Storage.INTERNAL)
            file = new File(getInternalFileFullPath(context, filename));
        else if (storage == Storage.EXTERNAL)
            file = new File(getExternalLocaleFilePath(context, filename));
        else    //Storage.UNKNOWN
            file = new File(filename);

        makeFolder(file.getParent());

        OutputStream outputStream = null;
        Gson gson = new GsonBuilder().registerTypeAdapter(classOfT, object).setPrettyPrinting()
                .create();
        try {
            outputStream = new FileOutputStream(file);
            BufferedWriter bufferedWriter;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream,
                        StandardCharsets.UTF_8));
            } else {
                bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            }

            gson.toJson(object, classOfT, bufferedWriter);
            bufferedWriter.close();

        } catch (FileNotFoundException e) {
//            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * save json file.
     * @param context application or activity context.
     * @param object source object.
     * @param type java.lang.reflect.type interface.
     * @param filename json file name.
     * @param storage storage option based on enum Storage class.
     * @param <T> generic class type.
     */
    public <T> void saveJson(Context context, Object object, Type type, String filename, Storage storage) {
        if (storage == Storage.ASSET) return;

        File file;
        if (storage == Storage.INTERNAL)
            file = new File(getInternalFileFullPath(context, filename));
        else
            file = new File(getExternalLocaleFilePath(context, filename));
        makeFolder(file.getParent());

        OutputStream outputStream = null;
        Gson gson = new GsonBuilder().registerTypeAdapter(type, object).setPrettyPrinting()
                .create();
        try {
            outputStream = new FileOutputStream(file);
            BufferedWriter bufferedWriter;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream,
                        StandardCharsets.UTF_8));
            } else {
                bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            }

            gson.toJson(object, type, bufferedWriter);
            bufferedWriter.close();

        } catch (FileNotFoundException e) {
//            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * delete file.
     * @param context application or activity context.
     * @param filename file name.
     * @param storage storage option based on enum Storage class.
     */
    public void deleteFile(Context context, String filename, Storage storage) {
        File file;
        if (storage == Storage.INTERNAL)
            file = new File(getInternalFileFullPath(context, filename));
        else if (storage == Storage.EXTERNAL)
            file = new File(getExternalLocaleFilePath(context, filename));
        else
            file = new File(filename);

        Log.d(TAG, "##### deleteFile : deleteFile : " + file.getPath());

        if (file != null)
            file.delete();
    }

    /**
     * make folders.
     * @param path folder path.
     */
    private void makeFolder(String path) {
        File dir = new File(path);
        if (dir != null) {
            dir.mkdirs();
        }
    }

    /**
     * get the full path of internal file.
     * @param context application or activity context.
     * @param filename file name
     * @return the full path of file which includes internal storage path.
     */
    public String getInternalFileFullPath(Context context, String filename) {
        if (context == null || filename == null || filename.length() == 0) return null;
        final String dir = context.getFilesDir().toString();
        if (filename.contains(dir)) return filename;
        return String.format("%s/%s", dir, filename);
    }

    /**
     * get the full path of external file.
     * @param context application or activity context.
     * @param filename file name
     * @return the full path of file which includes external storage path.
     */
    public String getExternalFileFullPath(Context context, String filename){
        if (context == null || filename == null || filename.length() == 0) return null;
        final String dir = context.getExternalFilesDir(null).toString();
        if (filename.contains(dir))
            return filename;
        return String.format("%s/%s", dir, filename);
    }



    /**
     * get strings of the file.
     * @param context application or activity context.
     * @param filename file name
     * @param storage storage option based on enum Storage class.
     * @return the strings of the file.
     */
    private String getText(Context context, String filename, Storage storage) {
        StringBuilder ret = new StringBuilder();

        File file = null;
        if (storage == Storage.INTERNAL)
            file = new File(getInternalFileFullPath(context, filename));
        else if (storage == Storage.EXTERNAL)
            file = new File(ResFileUtil.getInstance().getResourcePathWithLocale(context, filename));
        else
            file = new File(filename);

        if (file == null) return null;

        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
            InputStreamReader streamReader;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                streamReader = new InputStreamReader(inputStream,
                        StandardCharsets.UTF_8);
            } else {
                streamReader = new InputStreamReader(inputStream, "UTF-8");
            }

            BufferedReader f = new BufferedReader(streamReader);
            String line = f.readLine();
            while (line != null) {
                ret.append(line);
                line = f.readLine();
            }
            f.close();
            streamReader.close();
        } catch (FileNotFoundException e) {
//            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return ret.toString();
    }

    /**
     * get strings of the file in Assets folder.
     * @param context application or activity context.
     * @param filename file name.
     * @return the strings of the file.
     */
    private String getTextFromAsset(Context context, String filename) {
        StringBuilder ret = new StringBuilder();
        InputStream inputStream = null;
        try {
            inputStream = context.getAssets().open(filename);
            InputStreamReader streamReader;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                streamReader = new InputStreamReader(inputStream,
                        StandardCharsets.UTF_8);
            } else {
                streamReader = new InputStreamReader(inputStream, "UTF-8");
            }

            BufferedReader f = new BufferedReader(streamReader);
            String line = f.readLine();
            while (line != null) {
                ret.append(line);
                line = f.readLine();
            }
            f.close();
            streamReader.close();
        } catch (FileNotFoundException e) {
//            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return ret.toString();
    }
}
