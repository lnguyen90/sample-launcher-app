package com.tecace.retail.dynamic.content;


import com.tecace.retail.dynamic.model.ConfigModel;
import com.tecace.retail.dynamic.model.ConfigsModel;
import com.tecace.retail.dynamic.model.PageModel;
import com.tecace.retail.dynamic.util.JsonUtil;

/**
 * Created by icanmobile on 6/16/17.
 * Config enum class for the ConfigModel class.
 */

public enum Config {
    CONFIGS (
            ConfigsModel.class,
            "model/configs.json",
            JsonUtil.Storage.ASSET,
            "configs.json",
            JsonUtil.Storage.EXTERNAL,
            "configs.json",
            JsonUtil.Storage.EXTERNAL
    ),

    CONFIG (
            ConfigModel.class,
            "model/config.json",
            JsonUtil.Storage.ASSET,
            "model/config.json",
            JsonUtil.Storage.EXTERNAL,
            "model/config.json",
            JsonUtil.Storage.EXTERNAL
    ),


    FLAVOR,

    CONTENT,
    FOLDER,

    PAGE_1(
            PageModel.class,
            "model/page_1.json",
            JsonUtil.Storage.ASSET,
            "model/page_1.json",
            JsonUtil.Storage.ASSET,
            "model/page_1.json",
            JsonUtil.Storage.EXTERNAL
    );


    private final Class<?> classOfT;
    private final String initFile;
    private final String inputFile;
    private String outputFile;
    private final JsonUtil.Storage initStorageOpt;
    private final JsonUtil.Storage inputStorageOpt;
    private final JsonUtil.Storage outputStorageOpt;
    Config() {
        this(null, null, null, null, null, null, null);
    }

    /**
     * Config
     * @param classOfT class type.
     * @param initFile initial file name.
     * @param initStorageOpt initial storage option.
     * @param inputFile input file name.
     * @param inputStorageOpt input storage option.
     * @param outputFile output file name.
     * @param outputStorageOpt output storage option.
     */
    Config(Class<?> classOfT,
           String initFile, JsonUtil.Storage initStorageOpt,
           String inputFile, JsonUtil.Storage inputStorageOpt,
           String outputFile, JsonUtil.Storage outputStorageOpt) {
        this.classOfT = classOfT;
        this.initFile = initFile;
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        this.initStorageOpt = initStorageOpt;
        this.inputStorageOpt = inputStorageOpt;
        this.outputStorageOpt = outputStorageOpt;
    }

    /**
     * get class type.
     * @return class type
     */
    public Class<?> getClassOfT() {
        return this.classOfT;
    }

    /**
     * get initial file name.
     * @return initial file name.
     */
    public String getInitFile() {
        return this.initFile;
    }

    /**
     * get initial storage option.
     * @return initial storage option.
     */
    public JsonUtil.Storage getInitStorageOpt() {
        return this.initStorageOpt;
    }

    /**
     * get input file name.
     * @return input file name.
     */
    public String getInputFile() {
        return this.inputFile;
    }

    /**
     * get output file name.
     * @return output file name.
     */
    public String getOutputFile() {
        return this.outputFile;
    }
    public void setOutputFile(String outputFile) {
        this.outputFile = outputFile;
    }
    /**
     * get input storage option.
     * @return input storage option.
     */
    public JsonUtil.Storage getInputStorageOpt() {
        return this.inputStorageOpt;
    }

    /**
     * get output storage option.
     * @return output storage option.
     */
    public JsonUtil.Storage getOutputStorageOpt() {
        return this.outputStorageOpt;
    }

}
