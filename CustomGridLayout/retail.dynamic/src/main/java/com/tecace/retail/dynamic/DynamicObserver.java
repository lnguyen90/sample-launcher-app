package com.tecace.retail.dynamic;

/**
 * Created by icanmobile on 11/16/17.
 */

public interface DynamicObserver {
    void updated();
}
