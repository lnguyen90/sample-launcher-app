package com.tecace.retail.dynamic.model;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class PageModel extends ArrayList<PageModel.Item> implements JsonSerializer<PageModel>, Cloneable {

    private static final String FIELD_TYPE = "type";
    private static final String FIELD_LAYOUT = "layout";
    private static final String FIELD_IMAGE = "image";
    private static final String FIELD_TEXT = "text";

    /**
     * Item class includes the layout information of main menu item
     */
    public static class Item implements JsonSerializer<Item>, Cloneable {

        @SerializedName(FIELD_TYPE)
        private String type;

        @SerializedName(FIELD_LAYOUT)
        private String layout;

        @SerializedName(FIELD_IMAGE)
        private String image;

        @SerializedName(FIELD_TEXT)
        private String text;

        private transient int column;
        private transient int row;
        private transient int columnSpan;
        private transient int rowSpan;


        public String type() {
            return this.type;
        }
        public void type(String type) {
            this.type = type;
        }

        public String layout() {
            return this.layout;
        }
        public void layout(String layout) {
            this.layout = layout;
        }

        public String image() {
            return this.image;
        }
        public void image(String image) {
            this.image = image;
        }

        public String text() {
            return this.text;
        }
        public void text(String text) {
            this.text = text;
        }

        /**
         * set the orders of the items of PageModel.Item object when json file is created from object.
         * @param src PageModel.Item object
         * @param typeOfSrc java.lang.reflect.type interface.
         * @param context application or activity context.
         * @return the serialized json element.
         */
        @Override
        public JsonElement serialize(Item src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add(FIELD_TYPE, context.serialize(src.type()));
            jsonObject.add(FIELD_LAYOUT, context.serialize(src.layout()));
            jsonObject.add(FIELD_IMAGE, context.serialize(src.image()));
            jsonObject.add(FIELD_TEXT, context.serialize(src.text()));
            return jsonObject;
        }

        /**
         * toString
         * @return string values of PageModel.Item object.
         */
        public String toString() {
            StringBuilder sbuilder = new StringBuilder();
            appendString(sbuilder, FIELD_TEXT + " : " + type());
            appendString(sbuilder, FIELD_LAYOUT + " : " + layout());
            appendString(sbuilder, FIELD_IMAGE + " : " + image());
            appendString(sbuilder, FIELD_TEXT + " : " + text());
            return sbuilder.toString();
        }

        /**
         * append string with line.separator.
         * @param builder StringBuilder class object.
         * @param value string value.
         */
        public void appendString(StringBuilder builder, String value) {
            builder.append(value + System.getProperty("line.separator"));
        }

        /**
         * print
         */
        public void print() {
            Log.d(PageModel.Item.class.getName(), toString());
        }

        @Override
        public Item clone() throws CloneNotSupportedException {
            return (Item) super.clone();
        }

        public int getRowSpan() {
            return this.rowSpan;
        }

        public int getColumnSpan() {
            return this.columnSpan;
        }

        public int getRowPosition() {
            return this.row;
        }

        public int getColumnPosition() {
            return this.column;
        }


        public void setColumn(int column) {
            this.column = column;
        }

        public void setRow(int row) {
            this.row = row;
        }

        public void setColumnSpan(int columnSpan) {
            this.columnSpan = columnSpan;
        }

        public void setRowSpan(int rowSpan) {
            this.rowSpan = rowSpan;
        }
    }

    /**
     * set the orders of the items of PageModel object when json file is created from object.
     * @param src PageModel object
     * @param typeOfSrc java.lang.reflect.type interface.
     * @param context application or activity context.
     * @return the serialized json element.
     */
    @Override
    public JsonElement serialize(PageModel src, Type typeOfSrc, JsonSerializationContext context) {
        JsonArray jsonArray = new JsonArray();
        for (int i=0; i<src.size(); i++) {
            jsonArray.add(src.get(i).serialize(src.get(i), typeOfSrc, context));
        }
        return jsonArray;
    }

    /**
     * toString
     * @return string values of PageModel object.
     */
    public String toString() {
        StringBuilder sbuilder = new StringBuilder();
        for(PageModel.Item item : this) {
            appendString(sbuilder, item.toString());
        }
        return sbuilder.toString();
    }

    /**
     * append string with line.separator.
     * @param builder StringBuilder class object.
     * @param value string value.
     */
    public void appendString(StringBuilder builder, String value) {
        builder.append(value + System.getProperty("line.separator"));
    }

    /**
     * print
     */
    public void print() {
        Log.d(PageModel.class.getName(), toString());
    }

    @Override
    public PageModel clone() {
        return (PageModel) super.clone();
    }
}
