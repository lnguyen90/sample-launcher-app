package com.tecace.retail.dynamic.content;

import java.util.Iterator;

/**
 * Created by icanmobile on 6/15/17.
 * IContent interface supports to modify the specific menu items of MainModel and SubmenuModel object.
 * This interface supports re-ordering and delete functionalities for menu items.
 */

public interface IContent extends Iterator {
    /**
     * sort the member function of IContent interface. Sorting the model object items based on teh field value of items.
     * If the field value is less than 0 (including 0), the field is removed.
     * If the field value is larger than 1 (including 1), the field is added and sorted for re-ordering of main menu.
     */
    void sort();

    /**
     * the member function of IContent interface. Implemented the iterator interface.
     * @return Iterator object.
     */
    Iterator iterator();

    /**
     * the member function of IContent interface. Implemented the iterator interface.
     * @return if the next item exits, return true.
     */
    boolean hasNext();

    /**
     * the member function of IContent interface. Implemented the iterator interface.
     * @return the item.
     */
    Object next();
}
