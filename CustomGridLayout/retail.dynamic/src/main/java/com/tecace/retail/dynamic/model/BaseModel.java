package com.tecace.retail.dynamic.model;

import java.io.Serializable;

/**
 * Created by icanmobile on 6/16/17.
 * BaseModel class supports the basic functionalities for Model classes.
 */

public abstract class BaseModel implements Serializable {
    /**
     * toString
     * @return string values of Model object.
     */
    abstract public String toString();

    /**
     * print
     */
    abstract public void print();

    /**
     * append string with line.separator.
     * @param builder StringBuilder class object.
     * @param value string value.
     */
    public void appendString(StringBuilder builder, String value) {
        builder.append(value + System.getProperty("line.separator"));
    }
}
