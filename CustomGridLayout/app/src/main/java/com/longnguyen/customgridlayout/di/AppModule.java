package com.longnguyen.customgridlayout.di;

import com.longnguyen.customgridlayout.util.JsonUtil;
import com.longnguyen.customgridlayout.util.JsonUtilImpl;
import com.tecace.retail.dynamic.Dynamic;
import com.tecace.retail.res.util.ResJsonUtil;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    Dynamic providesDynamic() { return Dynamic.getInstance(); }

    @Provides
    ResJsonUtil providesResJsonUtil() { return ResJsonUtil.getInstance(); }

    @Provides
    JsonUtil providesJsonUtil(ResJsonUtil resJsonUtil) {
        return new JsonUtilImpl(resJsonUtil);
    }
}
