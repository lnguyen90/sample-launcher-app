package com.longnguyen.customgridlayout.customflexboxlayout;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Long on 09/26/2018
 */
public abstract class CustomFlexBoxAdapter {

    public abstract int getCount();

    protected int getItemViewType(int position) { return position; }

    protected int getViewTypeCount() { return 0; }

    public abstract CustomFlexBoxItem getCustomFlexBoxItem(int position);

    public abstract View getView(final int position, @NonNull ViewGroup parent);
}
