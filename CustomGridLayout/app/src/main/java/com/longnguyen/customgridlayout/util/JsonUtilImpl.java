package com.longnguyen.customgridlayout.util;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.tecace.retail.dynamic.model.ConfigModel;
import com.tecace.retail.dynamic.model.PageModel;
import com.tecace.retail.res.util.ResJsonUtil;

public class JsonUtilImpl implements JsonUtil {

    private ResJsonUtil mResJsonUtil;

    public JsonUtilImpl(ResJsonUtil resJsonUtil) {
        this.mResJsonUtil = resJsonUtil;
    }

    @Override
    public ConfigModel getConfigModel(Context context, String json) {
        if (json == null || json.length() == 0) return null;

//        ConfigsModel configsModel = mResJsonUtil.loadJsonModel(context, json, new TypeToken<ConfigsModel>() {}.getType());
//        for(ConfigsModel.Item item: configsModel) {
//            if(item.subsidiary().equals(subsidiary)) {
//                for(CarriersModel.Item _carrier: item.carriers()) {
//                    if(_carrier.carrier().equals(carrier)) {
//                        for (ConfigModel configModel: _carrier.configs()) {
//                            if(configModel.flavor().equals(flavor)) {
//                                config = configModel.page_1().getChildViews();
//                            }
//                        }
//                    }
//                }
//            }
//        }


        return mResJsonUtil.loadJsonModel(context, json, new TypeToken<ConfigModel>() {}.getType());
    }

    @Override
    public PageModel getPageModel(Context context, String json) {
        if (json == null || json.length() == 0) return null;
        return mResJsonUtil.loadJsonModel(context, json, new TypeToken<PageModel>() {}.getType());
    }
}
