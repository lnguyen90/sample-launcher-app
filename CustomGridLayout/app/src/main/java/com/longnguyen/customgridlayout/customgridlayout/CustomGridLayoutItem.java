package com.longnguyen.customgridlayout.customgridlayout;

/**
 * Created by Long on 09/26/2018
 */
public interface CustomGridLayoutItem {

    int getRowSpan();
    int getColumnSpan();
    int getRowPosition();
    int getColumnPosition();


}
