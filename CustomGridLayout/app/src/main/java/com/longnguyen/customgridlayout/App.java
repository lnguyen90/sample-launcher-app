package com.longnguyen.customgridlayout;

import android.app.Application;

import com.longnguyen.customgridlayout.di.AppComponent;
import com.longnguyen.customgridlayout.di.AppModule;
import com.longnguyen.customgridlayout.di.DaggerAppComponent;

public class App extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder().appModule(new AppModule()).build();
        appComponent.inject(this);

    }

    public AppComponent getAppComponent() {
        return this.appComponent;
    }
}
