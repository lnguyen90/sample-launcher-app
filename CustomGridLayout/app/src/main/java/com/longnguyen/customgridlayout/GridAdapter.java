package com.longnguyen.customgridlayout;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.longnguyen.customgridlayout.customgridlayout.CustomGridLayoutAdapter;
import com.longnguyen.customgridlayout.customgridlayout.CustomGridLayoutItem;
import com.tecace.retail.dynamic.model.PageModel;
import com.tecace.retail.res.util.ResFileUtil;
import com.tecace.retail.res.util.ResUtil;

import java.util.List;

/**
 * Created by Long on 09/26/2018
 */
public class GridAdapter extends CustomGridLayoutAdapter {

    private static final String TAG = GridAdapter.class.getSimpleName();

    private PageModel list;

    public GridAdapter(PageModel list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public PageModel.Item getCustomGridLayoutItem(int position) {
        return list.get(position);
    }

    @Override
    protected String getItemViewType(int position) {
        return list.get(position).layout();
    }

    @Override
    protected int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(int position, @NonNull ViewGroup parent) {

        View newView = null;
        String viewType = getItemViewType(position);
        switch (viewType) {
            case "@layout/row_item_image":
                newView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_item_image, parent, false);
                populateImage(newView, position);
                break;
            case "@layout/row_item_image_text":
                newView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_item_image_text, parent, false);
                populateTextImage(newView, position);
                break;
            default:
                break;
        }


        return newView;
    }

    private void populateTextImage(View view, int position) {
        int imageRes = ResUtil.getInstance().getResId(view.getContext(), list.get(position).image());
        ImageView imageView = view.findViewById(R.id.image);
        Glide.with(view.getContext()).load(imageRes).into(imageView);

        int textRes = ResUtil.getInstance().getResId(view.getContext(), list.get(position).text());
        TextView textView = view.findViewById(R.id.text);
        textView.setText(textRes);
        textView.setTextColor(Color.WHITE);
    }

    private void populateImage(View view, int position) {
        int imageRes = ResUtil.getInstance().getResId(view.getContext(), list.get(position).image());
        ImageView imageView = view.findViewById(R.id.image);
        Glide.with(view.getContext()).load(imageRes).into(imageView);
    }
}
