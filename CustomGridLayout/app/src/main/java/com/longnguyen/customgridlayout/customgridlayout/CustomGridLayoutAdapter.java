package com.longnguyen.customgridlayout.customgridlayout;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

import com.tecace.retail.dynamic.model.PageModel;

/**
 * Created by Long on 09/26/2018
 */
public abstract class CustomGridLayoutAdapter {

    public abstract int getCount();

    protected String getItemViewType(int position) {
        return null;
    }

    protected int getViewTypeCount() { return 0; }

    public abstract PageModel.Item getCustomGridLayoutItem(int position);

    public abstract View getView(final int position, @NonNull ViewGroup parent);
}
