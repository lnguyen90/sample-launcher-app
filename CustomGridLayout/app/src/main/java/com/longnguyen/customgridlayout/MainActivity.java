package com.longnguyen.customgridlayout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.longnguyen.customgridlayout.customflexboxlayout.CustomFlexBoxLayout;
import com.longnguyen.customgridlayout.customgridlayout.CustomGridLayout;
import com.longnguyen.customgridlayout.util.JsonUtil;
import com.tecace.retail.dynamic.Dynamic;
import com.tecace.retail.dynamic.model.ConfigModel;
import com.tecace.retail.dynamic.model.GridInfoArrayModel;
import com.tecace.retail.dynamic.model.PageModel;
import com.tecace.retail.util.PreferenceUtil;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Long on 09/26/2018
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    /* Using FlexBox */
    private static final List<FlexBoxItem> flexBoxItemList = Arrays.asList(
        new FlexBoxItem(4, FlexBoxAdapter.TYPE_IMAGE, R.drawable.watch_whats_new),
        new FlexBoxItem(2, FlexBoxAdapter.TYPE_IMAGE_TEXT, R.drawable.battery, "Fast Professor & All-Day Battery"),
        new FlexBoxItem(2, FlexBoxAdapter.TYPE_IMAGE_TEXT, R.drawable.battery, "New Bluetooth S Pen"),
        new FlexBoxItem(2, FlexBoxAdapter.TYPE_IMAGE_TEXT, R.drawable.battery, "Smartest Camera Ever"),
        new FlexBoxItem(2, FlexBoxAdapter.TYPE_IMAGE_TEXT, R.drawable.battery, "Samsung Dex"),
        new FlexBoxItem(4, FlexBoxAdapter.TYPE_IMAGE, R.drawable.spec),
        new FlexBoxItem(2, FlexBoxAdapter.TYPE_IMAGE_TEXT, R.drawable.battery, "Fast Professor & All-Day Battery"),
        new FlexBoxItem(2, FlexBoxAdapter.TYPE_IMAGE_TEXT, R.drawable.battery, "Samsung Dex")
    );

    @Inject
    Dynamic dynamic;

    @Inject JsonUtil jsonUtil;

    private PageModel pageModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((App) getApplication()).getAppComponent().inject(this);

        // Setup dynamic
        setUpDynamic();

        ConfigModel configModel = jsonUtil.getConfigModel(this, "model/config.json");
        pageModel = jsonUtil.getPageModel(this, "model/page_1.json");
        for(int i = 0; i < configModel.page_1().getChildViews().size(); i++) {
            PageModel.Item page = pageModel.get(i);
            GridInfoArrayModel.Item gridInfo = configModel.page_1().getChildViews().get(i);
            if(page.type().equals(gridInfo.getType())) {
                page.setColumn(gridInfo.getColumn());
                page.setRow(gridInfo.getRow());
                page.setColumnSpan(gridInfo.getColumnSpan());
                page.setRowSpan(gridInfo.getRowSpan());
            }
        }

        useGridLayout();

    }


    private void useFlexBox() {
        CustomFlexBoxLayout customFlexBoxLayout = findViewById(R.id.custom_layout);
        // the column values from the flexBoxItemList above depends on this value. Example, column count is 4,
        // and if you set a column span a 4, it will span out 100% of the parent's view's width, if
        // you set a column span at 2, it will span out 50% of the parent's view's width.
        customFlexBoxLayout.setColumnCount(4);
        FlexBoxAdapter adapter = new FlexBoxAdapter(flexBoxItemList);
        customFlexBoxLayout.setAdapter(adapter);
    }

    private void useGridLayout() {
        CustomGridLayout customGridLayout = findViewById(R.id.custom_layout);
        customGridLayout.setColumnCount(4);
        GridAdapter adapter = new GridAdapter(pageModel);
        customGridLayout.setAdapter(adapter);
    }

    private void setUpDynamic() {

        if (isFirstLaunch()) {
            this.dynamic.reset(this
                    , Dynamic.SUBSIDIARY_DEFAULT, Dynamic.CARRIER_DEFAULT, Dynamic.PRODUCT_FLAVOR_SAMPLE);
        }
        else if (this.dynamic.isModified(this,
                Dynamic.SUBSIDIARY_DEFAULT, Dynamic.CARRIER_DEFAULT, Dynamic.PRODUCT_FLAVOR_SAMPLE)) {
            this.dynamic.refresh(this,
                    Dynamic.SUBSIDIARY_DEFAULT, Dynamic.CARRIER_DEFAULT, Dynamic.PRODUCT_FLAVOR_SAMPLE);
        }
    }

    private boolean isFirstLaunch() {
        final String PREF_VERSION_CODE_KEY = "version_code";
        int currentVersionCode = BuildConfig.VERSION_CODE;
        int savedVersionCode = PreferenceUtil.getInstance().getInt(getApplicationContext(), PREF_VERSION_CODE_KEY);
        PreferenceUtil.getInstance().setInt(getApplicationContext(), PREF_VERSION_CODE_KEY, currentVersionCode);

        if (currentVersionCode != savedVersionCode)
            Log.d(TAG, "##### ICANMOBILE : isFirstLaunch : FIRST LAUNCH !!!!!!");
        return currentVersionCode == savedVersionCode ? false : true;
    }
}
