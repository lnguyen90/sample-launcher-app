package com.longnguyen.customgridlayout;

import com.longnguyen.customgridlayout.customflexboxlayout.CustomFlexBoxItem;

/**
 * Created by Long on 09/26/2018
 */
public class FlexBoxItem implements CustomFlexBoxItem {

    private int mColumnSpan;
    private int mType;
    private int mImage;
    private String mText;

    public FlexBoxItem(int columnSpan, int type, int image, String text) {
        this.mColumnSpan = columnSpan;
        this.mType = type;
        this.mImage = image;
        this.mText = text;
    }

    public FlexBoxItem(int columnSpan, int type, int image) {
        this.mColumnSpan = columnSpan;
        this.mType = type;
        this.mImage = image;
    }

    public FlexBoxItem(int columnSpan, int type, String text) {
        this.mColumnSpan = columnSpan;
        this.mType = type;
        this.mText = text;
    }

    public FlexBoxItem(int columnSpan, int type) {
        this.mColumnSpan = columnSpan;
        this.mType = type;
    }

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        this.mType = type;
    }

    public int getImage() {
        return mImage;
    }

    public void setImage(int image) {
        this.mImage = image;
    }

    public String getText() {
        return mText;
    }

    public void setText(String mText) {
        this.mText = mText;
    }

    @Override
    public int getColumnSpan() {
        return mColumnSpan;
    }

}
