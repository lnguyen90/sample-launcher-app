package com.longnguyen.customgridlayout;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.longnguyen.customgridlayout.customflexboxlayout.CustomFlexBoxAdapter;
import com.longnguyen.customgridlayout.customflexboxlayout.CustomFlexBoxItem;

import java.util.List;

/**
 * Created by Long on 09/26/2018
 */
public class FlexBoxAdapter extends CustomFlexBoxAdapter {

    private static final String TAG = FlexBoxAdapter.class.getSimpleName();

    public static final int TYPE_IMAGE_TEXT = 0;
    public static final int TYPE_IMAGE = 1;
    public static final int TYPE_TEXT = 2;
    public static final int TYPE_BLANK = 3;

    private List<FlexBoxItem> list;

    public FlexBoxAdapter(List<FlexBoxItem> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public CustomFlexBoxItem getCustomFlexBoxItem(int position) {
        return list.get(position);
    }

    @Override
    protected int getItemViewType(int position) {
        return list.get(position).getType();
    }

    @Override
    protected int getViewTypeCount() {
        return 4;
    }

    @Override
    public View getView(int position, @NonNull ViewGroup parent) {

        View newView;
        int viewType = getItemViewType(position);
        if(viewType == TYPE_IMAGE_TEXT) {
            newView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_item_image_text, parent, false);
            populateTextImage(newView, position);
        } else if(viewType == TYPE_TEXT){
            newView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_item_text, parent, false);
            populateText(newView, position);
        } else if(viewType == TYPE_IMAGE) {
            newView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_item_image, parent, false);
            populateImage(newView, position);
        } else {
            newView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_item_blank, parent, false);
        }


        return newView;
    }

    private void populateText(View view, int position) {
        TextView text = view.findViewById(R.id.text);
        text.setText(list.get(position).getText());
        text.setTextColor(Color.WHITE);
    }

    private void populateTextImage(View view, int position) {
        ImageView imageView = view.findViewById(R.id.image);
        Glide.with(view.getContext()).load(list.get(position).getImage()).into(imageView);

        TextView textView = view.findViewById(R.id.text);
        textView.setText(list.get(position).getText());
        textView.setTextColor(Color.WHITE);
    }

    private void populateImage(View view, int position) {
        ImageView imageView = view.findViewById(R.id.image);
        Glide.with(view.getContext()).load(list.get(position).getImage()).into(imageView);
    }
}
