package com.longnguyen.customgridlayout.di;

import com.longnguyen.customgridlayout.App;
import com.longnguyen.customgridlayout.MainActivity;

import dagger.Component;

@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(App app);
    void inject(MainActivity activity);
}
