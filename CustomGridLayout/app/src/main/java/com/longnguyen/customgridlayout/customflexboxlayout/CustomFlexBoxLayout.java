package com.longnguyen.customgridlayout.customflexboxlayout;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.AlignSelf;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayout;

/**
 * Created by Long on 09/26/2018
 */
public class CustomFlexBoxLayout extends ScrollView {

    private static final String TAG = CustomFlexBoxLayout.class.getSimpleName();

    private static int DEFAULT_COLUMN_SPAN = 4;

    private static int DEFAULT_ROW_HEIGHT = 450;

    private CustomFlexBoxAdapter mAdapter;

    private FlexboxLayout mContainer;

    private int mColumn = DEFAULT_COLUMN_SPAN;

    private boolean mIsRowEnabled = false;

    public CustomFlexBoxLayout(Context context) {
        super(context);
        init();
    }

    public CustomFlexBoxLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomFlexBoxLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * Create and add a FlexboxLayout to the ScrollView
     */
    private void init() {

        // Create container
        mContainer = new FlexboxLayout(getContext());

        FlexboxLayout.LayoutParams layoutParams = new FlexboxLayout
                .LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mContainer.setLayoutParams(layoutParams);

        mContainer.setFlexWrap(FlexWrap.WRAP);
        mContainer.setAlignItems(AlignItems.FLEX_START);

        addView(mContainer);

    }

    /**
     * Set the maximum column count.
     *
     * @param column
     */
    public void setColumnCount(int column) {
        this.mColumn = column;
        updateContainer();
    }

    /**
     * Enable row span functionality. Default behavior is wrap_content. If you enable row function.
     * You need to specify the the row span count in your model item. If you wish, you can change the
     * height of row as well in your model item.
     *
     * @param isRowEnabled
     */
    public void setRowEnabled(boolean isRowEnabled) {
        mIsRowEnabled = isRowEnabled;
        updateContainer();
    }

    /**
     * Set the adapter so that we can add the view created by the adapter to
     * the FlexBoxLayout container.
     *
     * @param adapter
     */
    public void setAdapter(CustomFlexBoxAdapter adapter) {
        this.mAdapter = adapter;
        updateContainer();
    }

    /**
     * Remove all previous views, and add all the views created by adapter to
     * the FlexBoxLayout container.
     */
    private void updateContainer() {
        if(mAdapter == null) return;

        removeChildViews(mContainer);

        for(int i = 0; i < mAdapter.getCount(); i++) {
            View view = mAdapter.getView(i, this);

            FlexboxLayout.LayoutParams layoutParams;

            // Get information about the item
            int viewColumn = mAdapter.getCustomFlexBoxItem(i).getColumnSpan();

//            if(mIsRowEnabled) {
//                int viewRow = mAdapter.getCustomGridLayoutItem(i).getRowSpan();
//                int rowHeight = mAdapter.getCustomGridLayoutItem(i).getRowHeight();
//
//                if(rowHeight <= 0) rowHeight = DEFAULT_ROW_HEIGHT;
//
//                layoutParams =
//                        new FlexboxLayout.LayoutParams(
//                                ViewGroup.LayoutParams.WRAP_CONTENT,
//                                getHeight(viewRow, rowHeight));
//            } else {
//                layoutParams =
//                        new FlexboxLayout.LayoutParams(
//                                ViewGroup.LayoutParams.WRAP_CONTENT,
//                                ViewGroup.LayoutParams.WRAP_CONTENT);
//            }
            layoutParams =
                    new FlexboxLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);

            layoutParams.setFlexBasisPercent(getWidth(viewColumn));
            layoutParams.setAlignSelf(AlignSelf.FLEX_START);
            view.setLayoutParams(layoutParams);

            mContainer.addView(view);
        }

    }

    /**
     * Calculate the width of the child view based on the column number.
     *
     * @param viewColumn The child view's column.
     * @return float
     */
    private float getWidth(int viewColumn) {
        if(viewColumn < 0 || viewColumn > mColumn) {
            throw new IllegalArgumentException("column span must be between 0 and " + mColumn);
        }
        return (viewColumn * 100f / mColumn) / 100f;
    }

    /**
     * Calculate the height of the child view based on the row number.
     *
     * @param viewRow The child view's row.
     * @return int
     */
    private int getHeight(int viewRow, int height) {
        if(viewRow < 0) {
            throw new IllegalArgumentException("row span must be between greater than 0");
        }

        return (viewRow * height);
    }

    /**
     * Remove all child views from its parent.
     *
     * @param parent The parent view.
     */
    private void removeChildViews(ViewGroup parent) {
        if (parent == null) return;
        release(parent);
        parent.removeAllViews();
    }

    private void release(ViewGroup parent) {
        for(int i=0; i<parent.getChildCount(); i++) {
            View childView = parent.getChildAt(i);
            if (childView instanceof ViewGroup) {
                release((ViewGroup) childView);
                ((ViewGroup) childView).removeAllViews();
            }
        }
    }

}
