package com.longnguyen.customgridlayout.util;

import android.content.Context;

import com.tecace.retail.dynamic.model.ConfigModel;
import com.tecace.retail.dynamic.model.PageModel;

public interface JsonUtil {

    ConfigModel getConfigModel(Context context, String json);
    PageModel getPageModel(Context context, String json);

}
